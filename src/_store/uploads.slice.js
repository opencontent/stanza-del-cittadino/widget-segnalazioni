import { createAsyncThunk, createSlice, current } from '@reduxjs/toolkit';
import { fetchWrapper } from '../_helpers/fetch-wrapper';

// create slice
const name = 'files';
const initialState = createInitialState();
const extraActions = createExtraActions();
const slice = createSlice({
  name,
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(`${name}/saveImage/fulfilled`, (state, action) => {
        const currentState = current(state);
        const images = [...currentState.images, action.payload];
        state.images = images;
      })
      .addCase(`${name}/removeImage/fulfilled`, (state, action) => {
        const currentState = current(state);
        state.images = currentState.images.filter((el) => el.file !== action.payload);
      })
      .addCase(`${name}/saveDoc/fulfilled`, (state, action) => {
        const currentState = current(state);
        const docs = [...currentState.docs, action.payload];
        state.docs = docs;
      })
      .addCase(`${name}/removeDoc/fulfilled`, (state, action) => {
        const currentState = current(state);
        state.docs = currentState.docs.filter((el) => el.file !== action.payload);
      })
      //UploadS3
      .addCase(`${name}/UploadS3/fulfilled`, (state, action) => {
        state.uploads = action.payload;
      })
      //UploadS3PUT
      .addCase(`${name}/UploadS3PUT/fulfilled`, (state, action) => {
        state.uploads = action.payload;
      });
  }
});

export const uploadsActions = { ...slice.actions, ...extraActions };
export const uploadsReducer = slice.reducer;

function createInitialState() {
  return {
    images: [],
    docs: [],
    uploads: []
  };
}

function createExtraActions() {
  const baseUrl = window.OC_BASE_URL || `${process.env.REACT_APP_API_URL}`;

  return {
    saveImage: saveImage(),
    removeImage: removeImage(),
    saveDoc: saveDoc(),
    removeDoc: removeDoc(),
    uploadS3: uploadS3(),
    uploadS3PUT: uploadS3PUT(),
    uploadS3PUTETAG: uploadS3PUTETAG()
  };

  function saveImage() {
    return createAsyncThunk(`${name}/saveImage`, (arg) => arg);
  }

  function removeImage() {
    return createAsyncThunk(`${name}/removeImage`, (arg) => arg);
  }

  function saveDoc() {
    return createAsyncThunk(`${name}/saveDoc`, (arg) => arg);
  }

  function removeDoc() {
    return createAsyncThunk(`${name}/removeDoc`, (arg) => arg);
  }

  function uploadS3() {
    return createAsyncThunk(
      `${name}/uploadS3`,
      async (data) => await fetchWrapper.post(`${baseUrl}/it/upload`, data)
    );
  }

  function uploadS3PUT() {
    return createAsyncThunk(
      `${name}/uploadS3PUT`,
      async (data) => await fetchWrapper.putWithHeader(`${data.uri}`, data.file)
    );
  }

  function uploadS3PUTETAG() {
    return createAsyncThunk(
      `${name}/uploadS3PUTETAG`,
      async (data) => await fetchWrapper.put(`${baseUrl}/it/upload/${data.id}`, data.data)
    );
  }
}
