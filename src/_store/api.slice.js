import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { fetchWrapper } from '../_helpers/fetch-wrapper';

// create slice
const name = 'api';
const initialState = createInitialState();
const extraActions = createExtraActions();
const slice = createSlice({
  name,
  initialState,
  reducers: {
    resetApiApplication: (state, action) => {
      state.application = {};
      state.sequential_id = null;
    },
setApiLanguage: (state, action) => {
  state.language = action.payload || 'it';
},
  },
  extraReducers: (builder) => {
    builder
      /** createApplication **/
      .addCase(`${name}/createApplication/fulfilled`, (state, action) => {
        state.application = action.payload;
      })
      .addCase(`${name}/createApplication/rejected`, (state, action) => {
        state.application = { error: action.error };
      })
      /** getServices **/
      .addCase(`${name}/getServices/fulfilled`, (state, action) => {
        state.services = action.payload;
      })
      /** cityInfo **/
      .addCase(`${name}/cityInfo/fulfilled`, (state, action) => {
        if (action.payload.length > 0) {
          state.city = action.payload[0];
        }
      })
      /** searchAddress **/
      .addCase(`${name}/searchAddress/fulfilled`, (state, action) => {
        if (action.payload.length > 0) {
          state.address = action.payload;
        }
      })
      .addCase(`${name}/getAddress/fulfilled`, (state, action) => {
        state.address = action.payload;
      })
      .addCase(`${name}/searchBoundingbox/fulfilled`, (state, action) => {
        if (action.payload.length > 0) {
          const boundArray = action.payload[0].boundingbox;
          state.boundingBox = boundArray[2].concat(',',boundArray[0],',',boundArray[3],',',boundArray[1]);
        }
      })
      /** getServiceReport **/
      .addCase(`${name}/getServiceReport/fulfilled`, (state, action) => {
        state.service = action.payload;
      })
      /** tenantInfo **/
      .addCase(`${name}/tenantInfo/fulfilled`, (state, action) => {
        state.meta = action.payload;
      })

      /** getDraftApplication **/
      .addCase(`${name}/getDraftApplication/fulfilled`, (state, action) => {
        const response = action.payload;
        state.application = response.data.length ? response.data[0] : {};
      })
      .addCase(`${name}/getDraftApplication/rejected`, (state, action) => {
        state.application = { error: action.error };
      })

      /** getPrivacyCheckApplication **/
      .addCase(`${name}/getPrivacyCheckApplication/fulfilled`, (state, action) => {
        const response = action.payload;
        state.privacyChecked = !!response.data.length;
      })
      /** updateApplication **/
      .addCase(`${name}/updateApplication/fulfilled`, (state, action) => {
        state.application = action.payload;
      })
      .addCase(`${name}/updateApplication/rejected`, (state, action) => {
        state.application = { error: action.error };
      })

      /** getGeographicAreas **/
      .addCase(`${name}/getGeographicAreas/fulfilled`, (state, action) => {
        state.application = action.payload;
      })
      .addCase(`${name}/getGeographicAreas/rejected`, (state, action) => {
        state.application = { error: action.error };
      })

      /** getGeographicAreasPoints **/
      .addCase(`${name}/getGeographicAreasPoints/fulfilled`, (state, action) => {
        state.application = action.payload;
      })
      .addCase(`${name}/getGeographicAreasPoints/rejected`, (state, action) => {
        state.application = { error: action.error };
      })

      /** getGeographicAreasById **/
      .addCase(`${name}/getGeographicAreasById/fulfilled`, (state, action) => {
        state.geographicAreas = action.payload;
      })
      .addCase(`${name}/getGeographicAreasById/rejected`, (state, action) => {
        state.geographicAreas = { error: action.error };
      })
      .addCase(`${name}/getSequentialId/fulfilled`, (state, action) => {
        state.sequential_id = action.payload.id;
      });
  }
});

// exports
export const apiActions = { ...slice.actions, ...extraActions };
export const apiReduced = slice.reducer;

// implementation
function createInitialState() {
  return {
    services: [],
    address: [],
    application: {},
    service: '',
    meta: {},
    geographicAreas: [],
    privacyChecked: false,
    sequential_id: null,
    language: 'it',
    city: {}
  };
}

function createExtraActions() {
  const baseUrl = window.OC_BASE_URL || `${process.env.REACT_APP_API_URL}`;
  const boundingBox = window.OC_BB || `${process.env.REACT_APP_OC_BB}`;
  const mapProvider =
    window.OC_MAP_SEARCH_PROVIDER || `${process.env.REACT_APP_MAP_SEARCH_PROVIDER}`;
  const sequentialIdProvider =
    window.OC_SEQUENTIAL_ID_PROVIDER || `${process.env.REACT_APP_OC_SEQUENTIAL_ID_PROVIDER}`;
  const serviceSlug = window.OC_SERVICE_IDENTIFIER || "inefficiencies";


  return {
    getServices: getServices(),
    createApplication: createApplication(),
    cityInfo: cityInfo(),
    searchAddress: searchAddress(),
    searchBoundingbox: searchBoundingbox(),
    getAddress: getAddress(),
    getServiceReport: getServiceReport(),
    tenantInfo: tenantInfo(),
    getDraftApplication: getDraftApplication(),
    updateApplication: updateApplication(),
    getGeographicAreas: getGeographicAreas(),
    getGeographicAreasPoints: getGeographicAreasPoints(),
    getGeographicAreasById: getGeographicAreasById(),
    getPrivacyCheckApplication: getPrivacyCheckApplication(),
    getSequentialId: getSequentialId()
  };

  function tenantInfo() {
    return createAsyncThunk(
      `${name}/tenantInfo`,
      async () => await fetchWrapper.get(`${baseUrl}/api/tenants/info`)
    );
  }

  function createApplication() {
    return createAsyncThunk(
      `${name}/createApplication`,
      async (data) => await fetchWrapper.post(`${baseUrl}/api/applications`, data)
    );
  }

  function cityInfo() {
    return createAsyncThunk(
      `${name}/cityInfo`,
      async (arg) =>
        await fetchWrapper.get(
          `https://api.opencontent.it/geo/comuni?cod_catastale=${arg.cadastral_code}`,
          null,
          true
        )
    );
  }

  function searchAddress() {
    return createAsyncThunk(
      `${name}/searchAddress`,
      async (arg) =>
        await fetchWrapper.get(
          `https://${mapProvider}/search?q=${arg.params}&countrycodes=it&viewbox=${arg.boundingBox || boundingBox}&bounded=1&addressdetails=1&format=jsonv2&accept-language=${arg.language}`
        )
    );
  }

  function searchBoundingbox() {
    return createAsyncThunk(
      `${name}/searchBoundingbox`,
      async (arg) =>
        await fetchWrapper.get(
          `https://${mapProvider}/search?city=${arg.city}&postalcode=${arg.postalcode}&format=json&country=Italia`
        )
    );
  }

  function getServices() {
    return createAsyncThunk(
      `${name}/getServices`,
      async () => await fetchWrapper.get(`${baseUrl}/api/services?limit=100`)
    );
  }

  function getAddress() {
    return createAsyncThunk(`${name}/getAddress`, (arg) => arg);
  }

  function getServiceReport() {
    return createAsyncThunk(
      `${name}/getServiceReport`,
      async () => await fetchWrapper.get(`${baseUrl}/api/services/${serviceSlug}`)
    );
  }

  function getPrivacyCheckApplication() {
    return createAsyncThunk(
      `${name}/getPrivacyCheckApplication`,
      async () =>
        await fetchWrapper.get(
          `${baseUrl}/api/applications?limit=1&service_identifier=${serviceSlug}`
        )
    );
  }

  function getDraftApplication() {
    return createAsyncThunk(
      `${name}/getDraftApplication`,
      async () =>
        await fetchWrapper.get(
          `${baseUrl}/api/applications?status=1000&limit=1&sort=desc&version=2&service_identifier=${serviceSlug}`
        )
    );
  }

  function updateApplication() {
    return createAsyncThunk(
      `${name}/updateApplication`,
      async (params) =>
        await fetchWrapper.put(`${baseUrl}/api/applications/${params.id}`, params.data)
    );
  }

  function getGeographicAreasPoints() {
    return createAsyncThunk(
      `${name}/getGeographicAreasPoints`,
      async (params) =>
        await fetchWrapper.get(
          `${baseUrl}/api/geographic-areas/contains?lat=${params.lat}&lon=${params.lon}&geographic_areas_ids=${params.geographic_areas_ids}`
        )
    );
  }
  function getGeographicAreas() {
    return createAsyncThunk(
      `${name}/getGeographicAreas`,
      async () => await fetchWrapper.get(`${baseUrl}/api/geographic-areas`)
    );
  }

  function getGeographicAreasById() {
    return createAsyncThunk(
      `${name}/getGeographicAreasById`,
      async (params) => await fetchWrapper.get(`${baseUrl}/api/geographic-areas/${params.id}`)
    );
  }
  function getSequentialId() {
    return createAsyncThunk(
      `${name}/getSequentialId`,
      async () =>
        await fetchWrapper.get(
          `${sequentialIdProvider}/app/api/application-id/by-identifier/${serviceSlug}`
        )
    );
  }
}

export const { resetApiApplication } = slice.actions;
export const { setApiLanguage } = slice.actions;
