import { createAsyncThunk, createSlice, current } from '@reduxjs/toolkit';

import { fetchWrapper } from '../_helpers/fetch-wrapper';

// create slice
const name = 'reports';
const initialState = createInitialState();
const extraActions = createExtraActions();

const slice = createSlice({
  name,
  initialState,
  reducers: {
    applicationTmp: (state, action) => {
      state.application_tmp = { ...action.payload };
    }
  },
  extraReducers: (builder) => {
    builder
      /** getAllReports **/
      .addCase(`${name}/getAllReports/pending`, (state, { meta }) => {
        const currentState = current(state);
        state.listReports = { ...currentState.listReports, loading: true };
      })
      .addCase(`${name}/getAllReports/fulfilled`, (state, action) => {
        state.listReports = action.payload;
      })
      .addCase(`${name}/getAllReports/rejected`, (state, action) => {
        state.listReports = { error: action.error };
      })

      /** getReportDetails **/
      .addCase(`${name}/getReportDetails/pending`, (state, { meta }) => {
        state.report = { loading: true };
      })
      .addCase(`${name}/getReportDetails/fulfilled`, (state, action) => {
        state.report = action.payload;
      })
      .addCase(`${name}/getReportDetails/rejected`, (state, action) => {
        state.report = { error: action.error };
      })

      /** getOthersReports **/
      .addCase(`${name}/getOthersReports/pending`, (state, { meta }) => {
        state.newItems = { loading: true };
      })
      .addCase(`${name}/getOthersReports/fulfilled`, (state, action) => {
        const currentState = current(state);
        const newItems = [...currentState.listReports.data, ...action.payload.data];
        state.listReports = { ...action.payload };
        state.listReports.data = newItems;
        state.newItems = { loading: false };
      })
      .addCase(`${name}/getOthersReports/rejected`, (state, action) => {
        state.newItems = { error: action.error };
      })

      /** getCategories **/
      .addCase(`${name}/getCategories/fulfilled`, (state, action) => {
        state.categories = action.payload;
      })
      .addCase(`${name}/getCategories/rejected`, (state, action) => {
        state.categories = { error: action.error };
      })

      /** addFiltersCategory **/
      .addCase(`${name}/addFiltersCategory/fulfilled`, (state, action) => {
        const currentState = current(state);
        let categories = [...currentState.filters.categories];
        categories.push(action.payload.category);
        state.filters = {
          categories: categories
        };
        let reports = currentState.listReports.data.filter((rep) => {
          return state.filters.categories.some((item) => {
            return rep.data.type
              ? item.value === (rep.data.type.value || rep.data.type.id) //|| rep.data.type
              : null;
          });
        });
        state.listReports = {
          meta: {
            count: reports.length
          },
          data: reports,
          links: null
        };
      })

      /** removeFiltersCategory **/
      .addCase(`${name}/removeFiltersCategory/fulfilled`, (state, action) => {
        const currentState = current(state);
        state.filters = {
          categories: currentState.filters.categories.filter(
            (item) => item.value !== action.payload.category.value
          )
        };
      })

      /** removeAllFiltersCategory **/
      .addCase(`${name}/removeAllFiltersCategory/fulfilled`, (state, action) => {
        state.filters = {
          categories: []
        };
      })

      /** getReportsGeoJson **/
      .addCase(`${name}/getReportsGeoJson/pending`, (state, { meta }) => {
        state.geoJson = { loading: true };
      })
      .addCase(`${name}/getReportsGeoJson/fulfilled`, (state, action) => {
        state.geoJson = action.payload;
      })
      .addCase(`${name}/getReportsGeoJson/rejected`, (state, action) => {
        state.geoJson = { error: action.error };
      })

      /** getReportsBoundingBoxGeoJson **/
      /*  addCase(`${name}/getReportsBoundingBoxGeoJson/pending`, (state, { meta }) => {
        state.geoJson = { loading: true };
      }) */ .addCase(`${name}/getReportsBoundingBoxGeoJson/fulfilled`, (state, action) => {
        state.geoJson = action.payload;
      })
      .addCase(`${name}/getReportsBoundingBoxGeoJson/rejected`, (state, action) => {
        state.geoJson = { error: action.error };
      })
      /** getStats **/
      .addCase(`${name}/getStats/fulfilled`, (state, action) => {
        state.stats = action.payload.meta.count;
      })
      /** getCategoryById **/
      .addCase(`${name}/getCategoryById/fulfilled`, (state, action) => {})
      .addCase(`${name}/getCategoryById/rejected`, (state, action) => {})
      /** getComments **/
      .addCase(`${name}/getComments/fulfilled`, (state, action) => {
        state.messages = action.payload;
      })
      .addCase(`${name}/updateComment/fulfilled`, (state, action) => {
        //const currentState = current(state);
        //const payload = [action.payload];
        /*   state.detailPost._embedded.comments = currentState.detailPost._embedded.comments.map(
        (obj) => payload.find((o) => o.id === obj.id) || obj
      ); */
      })
      .addCase(`${name}/updateComment/rejected`, (state, action) => {
        state.errors = { error: action.error };
      })
      .addCase(`${name}/createComment/pending`, (state, { meta }) => {
        //state.detailPost = { loading: true };
      })
      .addCase(`${name}/createComment/fulfilled`, (state, action) => {
        /// const currentState = current(state);
        //const payload = [action.payload];
      })
      .addCase(`${name}/createComment/rejected`, (state, action) => {
        state.errors = { error: action.error };
      })
      .addCase(`${name}/getHistory/fulfilled`, (state, action) => {
        state.history = action.payload;
      })
      .addCase(`${name}/getUserGroupId/fulfilled`, (state, action) => {
        state.user_group = action.payload;
      });
  }
});

// exports
export const reportsActions = { ...slice.actions, ...extraActions };
export const reportsReducer = slice.reducer;

// implementation

function createInitialState() {
  return {
    listReports: {
      data: [],
      loading: false,
      error: false,
      links: {},
      meta: {}
    },
    posts: {},
    newItems: null,
    categories: [],
    filters: {
      auth: false,
      categories: []
    },
    geoJson: [],
    stats: [],
    report: {},
    user_group: null,
    history: [],
    messages: {},
    errors: {},
    application_tmp: {}
  };
}

function createExtraActions() {
  const baseUrl = window.OC_BASE_URL || `${process.env.REACT_APP_API_URL}`;
  const categoriesUrl = window.OC_CATEGORIES_URL || `${process.env.REACT_APP_OC_CATEGORIES_URL}`;
  const serviceSlug = window.OC_SERVICE_IDENTIFIER || "inefficiencies";
  return {
    getAllReports: getAllReports(),
    getOthersReports: getOthersReports(),
    getCategories: getCategories(),
    getCategoryById: getCategoryById(),
    getStats: getStats(),
    filterPosts: filterPosts(),
    addFiltersCategory: addFiltersCategory(),
    removeFiltersCategory: removeFiltersCategory(),
    removeAllFiltersCategory: removeAllFiltersCategory(),
    getReportsGeoJson: getReportsGeoJson(),
    filterMyPost: filterMyPost(),
    getReportDetails: getReportDetails(),
    getImage: getImage(),
    getComments: getComments(),
    updateComment: updateComment(),
    createComment: createComment(),
    getReportsBoundingBoxGeoJson: getReportsBoundingBoxGeoJson(),
    getHistory: getHistory(),
    getUserGroupId: getUserGroupId()
  };

  function getAllReports() {
    return createAsyncThunk(
      `${name}/getAllReports`,
      async () =>
        await fetchWrapper.get(
          `${baseUrl}/api/applications?limit=20&sort=desc&version=2&service_identifier=${serviceSlug}`
        )
    );
  }

  function getImage() {
    return createAsyncThunk(
      `${name}/getImage`,
      async (params) => await fetchWrapper.get(`${params.url}`)
    );
  }

  function getReportDetails() {
    return createAsyncThunk(
      `${name}/getReportDetails`,
      async (params) => await fetchWrapper.get(`${baseUrl}/api/applications/${params.id}?version=2`)
    );
  }

  function getComments() {
    return createAsyncThunk(
      `${name}/getComments`,
      async (params) => await fetchWrapper.get(`${baseUrl}/api/applications/${params.id}/messages`)
    );
  }

  function getOthersReports() {
    return createAsyncThunk(
      `${name}/getOthersReports`,
      async (arg) => await fetchWrapper.get(`${arg.next}`)
    );
  }

  function getCategoryById() {
    return createAsyncThunk(
      `${name}/getCategoryById`,
      async (arg) => await fetchWrapper.get(`${baseUrl}/api/posts?categories=${arg}&limit=1`)
    );
  }

  function getCategories() {
    return createAsyncThunk(
      `${name}/getCategories`,
      async () =>
        await fetchWrapper.get(
          categoriesUrl,
          undefined,
          true,
          true
        )
    );
  }

  function getStats() {
    return createAsyncThunk(
      `${name}/getStats`,
      async (arg) =>
        await fetchWrapper.get(
          `${baseUrl}/api/applications?sort=desc&version=2&service_identifier=${serviceSlug}&status=2000`
        )
    );
  }

  function filterPosts() {
    return createAsyncThunk(
      `${name}/addFiltersPost`,
      async (arg) => await fetchWrapper.get(`${baseUrl}/api/applications?${arg}`)
    );
  }

  function addFiltersCategory() {
    return createAsyncThunk(`${name}/addFiltersCategory`, async (arg) => arg);
  }

  function removeFiltersCategory() {
    return createAsyncThunk(`${name}/removeFiltersCategory`, (arg) => arg);
  }

  function removeAllFiltersCategory() {
    return createAsyncThunk(`${name}/removeAllFiltersCategory`, (arg) => arg);
  }

  function filterMyPost() {
    return createAsyncThunk(`${name}/filterMyPost`, (arg) => arg);
  }

  function getReportsGeoJson() {
    return createAsyncThunk(`${name}/getReportsGeoJson`, async (arg) => {
      return await fetchWrapper.getGeoJson(
        `${baseUrl}/api/applications?limit=50&sort=desc&version=2&service_identifier=${serviceSlug}&${
          arg || ''
        }`
      );
    });
  }

  function getReportsBoundingBoxGeoJson() {
    return createAsyncThunk(`${name}/getReportsBoundingBoxGeoJson`, async (params) => {
      return await fetchWrapper.getGeoJson(
        `${baseUrl}/api/applications?bounding_box=${params.boundingBox}&version=2&service_identifier=${serviceSlug}`
      );
    });
  }

  function updateComment() {
    return createAsyncThunk(
      `${name}/updateComment`,
      async (params) =>
        await fetchWrapper.patch(
          `${baseUrl}/api/applications/${params.application}/messages/${params.comment}`,
          params.payload
        )
    );
  }

  function createComment() {
    return createAsyncThunk(
      `${name}/createComment`,
      async (params) =>
        await fetchWrapper.post(
          `${baseUrl}/api/applications/${params.application}/messages`,
          params.payload
        )
    );
  }

  function getHistory() {
    return createAsyncThunk(
      `${name}/getHistory`,
      async ({ idApplication }) =>
        await fetchWrapper.get(`${baseUrl}/api/applications/${idApplication}/history`)
    );
  }

  function getUserGroupId() {
    return createAsyncThunk(
      `${name}/getUserGroupId`,
      async ({ userGroupId }) => await fetchWrapper.get(`${baseUrl}/api/user-groups/${userGroupId}`)
    );
  }
}

export const { applicationTmp } = slice.actions;
