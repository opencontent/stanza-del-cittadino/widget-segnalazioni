import { configureStore } from '@reduxjs/toolkit';
import { authReducer } from './auth.slice';
import { apiReduced } from './api.slice';
import { currentUserReducer } from './currentuser.slice';
import { formReducer } from './form.slice';
import { reportsReducer } from './reports.slice';
import { uploadsReducer } from './uploads.slice';

export * from './auth.slice';
export * from './api.slice';
export * from './currentuser.slice';
export * from './form.slice';
export * from './uploads.slice';
export * from './reports.slice';

export const store = configureStore({
  reducer: {
    auth: authReducer,
    currentUser: currentUserReducer,
    form: formReducer,
    files: uploadsReducer,
    api: apiReduced,
    reports: reportsReducer
  }
  // here we restore the previously persisted state
  // preloadedState: loadState(),
});
