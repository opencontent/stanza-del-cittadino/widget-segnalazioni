import React from 'react';
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';
import { BrowserRouter, HashRouter } from 'react-router-dom';

import { store } from './_store';
import { App } from './App';
import './i18n';

const container = document.getElementById('oc-inefficiencies');
const root = createRoot(container);

root.render(
  <Provider store={store}>
    {window.OC_BASENAME ? (
      <BrowserRouter basename={window.OC_BASENAME || '/'}><App /></BrowserRouter>
    ) : (
      <HashRouter>
        <App />
      </HashRouter>
    )}
  </Provider>
);
