import { Chip, ChipLabel } from 'design-react-kit';
import { t } from 'i18next';

export { ButtonsStatus };

export { Status };

function ButtonsStatus({ status }) {
  if (status === '7000') {
    //Accettata
    return <ButtonStatusCompleted />;
  } else if (status === '4000') {
    // Presa in carico
    return <ButtonStatusPending />;
  } else if (status === '9000') {
    // Rifiutata
    return <ButtonStatusClose />;
  } else {
    return <ButtonStatusOpen />; // Aperta
  }
}


function Status({ status }) {
  if (status === 7000) {
    //Accettata
    return t('completed');
  } else if (status === 4000) {
    // Presa in carico
    return t('pending');
  } else if (status === 9000) {
    // Rifiutata
    return t('closed');
  }  else {
    return t('opened'); // Aperta
  }
}

function ButtonStatusPending() {
  return (
    <Chip color="warning" simple className={'m-0'}>
      <span className="visually-hidden">{t('status_report')}</span>
      <ChipLabel>{t('pending')}</ChipLabel>
    </Chip>
  );
}

function ButtonStatusCompleted() {
  return (
    <Chip color="success" simple className={'m-0'}>
      <span className="visually-hidden">{t('status_report')}</span>
      <ChipLabel>{t('completed')}</ChipLabel>
    </Chip>
  );
}

function ButtonStatusClose() {
  return (
    <Chip color="success" simple className={'m-0'}>
      <span className="visually-hidden">{t('status_report')}</span>
      <ChipLabel>{t('closed')}</ChipLabel>
    </Chip>
  );
}

function ButtonStatusOpen() {
  return (
    <Chip color="primary" simple className={'m-0'}>
      <span className="visually-hidden">{t('status_report')}</span>
      <ChipLabel>{t('opened')}</ChipLabel>
    </Chip>
  );
}
