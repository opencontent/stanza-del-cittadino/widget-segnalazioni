import { t } from 'i18next';

export { TextStatus };

function TextStatus({ point }) {
  if (point.status_code === 7000) {
    //Accettata
    return t('completed');
  } else if (point.status_code === 4000) {
    // Presa in carico
    return point.description;
  } else if (point.status_code === 9000) {
    // Rifiutata
    return t('rejected');
  } else if (point.status_code === 1900 || point.status_code === 2000) {
    return t('opened');
  } else {
    return t('opened'); // Aperta
  }
}
