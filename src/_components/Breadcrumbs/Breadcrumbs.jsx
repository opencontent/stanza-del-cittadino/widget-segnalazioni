import { Breadcrumb, BreadcrumbItem } from 'design-react-kit';
import { t } from 'i18next';
import { useId } from 'react';
import { Link } from 'react-router-dom';
import useBreadcrumbs from 'use-react-router-breadcrumbs';
import { routes } from '../../routes';

function Breadcrumbs() {
  const breadcrumbs = useBreadcrumbs(routes);
  const id = useId();

  return (
    <div className={'cmp-breadcrumbs d-print-none'}>
      <Breadcrumb>
        {breadcrumbs.map(({ match, breadcrumb }, index) => {
          return index === 0 ? (
            <BreadcrumbItem key={`${id}-${index}`}>
              <a href="/">{'Homepage'}</a>
              <span className="separator">/</span>
            </BreadcrumbItem>
          ) : index !== breadcrumbs.length - 1 ? (
            <BreadcrumbItem key={`${id}-${index}`}>
              <Link to={match.route.path}>
                {t(match.route.name)}
                <span className="separator">/</span>
              </Link>
            </BreadcrumbItem>
          ) : (
            <BreadcrumbItem active key={`${id}-${index}`}>
              {t(match.route.name)}
            </BreadcrumbItem>
          );
        })}
      </Breadcrumb>
    </div>
  );
}

export default Breadcrumbs;
