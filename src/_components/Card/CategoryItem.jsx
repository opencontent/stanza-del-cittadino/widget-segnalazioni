import { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { reportsActions } from '../../_store';

export { CategoryItem };

function CategoryItem({ category, setCallables, reset }) {
  const dispatch = useDispatch();
  const inputRef = useRef(null);
  const [isChecked, setIsChecked] = useState(false);
  const [countPost] = useState('');
  const { filters } = useSelector((x) => x.reports);
  const { i18n } = useTranslation();

  const handleFilterCategory = (category, event) => {
    if (event.currentTarget.checked) {
      setIsChecked(event.currentTarget.checked);
      dispatch(
        reportsActions.addFiltersCategory({
          category: category,
          i18n: i18n.resolvedLanguage
        })
      );
    } else {
      setIsChecked(event.currentTarget.checked);
      dispatch(
        reportsActions.removeFiltersCategory({
          category: category,
          i18n: i18n.resolvedLanguage
        })
      );
    }
  };

  useEffect(() => {
    if (reset) {
      setIsChecked(false);
      setCallables(false);
    }
  }, [reset, setCallables]);

  /*   useEffect(() => {
    if (props.props.id) {
      dispatch(reportsActions.getCategoryById(props.props.id))
        .unwrap()
        .then((res) => {
          setCountPost(res.count);
        });
    }
  }, [dispatch, props.props.id]); */

  return (
    <li>
      <div className="form-check">
        <div className="checkbox-body border-light my-3">
          <input
            type="checkbox"
            id={category.value}
            onChange={(event) =>
              handleFilterCategory({ label: category.label, value: category.value }, event)
            }
            name="category"
            value={category.value}
            checked={isChecked || filters.categories.toString().includes(category.value)}
            ref={inputRef}
            aria-label={category.label}
          />
          <label
            htmlFor={category.value}
            className="subtitle-small_semi-bold mb-0 category-list__list"
            aria-hidden="true"
          >
            {category.label} {countPost ? `(${countPost})` : ''}
          </label>
        </div>
      </div>
    </li>
  );
}
