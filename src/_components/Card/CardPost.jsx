//import { Splide } from '@splidejs/react-splide';
import dayjs from 'dayjs';
import {
  Accordion,
  AccordionBody,
  AccordionHeader,
  AccordionItem,
  Button,
  Card,
  CardBody,
  CardFooterCTA,
  CardHeader,
  CardTagsHeader,
  Icon
} from 'design-react-kit';
import { t } from 'i18next';
import { useState } from 'react';
import { getI18n } from "react-i18next";
import { useNavigate } from 'react-router-dom';
import { capitalizeFirstLetter } from '../../_helpers/utilities';
import { ButtonsStatus } from '../Button/ButtonStatus';
//import { ImageSlide } from '../Images/ImageSlide';
var LocalizedFormat = require( 'dayjs/plugin/localizedFormat')

dayjs.extend(LocalizedFormat)
require('dayjs/locale/de')
require('dayjs/locale/en')
require('dayjs/locale/it')


export { CardPost };

function CardPost({ report }) {
  const [collapseElementOpenCard, setCollapseElementCard] = useState('');

  let navigate = useNavigate();
  const routeChange = (id) => {
    let path = `/segnalazioni/${id}`;
    navigate(path);
  };

  return (
    <article
      style={{breakInside: 'avoid'}}
      className="cmp-card mb-4 mb-lg-30 navbar-custom" key={report.id}>
      <Card
        className="has-bkg-grey shadow-sm"
      >
        <CardBody className="p-0">
          <div className="cmp-info-button-card">
            <Card className="p-3 p-lg-4">
              <CardBody className="p-0">
                <CardTagsHeader
                  date={
                    t('send_at') + ' ' + dayjs(report.created_at).locale(getI18n().language).format('L LT')
                  }
                >
                  <ButtonsStatus status={report.status}></ButtonsStatus>
                </CardTagsHeader>
                <h2 className="h3 medium-title mb-0">{report?.data?.subject}</h2>
                <p className="card-info">
                  {t('type_report')} <br />{' '}
                  <span>
                    {report.data?.type
                      ? report.data.type.label
                      : report.data?.details
                      ? capitalizeFirstLetter(report.data.details)
                      : ''}
                  </span>
                </p>
                <Accordion>
                  <AccordionItem>
                    <AccordionHeader
                      active={collapseElementOpenCard === '1'}
                      onToggle={() =>
                        setCollapseElementCard(collapseElementOpenCard !== '1' ? '1' : '')
                      }
                    >
                      {collapseElementOpenCard !== '1' ? t('show_more') : t('show_less')}
                      <Icon
                        icon={'it-collapse'}
                        size={'sm'}
                        color={'primary'}
                        role="presentation"
                        focusable="false"
                        aria-hidden="true"></Icon>
                    </AccordionHeader>
                    <AccordionBody
                      className="accordion-collapse"
                      active={collapseElementOpenCard === '1'}
                      listClassName={'p-0'}
                    >
                      <div className="cmp-info-summary bg-white has-border">
                        <Card>
                          <CardHeader className="border-bottom border-light p-0 mb-0 d-flex justify-content-between d-flex justify-content-end">
                            <h3 className="h4 title-large-semi-bold mb-3">{t('contacts')}</h3>
                            <span className="d-none text-decoration-none">
                              <span className="text-button-sm-semi t-primary">
                                {collapseElementOpenCard !== '1' ? t('show_more') : t('show_less')}
                              </span>
                            </span>
                          </CardHeader>
                          <CardBody className="card-body p-0">
                            <div className="single-line-info border-light">
                              <div className="text-paragraph-small">{t('address')}</div>
                              <div className="border-light">
                                <p className="data-text">
                                  {report.data?.address?.display_name
                                    ? report.data?.address.display_name
                                    : '--'}
                                </p>
                              </div>
                            </div>
                            <div className="single-line-info border-light">
                              <div className="text-paragraph-small">{t('detail')}</div>
                              <div className="border-light">
                                  <p className="data-text richtext-wrapper pre">
                                    {report.data?.details ? report.data.details : '--'}
                                  </p>
                              </div>
                            </div>
                            {/*                   {report.data?.images?.length > 0 ? (
                              <div className="single-line-info border-light">
                                <div className="text-paragraph-small">{t('images')}</div>
                                <div className="border-light border-0">
                                  <div className="text-center mt-3">
                                    <Splide
                                      aria-label="reports carousel"
                                      options={{
                                        rewind: true,
                                        gap: '1rem'
                                      }}
                                    >
                                      {report.data.images.map((img, idx) => (
                                        <ImageSlide
                                          props={img}
                                          key={`image-${idx.toString()}`}
                                        ></ImageSlide>
                                      ))}
                                    </Splide>
                                  </div>
                                </div>
                              </div>
                            ) : null} */}
                          </CardBody>
                          <CardFooterCTA className={'mt-2'}>
                            <Button
                              outline
                              color="primary"
                              onClick={() => {
                                routeChange(report.id);
                              }}
                              className="mr-3"
                            >
                              {t('show_all')}
                            </Button>
                          </CardFooterCTA>
                        </Card>
                      </div>
                    </AccordionBody>
                  </AccordionItem>
                </Accordion>
              </CardBody>
            </Card>
          </div>
        </CardBody>
      </Card>
    </article>
  );
}
