import {
  Button,
  Col,
  Form,
  FormGroup,
  Icon,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Select,
  Spinner,
  TextArea
} from 'design-react-kit';
import { t } from 'i18next';
import { useEffect, useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { Trans } from 'react-i18next';
import { MapContainer, TileLayer } from 'react-leaflet';
import { useDispatch, useSelector } from 'react-redux';
import { apiActions } from '../../_store';
import { MapCustom } from '../Map/MapCustom';
import { ImageUpload } from '../Upload/ImageUpload';

export { ModalNewPost };

function ModalNewPost() {
  const [isOpen, toggleModal] = useState(false);
  const [isOpenMap, toggleModalMap] = useState(false);
  const [isOpenSubmitted, toggleOpenSubmitted] = useState(false);
  const [position, setPosition] = useState([]);
  const dispatch = useDispatch();

  const { types, zones, posts } = useSelector((x) => x.posts);

  const { images } = useSelector((x) => x.images);

  const { address } = useSelector((x) => x.detailPost);

  useEffect(() => {
    // dispatch(apiActions.getType());
    //dispatch(apiActions.getZone());
  }, [dispatch]);

  const optionsType = () => {
    if (types?.count > 0) {
      // Change key items for select input pattern
      return types.items.map(({ identifier: value, name: label, ...rest }) => ({
        value,
        label,
        ...rest
      }));
    }
    return [];
  };

  const optionsZone = () => {
    if (zones?.count > 0) {
      // Change key items for select input pattern
      return zones.items.map(({ id: value, name: label, ...rest }) => ({
        value,
        label,
        ...rest
      }));
    }
    return [];
  };

  const {
    handleSubmit,
    formState: { errors },
    control,
    reset,
    setValue,
    getValues
  } = useForm({
    defaultValues: {
      subject: '',
      address: '',
      type: '',
      description: '',
      zone: '',
      phone: ''
    }
  });
  const onSubmit = (data) => {
    let addr;
    if (address.hasOwnProperty('latitude')) {
      addr = { ...address };
    } else {
      addr = getValues('address');
    }

    data = {
      ...data,
      type: data.type.value,
      address: addr,
      images: images
    };

    return dispatch(apiActions.createPost(data)).then(() => {
      toggleModal(!isOpen);
      reset();
      toggleOpenSubmitted(!isOpenSubmitted);
      dispatch(apiActions.getAll());
    });
  };

  function getLocation() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        let lat = position.coords.latitude;
        let long = position.coords.longitude;
        setPosition([lat, long]);
        toggleModalMap(!isOpenMap);
      },
      (errors) => {
        console.log(errors);
      }
    );
  }

  function saveAddress() {
    toggleModalMap(!isOpenMap);
    dispatch(
      detailPostActions.getAddress({
        address: address.address,
        latitude: address.latitude,
        longitude: address.longitude
      })
    );
    setValue('address', address.address);
  }

  function closeModalAddress() {
    toggleModalMap(!isOpenMap);
    dispatch(
      detailPostActions.getAddress({
        address: ''
      })
    );
    setValue('address', '');
  }

  function onchangeAddress(e) {
    if (e.target.value === '') {
      dispatch(
        detailPostActions.getAddress({
          address: ''
        })
      );
    } else {
      setValue('address', e.target.value);
    }
  }

  return (
    <div>
      <Button
        className="btn btn btn-primary mobile-full py-3 mt-2 mb-4 mb-lg-0"
        color="primary"
        onClick={() => toggleModal(!isOpen)}
      >
        <span>{t('report_outage')}</span>
      </Button>

      <Modal
        keyboard={false}
        backdrop={'static'}
        isOpen={isOpen}
        toggle={() => toggleModal(!isOpen)}
        centered
        labelledBy="createPostModal"
        size={'lg'}
      >
        {posts?.loading ? (
          <Col lg={12} className="text-center p-5">
            <Spinner double active />
          </Col>
        ) : (
          <div>
            <ModalHeader
              tag={'h2'}
              toggle={() => toggleModal(!isOpen)}
              className={'title-small-semi-bold'}
            >
              {t('new_report')}
            </ModalHeader>

            <Form onSubmit={handleSubmit(onSubmit)}>
              <ModalBody>
                <div className="form-row p-3">
                  <div className="col-12">
                    <Controller
                      name="subject"
                      control={control}
                      rules={{ required: true }}
                      render={({ field }) => (
                        <Input
                          type="text"
                          label={t('title') + '*'}
                          placeholder={t('placeholder_title')}
                          invalid={errors.subject?.type === 'required' ? true : false}
                          infoText={
                            errors.subject?.type === 'required' ? t('required_field') : false
                          }
                          innerRef={field.ref}
                          value={field.value}
                          onChange={field.onChange}
                        />
                      )}
                    />
                  </div>
                  <FormGroup className="col-12 mb-5">
                    <div className="bootstrap-select-wrapper">
                      <label htmlFor="selectType">{t('type_report')}*</label>
                      <Controller
                        name="type"
                        control={control}
                        rules={{ required: true }}
                        render={({ field }) => (
                          <Select
                            id="selectType"
                            options={optionsType()}
                            placeholder={t('placeholder_type_report')}
                            aria-label={t('placeholder_type_report')}
                            invalid={errors.type?.type === 'required' ? true : false}
                            infoText={
                              errors.type?.type === 'required' ? t('required_field') : false
                            }
                            innerRef={field.ref}
                            value={field.value}
                            onChange={field.onChange}
                          />
                        )}
                      />
                      {errors.type?.type === 'required' ? (
                        <small className="invalid-feedback d-block form-text ">
                          {t('required_field')}
                        </small>
                      ) : null}
                    </div>
                  </FormGroup>
                  <FormGroup className="col-12 mb-5">
                    <div className="bootstrap-select-wrapper">
                      <label htmlFor="selectZone">{t('reporting_area')}*</label>
                      <Controller
                        name="zone"
                        control={control}
                        rules={{ required: true }}
                        render={({ field }) => (
                          <Select
                            id="selectZone"
                            options={optionsZone()}
                            placeholder={t('placeholder_reporting_area')}
                            aria-label={t('placeholder_reporting_area')}
                            innerRef={field.ref}
                            value={field.value}
                            onChange={field.onChange}
                          />
                        )}
                      />
                      {errors.zone?.type === 'required' ? (
                        <small className="invalid-feedback d-block form-text ">
                          {t('required_field')}
                        </small>
                      ) : null}
                    </div>
                  </FormGroup>
                  <FormGroup className="col-12 mb-0">
                    <Controller
                      name="address"
                      control={control}
                      rules={{ required: true }}
                      render={({ field }) => (
                        <>
                          <div className="form-group">
                            <div className="input-group">
                              <label htmlFor="input-group-1" className={'active'}>
                                {t('address_report')}*
                              </label>
                              <input
                                type="text"
                                className={'form-control'}
                                id="input-group-1"
                                ref={field.ref}
                                onChange={(e) => {
                                  field.onChange(e);
                                  onchangeAddress(e);
                                }}
                                value={field.value}
                                placeholder={t('placeholder_address_report')}
                              />
                              <div className="input-group-append">
                                <Button
                                  className="btn"
                                  type="button"
                                  color={'primary'}
                                  onClick={() => getLocation()}
                                >
                                  <Icon
                                    icon="it-map-marker-circle"
                                    aria-hidden
                                    size="sm"
                                    color={'light'}
                                  />
                                </Button>
                              </div>
                            </div>
                            {errors.address?.type === 'required' ? (
                              <small className="invalid-feedback d-block form-text ">
                                {t('required_field')}
                              </small>
                            ) : null}
                          </div>
                        </>
                      )}
                    />
                  </FormGroup>
                  <FormGroup className={'col-12 mb-0'}>
                    <Controller
                      name="phone"
                      control={control}
                      rules={{ required: true }}
                      render={({ field }) => (
                        <Input
                          label={t('phone_number') + '*'}
                          placeholder={t('placeholder_phone_number')}
                          type={'tel'}
                          invalid={errors.phone?.type === 'required' ? true : false}
                          infoText={errors.phone?.type === 'required' ? t('required_field') : false}
                          innerRef={field.ref}
                          value={field.value}
                          onChange={field.onChange}
                        />
                      )}
                    />
                  </FormGroup>
                  <div className={'col-12 mb-0'}>
                    <Controller
                      name="description"
                      control={control}
                      rules={{ required: true }}
                      render={({ field }) => (
                        <TextArea
                          className={'border-none my-3'}
                          label={t('description') + '*'}
                          rows={3}
                          placeholder={t('placeholder_description')}
                          invalid={errors.description?.type === 'required' ? true : false}
                          infoText={
                            errors.description?.type === 'required' ? t('required_field') : false
                          }
                          innerRef={field.ref}
                          value={field.value}
                          onChange={field.onChange}
                        />
                      )}
                    />
                  </div>
                  <FormGroup className={'col-12 my-3'}>
                    <p>
                      <strong>{t('label_upload_images')}</strong>
                    </p>
                    <ImageUpload></ImageUpload>
                  </FormGroup>
                </div>
              </ModalBody>
              <ModalFooter>
                <Row className={'w-100'}>
                  <Col xs={12} lg={6}>
                    <Button
                      color="secondary"
                      outline
                      className={'w-100'}
                      onClick={() => toggleModal(!isOpen)}
                    >
                      {t('close')}
                    </Button>
                  </Col>
                  <Col xs={12} lg={6}>
                    <Button color="primary" className={'w-100'} type={'submit'}>
                      {t('send_report')}
                    </Button>
                  </Col>
                </Row>
              </ModalFooter>
            </Form>
          </div>
        )}
      </Modal>

      <Modal
        keyboard={false}
        backdrop={'static'}
        isOpen={isOpenMap}
        toggle={() => toggleModalMap(!isOpenMap)}
        centered
        labelledBy="modalMap"
      >
        <ModalHeader toggle={() => toggleModalMap(!isOpenMap)} id="modalMap">
          {t('select_map_point')}
        </ModalHeader>
        <ModalBody>
          <MapContainer
            center={position}
            zoom={20}
            scrollWheelZoom={false}
            style={{ height: '500px' }}
          >
            <MapCustom position={position} draggable={true} />
            <TileLayer
              attribution='&copy; <a target="_blank" href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
          </MapContainer>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={() => closeModalAddress()}>
            {t('close')}
          </Button>
          <Button color="primary" onClick={() => saveAddress()}>
            {t('save_address')}
          </Button>
        </ModalFooter>
      </Modal>

      <Modal
        isOpen={isOpenSubmitted}
        toggle={() => toggleOpenSubmitted(!isOpenSubmitted)}
        centered
        labelledBy="modalSubmit"
      >
        <ModalHeader
          toggle={() => toggleOpenSubmitted(!isOpenSubmitted)}
          id="modalSubmit"
        ></ModalHeader>
        <ModalBody>
          <Row className="justify-content-center mb-50">
            <Col className="col-12 pb-2" lg={10}>
              <div className="cmp-heading p-0">
                <div className="categoryicon-top d-flex">
                  <Icon color="success" icon="it-check-circle" className={' mr-10 icon-sm mb-1'} />
                  <h1 className="title-xxxlarge">{t('report_sent')}</h1>
                </div>
                <p className="subtitle-small">
                  <Trans t={t} i18nKey={'thanks_message_report_sent'}></Trans>
                </p>
                <p className="subtitle-small">
                  <Trans t={t} i18nKey={'thanks_message_report_sent_description'}></Trans>
                </p>
              </div>
            </Col>
          </Row>
        </ModalBody>
      </Modal>
    </div>
  );
}
