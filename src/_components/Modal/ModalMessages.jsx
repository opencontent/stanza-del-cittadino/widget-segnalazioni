import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'design-react-kit';
import { t } from 'i18next';

export { ModalMessages };

function ModalMessages({ isOpen, toggleModal, message }) {
  return (
    <div>
      <Modal
        isOpen={isOpen}
        toggle={() => toggleModal(!isOpen)}
        withIcon
        labelledBy="error-modal-message"
      >
        <ModalHeader icon="it-info-circle" id="error-modal-message">
          {t('warning')}
        </ModalHeader>
        <ModalBody>
          <p>{message}</p>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={() => toggleModal(!isOpen)}>
            {t('close')}
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}
