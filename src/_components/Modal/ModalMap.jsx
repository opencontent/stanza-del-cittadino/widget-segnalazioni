import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'design-react-kit';
import { t } from 'i18next';
import { useEffect, useState } from 'react';
import { MapContainer, TileLayer } from 'react-leaflet';
import { useDispatch, useSelector } from 'react-redux';
import { apiActions, storeAddress } from '../../_store';
import { MapCustom } from '../Map/MapCustom';

export { ModalMap };

function ModalMap({ isOpen, toggleModal, position }) {

  const dispatch = useDispatch();

  const { address } = useSelector((x) => x.api);

  function saveAddress() {
    toggleModal(!isOpen);
    dispatch(apiActions.getAddress({ ...address }));
    //  setValue('address', address);
    dispatch(storeAddress(address));
  }

  function closeModalAddress() {
    toggleModal(!isOpen);
    dispatch(apiActions.getAddress(''));
    //   setValue('address', '');
  }

  return (
    <Modal
      keyboard={false}
      backdrop={'static'}
      isOpen={isOpen}
      toggle={() => toggleModal(!isOpen)}
      centered
      labelledBy="modalMap"
    >
      <ModalHeader toggle={() => toggleModal(!isOpen)} id="modalMap">
        {t('select_map_point')}
      </ModalHeader>
      <ModalBody>
        <MapContainer
          center={position}
          zoom={20}
          scrollWheelZoom={false}
          style={{ height: '500px' }}
        >
          <MapCustom position={position} draggable={true} />
          <TileLayer
            attribution='&copy; <a target="_blank" href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
        </MapContainer>
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={() => closeModalAddress()}>
          {t('close')}
        </Button>
        <Button color="primary" onClick={() => saveAddress()}>
          {t('save_address')}
        </Button>
      </ModalFooter>
    </Modal>
  );
}
