import useAuth from "../_hooks/useAuth";
import {Navigate, useNavigate} from "react-router-dom";
import {useEffect} from "react";
import {useAuthContext} from "../context/AuthProvider";


export function NoAuthRoute2({ children }) {

  const {isLogged} = useAuth()
  const { setSharedState,sharedState } = useAuthContext();
  function Redirect({ to }) {
    let navigate = useNavigate();
    useEffect(() => {
      navigate(to);
    });
    return null;
  }

  if(isLogged){
    return children
  }else {
    if(sharedState?.anonym === true || sharedState?.anonym === false){
        return children
    }else{
        return <Navigate to={'/login'}/>
    }
  }

}
