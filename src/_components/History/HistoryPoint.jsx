import dayjs from 'dayjs';
import { getI18n } from "react-i18next";
import { TextStatus } from '../Button/TextStatus';

export { HistoryPoint };

function HistoryPoint({ point }) {
  return (
    <>
      {point.status_code > 1900 ? (
        <div className="point-list">
          <div className="point-list-aside point-list-primary p-0"></div>
          <div className="point-list-content ms-3">
            <div className="card card-teaser rounded p-2">
              <div className="card-body">
                <strong>
                  <TextStatus point={point}></TextStatus>
                </strong>
                <small className="d-block">
                  <time dateTime={dayjs(point.date).locale(getI18n().language).format()}>
                    {dayjs(point.date).locale(getI18n().language).format('L LT')}
                  </time>
                </small>
              </div>
            </div>
          </div>
        </div>
      ) : null}
    </>
  );
}
