import React from 'react';
import { Icon } from 'design-react-kit';

export default function ExternalRef({ data }) {
  if (!data) {
    return null;
  }
  const { title, text, channels } = data;

  const iconMap = {
    web: 'it-external-link',
    email: 'it-mail',
    phone: 'it-telephone',
    ios: 'it-external-link',
    android: 'it-external-link'
  };

  return (
    <div className="card-body p-0">
      <div className="cmp-info-button-card mt-3">
        <div className="card p-3 p-lg-4">
          <div className="card-body p-0">
            <div className="d-flex">
              <h4 className="mb-1">{title}</h4>
            </div>
            <p className="subtitle-small mb-0 pre">{text}</p>
            {channels.map((ch, i) => (
              <p key={i} className={'mt-3'}>
                <a
                  className={'list-item active icon-left p-0 cursor-pointer nav-link'}
                  href={ch.href}
                  target="_blank"
                  rel="nofollow noreferrer noopener">
                  <Icon icon={iconMap[ch.type]} size={'sm'} color={'primary'}></Icon>
                  {ch.label}
                </a>
              </p>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
