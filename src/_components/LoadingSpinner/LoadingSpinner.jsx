import React from 'react';
import './spinner.css';
import { Icon } from 'design-react-kit';

export default function LoadingSpinner() {
  return (
    <div className="loading-spinner">
      <Icon icon={'it-refresh'} size={'sm'} color={'primary'} className={'icon-white'}></Icon>
    </div>
  );
}
