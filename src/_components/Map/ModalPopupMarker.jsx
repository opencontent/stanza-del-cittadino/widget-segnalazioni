import dayjs from 'dayjs';
import { Button, Col, Modal, ModalBody, ModalFooter, ModalHeader } from 'design-react-kit';
import { t } from 'i18next';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import shortid from 'shortid';
import { reportsActions } from '../../_store';
import { ButtonsStatus } from '../Button/ButtonStatus';
import { ImageHeader } from '../Images/ImageHeader';

export const ModalPopupMarker = ({ show, onClose, properties }) => {
  const key = shortid.generate();
  const dispatch = useDispatch();
  const { report } = useSelector((x) => x.reports);

  useEffect(() => {
    if (properties?.id) {
      dispatch(reportsActions.getReportDetails({ id: properties.id })).unwrap();
    }
  }, [dispatch, properties.id]);

  return (
    <Modal isOpen={show} toggle={() => onClose(!show)} align="center" scrollable labelledBy={key}>
      <ModalHeader toggle={() => onClose(!show)} id={key} tag={'h4'}>
        <span className={'modal-title title-small-semi-bold'}>
          {t('status_report')}
          {report && report.status ? <ButtonsStatus status={report?.status}></ButtonsStatus> : '--'}
        </span>
      </ModalHeader>
      <ModalBody>
        <div className="border-bottom border-light">
          <h3 className="title-xsmall border-light pt-2">{t('title')}</h3>
          <p className="subtitle-small pb-2">
            {report?.data?.subject ? report?.data?.subject : '--'}
          </p>
        </div>
        <div className="border-bottom border-light">
          <h3 className="title-xsmall border-light pt-2">{t('report_type')}</h3>
          <p className="subtitle-small pb-2">
            {report?.data?.type?.hasOwnProperty('name') ||
            report?.data?.type?.hasOwnProperty('label')
              ? report?.data?.type?.name || report?.data?.type?.label
              : report?.data?.type
              ? report?.data?.type
              : '--'}
          </p>
        </div>
        <div className="border-bottom border-light">
          <h3 className="title-xsmall border-light pt-2">{t('published_at')}</h3>
          <p className="subtitle-small pb-2">
            {dayjs(report?.created_at).format('DD/MM/YYYY  HH:mm')}
          </p>
        </div>
        <div className="border-bottom border-light">
          <h3 className="title-xsmall border-light pt-2">{t('address')}</h3>
          <p className="subtitle-small pb-2">
            {report?.data?.address ? report.data.address.display_name : '--'}
          </p>
        </div>
        <div className="border-bottom border-light">
          <h3 className="title-xsmall border-light pt-2">{t('detail')}</h3>
          <p className="subtitle-small pb-2">
            {report?.data?.details ? report.data.details : '--'}
          </p>
        </div>
        {report?.data?.images?.length ? (
          <div>
            <h3 className="title-xsmall border-light pt-2">{t('images')}</h3>
            <Col className="d-lg-flex gap-2 row">
              {report?.data?.images.map((img, idx) => (
                <ImageHeader props={img} key={`image-${idx.toString()}`}></ImageHeader>
              ))}
            </Col>
          </div>
        ) : null}
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={() => onClose(!show)} outline className={'w-100'}>
          {t('close')}
        </Button>
      </ModalFooter>
    </Modal>
  );
};
