import React, { useEffect, useRef, useState } from 'react';
import { GeoJSON, LayerGroup, LayersControl, TileLayer, useMap, useMapEvent } from 'react-leaflet';
import iconMarker from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import L from 'leaflet';
import { useDispatch } from 'react-redux';
import { apiActions, reportsActions } from '../../_store';
import { DraggableMarker } from './DraggableMarker';
import { ModalPopupMarker } from './ModalPopupMarker';
//import MarkerClusterGroup from "react-leaflet-cluster";

const icon = L.icon({
  iconRetinaUrl: iconMarker,
  iconUrl: iconMarker,
  shadowUrl: iconShadow,
  iconSize: [25, 41],
  iconAnchor: [13, 41],
  popupAnchor: [1, -34]
});

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

const iconCustom = ({ count }) => {
  const classSize =
    count < 100
      ? 'marker-cluster-small'
      : count < 200
      ? 'marker-cluster-medium'
      : count >= 500
      ? 'marker-cluster-large'
      : 'marker-cluster';
  return L.divIcon({
    className: `leaflet-marker-icon marker-cluster ${classSize} leaflet-zoom-animated leaflet-interactive`,
    html: `<div class="m-0"><span>${count}</span></div>`,
    iconSize: [30, 30]
  });
};

export const MapCustom = ({ position, geoJson, draggable, layers }) => {
  // Set up the useMap hook in the descendant of MapContainer
  const map = useMap();
  const dispatch = useDispatch();
  const refGeoJson = useRef(null);
  const refGeographicAreas = useRef(null);
  //const refMarkerGroup = useRef(null);
  const [hasGeoJson, setHasGeoJson] = useState(false);
  const [dataMarker, setDataMarker] = useState(null);

  const [show, setShow] = useState(false);
  const [geographicAreas, setGeographicAreas] = useState([]);

  const handleClose = () => setShow(false);

  const pointToLayer = (feature, latlng) => {
    if (feature?.properties?.hasOwnProperty('point_number')) {
      return L.marker(latlng, {
        icon: iconCustom({ count: feature.properties.point_number }),
        interactive: false
      }).off('click');
    } else {
      return L.marker(latlng, { icon: icon });
    }
  };

  // Prevent unused polygon
  const pointToLayerPolygon = (feature, latlng) => {
    return;
  };

  /*   const Popup = ({ feature }) => {
    return (
      <div>
        <b>{feature.properties.type.name}</b>
        <br />
        <Badge color={'primary'}>{feature.properties.status.name}</Badge>
        <br />
        <span> del {dayjs(feature.properties.published_at).format('DD/MM/YYYY  HH:mm')}</span>
        <p className={'mt-0'}>{feature.properties.subject}</p>
      </div>
    );
  }; */

  const onEachFeature = (feature, layer) => {
    layer.on('click', function () {
      setDataMarker(feature);
      setShow(true);
    });
  };

  const getGeoJson = (data = null) => {
    if (data) {
      return data;
    }
    return geoJson;
  };

  function mapMoveEnd() {
    if (refGeoJson.current) {
      // Prevent multiple api call
      map.off('moveend', mapMoveEnd);
      map.doubleClickZoom.disable();

      const center = map.getCenter();
      const zoom = map.getZoom();

      const calcBoundingZoom = getScaledBounds(center, zoom);
      const bb = `${calcBoundingZoom._southWest.lng},${calcBoundingZoom._southWest.lat},${calcBoundingZoom._northEast.lng},${calcBoundingZoom._northEast.lat}`;

      dispatch(reportsActions.getReportsBoundingBoxGeoJson({ boundingBox: bb }))
        .unwrap()
        .then((res) => {
          if (map.hasLayer(refGeoJson.current)) {
            map.removeLayer(refGeoJson.current);
          }
          const ref = refGeoJson.current.clearLayers().addData(res);
          map.addLayer(ref);
          // refMarkerGroup.current.refreshClusters()

          map.doubleClickZoom.enable();
        });
    }
  }

  function getScaledBounds(center, zoom) {
    const bounds = map.getPixelBounds(center, zoom);
    const sw = map.unproject(bounds.getBottomLeft(), zoom);
    const ne = map.unproject(bounds.getTopRight(), zoom);
    return L.latLngBounds(sw, ne);
  }

  useEffect(() => {
    // Access the map instance:
    map.whenReady(() => {
      map.invalidateSize();
      if (geoJson) {
        setHasGeoJson(true);
        addLayers();
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [map]);

  useMapEvent('moveend', () => {
    mapMoveEnd();
  });

  useMapEvent('overlayadd', () => {
    addLayers();
  });

  async function addLayers() {
    if (layers.length > 0) {
      const getPromisesLayers = [];
      refGeographicAreas.current.clearLayers();
      for (const layer of layers) {
        getPromisesLayers.push(dispatch(apiActions.getGeographicAreasById({ id: layer })).unwrap());
      }
      await Promise.all(getPromisesLayers).then((response) => {
        response.forEach((l) => {
          const geofence = JSON.parse(l.geofence);
          setGeographicAreas(geofence);
          refGeographicAreas?.current?.addData(geofence);
        });
        let bounds = L.latLngBounds(refGeographicAreas?.current);
        if (bounds) {
          map.fitBounds(refGeographicAreas.current.getBounds());
        }
      });
    }
  }

  return (
    <div className='d-print-none'>
      <TileLayer
        attribution='&copy; <a target="_blank" href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {layers ? (
        <LayersControl position="topright">
          <LayersControl.Overlay name="Aree geografiche" checked>
            <LayerGroup>
              <GeoJSON
                data={geographicAreas}
                ref={refGeographicAreas}
                pointToLayer={pointToLayerPolygon}
              />
            </LayerGroup>
          </LayersControl.Overlay>
        </LayersControl>
      ) : null}
      {position && position.length ? (
        <DraggableMarker position={position} icon={icon} draggable={draggable}></DraggableMarker>
      ) : null}

      {hasGeoJson ? (
        /*         <MarkerClusterGroup chunkedLoading
                               ref={refMarkerGroup}
            animateAddingMarkers={true}
                                //onClick={(e) => console.log('onClick', e)}
                               // iconCreateFunction={createClusterCustomIcon}
                                maxClusterRadius={100}
                                spiderfyOnMaxZoom={true}
                                 polygonOptions={{
                                   fillColor: '#ffffff',
                                   color: '#f00800',
                                   weight: 3,
                                   opacity: 1,
                                   fillOpacity: 0.8,
                                 }}
                                showCoverageOnHover={true}
            > */
        <GeoJSON
          data={getGeoJson(null)}
          pointToLayer={pointToLayer}
          onEachFeature={onEachFeature}
          ref={refGeoJson}
        />
      ) : //  </MarkerClusterGroup>
      null}
      {dataMarker && show ? (
        <ModalPopupMarker
          show={show}
          onClose={handleClose}
          properties={dataMarker}
        ></ModalPopupMarker>
      ) : null}
    </div>
  );
};
