import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { Marker, Popup } from 'react-leaflet';
import { useDispatch } from 'react-redux';
import { apiActions } from '../../_store';
import { locationNameFormatter } from '_helpers/utilities';

export function DraggableMarker({ icon, draggable, position }) {
  const [draggableState] = useState(draggable);
  const [positionGet, setPosition] = useState(position);
  const [address, setAddress] = useState('');
  const markerRef = useRef(null);
  const dispatch = useDispatch();

  const getAddress = useCallback(() => {
    const marker = markerRef.current;
    if (marker != null) {
      setPosition([marker.getLatLng().lat, marker.getLatLng().lng]);
      fetch(
        `https://${
          window.OC_MAP_REVERSE_PROVIDER || process.env.REACT_APP_MAP_REVERSE_PROVIDER
        }/reverse?format=geocodejson&lat=${marker.getLatLng().lat}&lon=${marker.getLatLng().lng}`
      ).then((response) => {
        response.json().then((data) => {
          const dataMap = {
            latitude: marker.getLatLng().lat,
            longitude: marker.getLatLng().lng,
            value: data.features[0].properties.geocoding.place_id,
            display_name: data.features[0].properties.geocoding.label,
            label: data.features[0].properties.geocoding.label,
            place_id: data.features[0].properties.geocoding.place_id,
            licence: data.geocoding.attribution,
            osm_type: data.features[0].properties.geocoding.osm_type,
            osm_id: data.features[0].properties.geocoding.osm_id,
            lat: marker.getLatLng().lat,
            lon: marker.getLatLng().lng,
            type: data.features[0].properties.geocoding.type,
            address: {
              name: data.features[0].properties.geocoding.name,
              road: data.features[0].properties.geocoding.street,
              house_number: data.features[0].properties.geocoding.housenumber,
              city: data.features[0].properties.geocoding.city,
              county: data.features[0].properties.geocoding.county,
              state: data.features[0].properties.geocoding.state,
              country: data.features[0].properties.geocoding.country,
              pedestrian: data.features[0].properties.geocoding.locality,
              suburb: data.features[0].properties.geocoding.district
            }
          };
          dataMap.label = locationNameFormatter(dataMap);
          dataMap.display_name = dataMap.label;
          setAddress(dataMap.label);
          dispatch(apiActions.getAddress(dataMap));
          marker.openPopup();
        });
      });
    }
  }, [dispatch]);

  // usual useEffect that'll be triggered on component load, only one time
  useEffect(() => {
    getAddress();
  }, [getAddress]);

  const eventHandlers = useMemo(
    () => ({
      dragend() {
        getAddress();
      },
      click(e) {
        getAddress();
      }
    }),
    [getAddress]
  );

  return (
    <Marker
      icon={icon}
      draggable={draggableState}
      eventHandlers={eventHandlers}
      position={positionGet}
      ref={markerRef}
    >
      {address ? (
        <Popup minWidth={90}>
          <span>{address}</span>
        </Popup>
      ) : null}
    </Marker>
  );
}
