import React, { useEffect, useRef, useState } from 'react';
import { GeoJSON, LayerGroup, LayersControl, TileLayer, useMap, useMapEvent } from 'react-leaflet';
import iconMarker from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import L from 'leaflet';
import { useDispatch } from 'react-redux';
import { apiActions } from '../../_store';
import { DraggableMarker } from './DraggableMarker';
import { ModalPopupMarker } from './ModalPopupMarker';

const icon = L.icon({
  iconRetinaUrl: iconMarker,
  iconUrl: iconMarker,
  shadowUrl: iconShadow,
  iconSize: [25, 41],
  iconAnchor: [13, 41],
  popupAnchor: [1, -34]
});

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

export const MapLayers = ({ position, draggable, layers, geoJson }) => {
  // Set up the useMap hook in the descendant of MapContainer
  const map = useMap();
  const dispatch = useDispatch();
  const refGeographicAreas = useRef(null);

  const [dataMarker] = useState(null);

  const [show, setShow] = useState(false);
  const [geographicAreas, setGeographicAreas] = useState([]);

  const handleClose = () => setShow(false);

  const pointToLayerPolygon = (feature, latlng) => {
    return;
  };

  useEffect(() => {
    // Access the map instance:
    map.whenReady(() => {
      map.invalidateSize();
      addLayers();
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [map]);

  useMapEvent('overlayadd', () => {
    addLayers();
  });

  function addLayers() {
    if (layers.length > 0) {
      refGeographicAreas.current.clearLayers();
      layers.forEach((id) => {
        dispatch(apiActions.getGeographicAreasById({ id }))
          .unwrap()
          .then((response) => {
            const geofence = JSON.parse(response.geofence);
            setGeographicAreas(geofence);
            refGeographicAreas.current.addData(geofence);
          });
      });
      setTimeout(() => {
        map.setView([position[0], position[1]], 16);
      }, 300);
    }
  }

  return (
    <>
      <TileLayer
        attribution='&copy; <a target="_blank" href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <LayersControl position="topright">
        <LayersControl.Overlay name="Aree geografiche" checked>
          <LayerGroup>
            <GeoJSON
              data={geographicAreas}
              ref={refGeographicAreas}
              pointToLayer={pointToLayerPolygon}
            />
          </LayerGroup>
        </LayersControl.Overlay>
      </LayersControl>

      {position && position.length ? (
        <DraggableMarker position={position} icon={icon} draggable={draggable}></DraggableMarker>
      ) : null}

      {dataMarker && show ? (
        <ModalPopupMarker
          show={show}
          onClose={handleClose}
          properties={dataMarker}
        ></ModalPopupMarker>
      ) : null}
    </>
  );
};
