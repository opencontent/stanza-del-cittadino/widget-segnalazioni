import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useSelector } from 'react-redux';


export function NoAuthRoute({ children }) {

  const { currentUser } = useSelector((x) => x.currentUser);
  const [loading, setLoading] = useState(false)
  const location = useLocation()

  function Redirect({ to }) {
    let navigate = useNavigate();
    useEffect(() => {
      navigate(to);
    });
    return null;
  }

  useEffect(() => {
   setTimeout(() => {
     setLoading(true)
    }, 1100);
  }, [currentUser]);


 if(loading){
   if((Object.keys(currentUser).length === 0 || currentUser?.isAnonym) && window.location.href.includes('segnala_disservizio') && location?.state?.anonym === true){
     return children
   } else if(Object.keys(currentUser).length > 0){
     return children
   }
   else {
     return <Redirect to={`/login`} />;
   }
 }

}
