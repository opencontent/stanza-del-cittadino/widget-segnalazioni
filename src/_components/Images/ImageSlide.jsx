import { SplideSlide } from '@splidejs/react-splide';
import { Col, Spinner, Icon } from 'design-react-kit';
import { useEffect, useState } from 'react';
import Zoom from 'react-medium-image-zoom';
import { useSelector } from 'react-redux';
import { t } from 'i18next';
import shortid from 'shortid';

export { ImageSlide };

function ImageSlide(props) {
  const [img, setImg] = useState(null);
  const imageUrl = props.props;
  const key = shortid.generate();
  const { token } = useSelector((x) => x.auth);

  useEffect(() => {
    const fetchImage = async () => {
      const res = await fetch('https://flyimg.opencityitalia.it/upload/o_auto/' + imageUrl.url, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
      const imageBlob = await res.blob();
      const imageObjectURL = URL.createObjectURL(imageBlob);
      setImg(imageObjectURL);
    };

    fetchImage();
  }, [imageUrl, token]);

  if (imageUrl) {
    return (
      <SplideSlide>
        <Zoom
          a11yNameButtonZoom={t('expand_image')}
          a11yNameButtonUnzoom={t('reduce_image')}
          IconZoom={() => <Icon icon={'it-zoom-in'} size={'sm'} color={'white'}></Icon>}
          IconUnzoom={() => <Icon icon={'it-close'} size={'md'} color={'white'}></Icon>}>
          {img ? (
            <img
              src={img ? img : ''}
              alt={'image-' + key}
              className="img-fluid mb-3 mb-lg-0 d-print-block"
              style={{ maxHeight: '250px' }}
            />
          ) : (
            <Col className="text-center p-5">
              <Spinner double active />
            </Col>
          )}
        </Zoom>
      </SplideSlide>
    );
  } else {
    return (
      <Col className="text-center p-5">
        <Spinner double active />
      </Col>
    );
  }
}
