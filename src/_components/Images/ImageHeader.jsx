import { Col, Spinner } from 'design-react-kit';
import { useEffect, useState } from 'react';
import Zoom from 'react-medium-image-zoom';
import { useSelector } from 'react-redux';
import shortid from 'shortid';

export { ImageHeader };

function ImageHeader(props) {
  const [img, setImg] = useState(null);
  const imageUrl = props.props;
  const key = shortid.generate();
  const { token } = useSelector((x) => x.auth);

  useEffect(() => {
    const fetchImage = async () => {
      const res = await fetch(imageUrl.url, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
      const imageBlob = await res.blob();
      const imageObjectURL = URL.createObjectURL(imageBlob);
      setImg(imageObjectURL);
    };

    fetchImage();
  }, [imageUrl, token]);

  if (imageUrl) {
    return (
      <Zoom IconUnzoom={() => <>-</>}>
        <div className={'col'}>
          {img ? (
            <img
              src={img ? img : ''}
              alt={'image-' + key}
              className="img-fluid mb-3 mb-lg-0"
              style={{ maxHeight: '300px' }}
            />
          ) : (
            <Col className="text-center p-5">
              <Spinner double active />
            </Col>
          )}
        </div>
      </Zoom>
    );
  } else {
    return (
      <Col className="text-center p-5">
        <Spinner double active />
      </Col>
    );
  }
}
