import { Col, Spinner, Icon } from 'design-react-kit';
import { useEffect, useState } from 'react';
import Zoom from 'react-medium-image-zoom';
import { useSelector } from 'react-redux';
import shortid from 'shortid';
import { t } from 'i18next';

export { ImageThumb };

function ImageThumb({ imageUrl }) {
  const [img, setImg] = useState(null);
  const key = shortid.generate();
  const { token } = useSelector((x) => x.auth);

  useEffect(() => {
    const fetchImage = async () => {
      const res = await fetch(imageUrl, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
      const imageBlob = await res.blob();
      const imageObjectURL = URL.createObjectURL(imageBlob);
      setImg(imageObjectURL);
    };

    fetchImage();
  }, [imageUrl, token]);

  if (imageUrl) {
    return (
      <Zoom
        a11yNameButtonZoom={t('expand_image')}
        a11yNameButtonUnzoom={t('reduce_image')}
        IconZoom={() => <Icon icon={'it-zoom-in'} size={'sm'} color={'white'}></Icon>}
        IconUnzoom={() => <Icon icon={'it-close'} size={'md'} color={'white'}></Icon>}>
        {img ? (
          <img
            src={img ? img : ''}
            alt={'image-' + key}
            className="img-fluid mb-3 mb-lg-0"
            style={{ maxHeight: '100px' }}
          />
        ) : (
          <Col className="text-center p-5">
            <Spinner double active />
          </Col>
        )}
      </Zoom>
    );
  } else {
    return (
      <Col className="text-center p-5">
        <Spinner double active />
      </Col>
    );
  }
}
