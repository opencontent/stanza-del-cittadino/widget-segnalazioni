import {Navigate} from 'react-router-dom';
import {useSelector} from "react-redux";
import useAuth from "../_hooks/useAuth";
import {useAuthContext} from "../context/AuthProvider";

export function PrivateRoute({ children }) {
  const { currentUser } = useSelector((x) => x.currentUser);
  const { setSharedState,sharedState } = useAuthContext();

  if (currentUser.isAnonym || sharedState?.anonym === true) {
    return <Navigate to="/login" />;
  } else{
    return children;
  }

}
