import { useState } from 'react';
import { UploadComponent } from './UploadComponent';

export { ImageUpload };

function ImageUpload(props) {
  const [size] = useState(3);

  if (size >= 3) {
    return (
      <ul className="upload-pictures-wall">
        {[...Array(size)].map((x, i) => (
          <UploadComponent key={i} />
        ))}
      </ul>
    );
  }
}
