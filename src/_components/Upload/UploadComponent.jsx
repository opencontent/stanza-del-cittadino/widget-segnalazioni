import { Icon } from 'design-react-kit';
import { t } from 'i18next';
import { useId, useState } from 'react';
import { useDispatch } from 'react-redux';
import { uploadsActions } from '../../_store';

export { UploadComponent };

function UploadComponent(props) {
  const [img, setImg] = useState(null);
  const id = useId();
  const dispatch = useDispatch();

  const onSelectFile = (data) => {
    const extension = data.currentTarget.value.match(/\.[0-9a-z]+$/i);
    function getBase64(file) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function () {
        const storeImg = {
          file: reader.result.replace(/data:.+?,/, ''),
          filename: 'img-' + Date.now() + extension[0]
        };
        setImg(reader.result);
        dispatch(uploadsActions.saveImage(storeImg));
      };
      reader.onerror = function (error) {
        console.log('Error: ', error);
      };
    }
    getBase64(data.target.files[0]);
  };

  const handleDeleteImage = () => {
    setImg(null);
  };

  return (
    <>
      {img && (
        <li className="position-relative">
          <div className="upload-image">
            <Icon icon="it-close" onClick={handleDeleteImage} />
            <img src={`${img}`} alt="tmp" />
          </div>
        </li>
      )}
      {!img && (
        <li>
          <div className="upload-image">
            <input
              type="file"
              name={id}
              id={id}
              className="upload pictures-wall"
              multiple="multiple"
              onChange={onSelectFile}
              accept="image/png, image/jpeg, image/jpeg"
            />
            <label htmlFor={id} className="position-relative py-3">
              <Icon color="primary" icon="it-plus" size={'xs'} className="position-relative" />
              <span>{t('add_photos')}</span>
            </label>
          </div>
        </li>
      )}
    </>
  );
}
