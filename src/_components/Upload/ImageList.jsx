import { Icon } from 'design-react-kit';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { isImage } from '../../_helpers/utilities';
import { removeDocs, removeImage } from '../../_store';
import './pointer.css';

export { ImageList };

function ImageList({ file }) {
  const dispatch = useDispatch();
  const { token } = useSelector((x) => x.auth);
  const [img, setImg] = useState(null);

  const handleDeleteImage = (storeImg) => {
    dispatch(removeImage(storeImg));
  };

  const handleDeleteDocs = (storeImg) => {
    dispatch(removeDocs(storeImg));
  };

  useEffect(() => {
    const fetchImage = async () => {
      const res = await fetch(file.url, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
      const imageBlob = await res.blob();
      const imageObjectURL = URL.createObjectURL(imageBlob);
      setImg(imageObjectURL);
    };

    if (!file?.preview && isImage(file?.name) && !img) {
      fetchImage();
    }
  }, [file, token, img]);

  return (
    <>
      {file && (
        <>
          <div
            className="upload-wrapper d-flex justify-content-between align-items-center"
            key={'img-'}
          >
            {file?.preview || isImage(file?.name) ? (
              <img src={file.preview || img} alt={'img-resized'} className={'img w-50-px'} style={{width: "50px"}} />
            ) : (
              <Icon icon={'it-file'} color={'primary'}></Icon>
            )}
            <span className="t-primary fw-bold text-break w-100 ms-2">{file.originalName || file.name}</span>
            {/* <span className="upload-file-weight">{formatBytes(file.size)}</span> */}
            <span className="align-self-center cursor-pointer">
              {file?.preview || isImage(file?.name) ? (
                <Icon
                  onClick={() => handleDeleteImage(file)}
                  icon={'it-close'}
                  size={'sm'}
                  color={'primary'}
                  className={'mb-1'}
                ></Icon>
              ) : (
                <Icon
                  onClick={() => handleDeleteDocs(file)}
                  icon={'it-close'}
                  size={'sm'}
                  color={'primary'}
                  className={'mb-1'}
                ></Icon>
              )}
            </span>
          </div>
          <hr />
        </>
      )}
    </>
  );
}
