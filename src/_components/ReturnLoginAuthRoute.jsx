import useAuth from "../_hooks/useAuth";
import {Navigate} from "react-router-dom";

export function ReturnLoginAuthRoute({ children }) {

  const {isLogged} = useAuth();

  if (isLogged) return <Navigate to={`${window.OC_READ_ONLY_MODE === true || /segnalazioni\/|inefficiencies-list/.test(window.location.href) ? "/segnalazioni" : "/segnala_disservizio"}`} />;
  return children;
}
