import dayjs from 'dayjs';
import { Icon, Button } from 'design-react-kit';
import { t } from 'i18next';
import { getI18n } from "react-i18next";
import { useSelector } from 'react-redux';

import * as DOMPurify from 'dompurify';
import { isImage } from '../../_helpers/utilities';
import { ImageThumb } from '../Images/ImageThumb';

export { Comment };

function Comment({ comment, print = false }) {
  const { currentUser } = useSelector((x) => x.currentUser);
  const { token } = useSelector((x) => x.auth);

  function renderCommentDate(comment) {
    return (
      <time dateTime={dayjs(comment.created_at).locale(getI18n().language).format()}>
        {dayjs(comment.created_at).locale(getI18n().language).format('L')}
        {" "}{t('created_at_time')}{" "}
        {dayjs(comment.created_at).locale(getI18n().language).format('LT')}
      </time>
    )
  }

  function downloadFile(url, fileName) {
    fetch(url, {
      method: 'get',
      referrerPolicy: 'no-referrer',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then((res) => res.blob())
      .then((res) => {
        const aElement = document.createElement('a');
        aElement.setAttribute('download', fileName);
        const href = URL.createObjectURL(res);
        aElement.href = href;
        aElement.setAttribute('target', '_blank');
        aElement.click();
        URL.revokeObjectURL(href);
      });
  }

  return (
    <article
      style={{breakInside: 'avoid'}}
      className={`mb-4 message ${
        print 
          ? "border" 
          : comment.author && comment.author === currentUser.id 
            ? "container-right" 
            : "container-left bg-operator"
      }`}
    >
      <div className="rounded p-3 shadow w-100">
        <h3
          className={`h6 message-header ${
            comment.author && comment.author === currentUser.id ? '' : 'text-start'}
          `
        }
        >
          <span className="avatar d-print-none">
            <Icon
              icon={'it-user'}
              aria-hidden
            ></Icon>
          </span>
          <span className='visually-hidden'>
            {t('comment_author')}
            {" "}
          </span>
          <span className="mx-2 text-secondary">{`${
            comment.author && comment.author === currentUser.id
              ? currentUser?.full_name
              : 'operatore'
          }`}</span>
          
          <span className="visually-hidden">
            {" "}
            {t('date_at')}
            {" "}
            {renderCommentDate(comment)}
          </span>
        </h3>
        <div
          className={`message-body text-break fw-semibold my-2 ${
            comment.author && comment.author === currentUser.id ? '' : 'text-start'
          }`}
        >
          <span
            className="text-paragraph"
            dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(comment.message + ' ') }}
          ></span>
          {comment?.attachments?.map((att, idx) => (
            <div key={`att-${idx.toString()}`}>
              {isImage(att.original_name) ? <ImageThumb imageUrl={att.url}></ImageThumb> : null}
              <Button
                className="list-item icon-left d-inline-block p-0"
                onClick={() => downloadFile(att.url, att.description.replace(/.bin/i, ''))}
                aria-label={t('download_attachments')}
                title={' ' + t('download_attachments')}
                role="button"
              >
                <span className="list-item-title-icon-wrapper">
                  <span className="list-item"> {att.description.replace(/.bin/i, '')}</span>
                </span>
              </Button>
            </div>
          ))}
        </div>
        <div className="row" aria-hidden>
          <p className="mb-0 x-small text-end">
            <span className="date">
              <span className="visually-hidden">
                {t('created_at')}
              </span>
              {renderCommentDate(comment)}
            </span>
          </p>
        </div>
      </div>
    </article>
  );
}
