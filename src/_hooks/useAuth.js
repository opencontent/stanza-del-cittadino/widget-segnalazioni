import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { authActions, currentUserActions } from '../_store';

export default function useAuth() {
  const [tokenState, setToken] = useState(null);
  const [userData, setUserData] = useState(null);
  const [isLogged, setIsLogged] = useState(false);
  const dispatch = useDispatch();
  const { token, user_data } = useSelector((x) => x.auth);

  useEffect(() => {
    getAuthData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token]);

  const getAuthData = async () => {
    dispatch(authActions.getSessionToken()).then((el) => {
      if (el.meta.requestStatus === 'rejected' && !token) {
        if (!user_data && !token) {
          dispatch(authActions.createSessionToken())
            .unwrap()
            .then((response) => {
              setToken(response.token);
              dispatch(currentUserActions.getCurrentUser())
                .unwrap()
                .then((response) => {
                if(response[0].nome === 'Anonymous'){
                  setIsLogged(false)
                }else{
                  setIsLogged(true)
                }
                  setUserData(response[0]);
                });
            });
        }
      } else {
        setToken(token);
          dispatch(currentUserActions.getCurrentUser())
              .unwrap()
              .then((response) => {
                if(response[0].nome === 'Anonymous'){
                  setIsLogged(false)
                }else{
                  setIsLogged(true)
                }
                setUserData(response[0]);
              });
      }
    })
  };

  return { tokenState, userData, isLogged };
}
