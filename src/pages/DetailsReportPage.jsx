import { Splide } from '@splidejs/react-splide';
import dayjs from 'dayjs';
import {
  Button,
  Col,
  Container,
  Icon,
  NavLink,
  NotificationManager,
  notify,
  Row,
  Spinner,
  TextArea,
  Label,
  Accordion,
  AccordionItem,
  AccordionHeader,
  NavItem,
  LinkList,
  AccordionBody,
  useNavScroll,
  UncontrolledTooltip, DropdownMenu, DropdownToggle, Dropdown
} from "design-react-kit";
import React, { useEffect, useRef, useState } from 'react';
import { getI18n, useTranslation } from "react-i18next";
import { MapContainer } from 'react-leaflet';
import { useSelector, useDispatch } from 'react-redux';
import { HashLink } from 'react-router-hash-link';
import useScrollPercentage from 'react-scroll-percentage-hook';
import Breadcrumbs from '../_components/Breadcrumbs/Breadcrumbs';
import { HistoryPoint } from '../_components/History/HistoryPoint';
import { ImageSlide } from '../_components/Images/ImageSlide';
import { countItemsNested } from '../_helpers/utilities';

import { currentUserActions, reportsActions } from '../_store';
import { useParams } from 'react-router-dom';
import { ButtonsStatus, Status } from "../_components/Button/ButtonStatus";
import { Comment } from '../_components/Comment/Comment';
import { MapCustom } from '../_components/Map/MapCustom';

// Default theme
import '@splidejs/react-splide/css';
import { Login } from '../login/Login';

export { DetailsReportPage };
function DetailsReportPage() {
  const dispatch = useDispatch();
  const { currentUser } = useSelector((x) => x.currentUser);
  const [collapseElementOpen, setCollapseElement] = useState('1');
  const [collapseElementIterOpen, setCollapseElementIter] = useState('1');
  const [copyState, setCopyState] = useState(false);
  const { percentage } = useScrollPercentage({ windowScroll: true });
  const { token, show_login } = useSelector((x) => x.auth);
  const { report, messages, history } = useSelector((x) => x.reports);
  const { t } = useTranslation();
  let { id } = useParams();
  const [open, toggle] = useState(false);


  //References object
  const containerRef = useRef(null);
  const refMessage = useRef(null);
  const inputFileRef = useRef(null);
  const inputFileDocRef = useRef(null);
  const buttonCopyRef = useRef(null);
  const idRef = useRef(null);

  const { register, isActive } = useNavScroll({
    offset: 0
  });

  const getActiveClass = (id) => (isActive(id) ? 'active nav-link' : 'nav-link');

  // Modal
  const [message, setMessage] = useState('');
  const [countFiles, setCountFiles] = useState({ images: 0, files: 0 });

  function saveComment() {
    const data = {
      application: report.id,
      payload: {
        message: message,
        visibility: 'applicant'
      }
    };
    if (message) {
      dispatch(reportsActions.createComment(data))
        .unwrap()
        .then(() => {
          setMessage('');
          dispatch(reportsActions.getComments({ id }));
        })
        .catch(() => {
          notify(t('warning'), <p>{t('error_messages')}</p>, { state: 'error' });
        });
    }
  }

  const handleClickButtonFile = (inputFileRef) => {
    inputFileRef.current.click();
  };

  function uploadAttachments(fileObj, base64File) {
    const data = {
      application: report.id,
      payload: {
        message: base64File && message === '' ? `${fileObj.type && fileObj.type.includes("image") ? t('added_image') : t('added_file')} ${fileObj.name}` : message,
        visibility: 'applicant',
        attachments: [
          {
            name: fileObj.name,
            mime_type: fileObj.type,
            file: base64File,
            original_name: fileObj.originalName || fileObj.name
          }
        ]
      }
    };
    if (data.payload.message) {
      dispatch(reportsActions.createComment(data))
        .unwrap()
        .then(() => {
          setMessage('');
          dispatch(reportsActions.getComments({ id }));
        })
        .catch(() => {
          notify(t('file_limit_upload_error'), {
            dismissable: true,
            state: 'error',
            duration: 3000
          });
        });
    }
  }

  const handleFileChange = (event) => {
    const fileObj = Array.from(event.target.files)[0];

    if (!fileObj) {
      return;
    }

    if (fileObj && fileObj.size > 6000000) {
      notify(t('file_limit_size'), {
        dismissable: true,
        state: 'error',
        duration: 3000
      });
      return;
    }

    let reader = new FileReader();
    reader.readAsDataURL(fileObj);
    reader.onload = function () {
      uploadAttachments(fileObj, reader.result.replace(/data:.+?,/, ''));
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
  };

  // recovery token otherwise I create it
  useEffect(() => {
    if (token) {
      if (id) {
        dispatch(currentUserActions.getCurrentUser())
          .unwrap()
          .then(() => {
            dispatch(reportsActions.getReportDetails({ id }));
            dispatch(reportsActions.getComments({ id }));
          });
      }
    }
  }, [dispatch, token, id]);

  useEffect(() => {
    if (messages.length) {
      setCountFiles(countItemsNested(messages));
    }
  }, [messages]);

  useEffect(() => {
    if (report.id) {
      dispatch(reportsActions.getHistory({ idApplication: report.id }));
    }
  }, [dispatch, report.id]);

  const handleNotify = (props) => {
    notify(t('warning'), <p>{props}</p>, { state: 'error' });
  };

  const handleCopy = (text) => {
    navigator.clipboard.writeText(text).then(
      () => {
        setCopyState(true);
        setTimeout(() => {
          setCopyState(false);
        }, 1000);
        /* Resolved - text copied to clipboard successfully */
      },
      () => {
        console.error('Failed to copy');
        /* Rejected - text failed to copy to the clipboard */
      }
    );
  };

  function downloadFile(url, fileName) {
    fetch(url, {
      method: 'get',
      referrerPolicy: 'no-referrer',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then((res) => res.blob())
      .then((res) => {
        const aElement = document.createElement('a');
        aElement.setAttribute('download', fileName);
        const href = URL.createObjectURL(res);
        aElement.href = href;
        aElement.setAttribute('target', '_blank');
        aElement.click();
        URL.revokeObjectURL(href);
      });
  }

  function calculateExpiredMessages(date) {
    const now = dayjs();
    const lastChangedStatus = dayjs(date);
    // Return true id params date is greater than 48 hours / 2 days
    return now.diff(lastChangedStatus, 'days', false) >= 2;
  }



  return (
    <>
      {report.id ? (
        <>
          <Container id="intro" tag="header" className='d-print-none'>
            <Row className="justify-content-center mb-40 mb-lg-80">
              <Col className="col-12 pb-2" lg={8}>
                {window.OC_RENDER_BREADCRUMB === true ||
                  process.env.REACT_APP_OC_RENDER_BREADCRUMB === 'true' ? (
                  <Breadcrumbs></Breadcrumbs>
                ) : null}
                <h1>{report?.data?.subject}</h1>
                <ButtonsStatus status={report.status}></ButtonsStatus>
                {report?.data?.sequential_id ? null : (
                  <div className={'my-3'}>
                    <span>ID {t('reporting')}:</span>
                    <strong ref={idRef}>
                      {' '}
                      {report?.data?.sequential_id
                        ? report?.data?.sequential_id
                        : report.id.split('-')[0]}
                    </strong>
                    <Button
                      className={'px-2'}
                      innerRef={buttonCopyRef}
                      icon={true}
                      size={'xs'}
                      outline
                      onClick={() =>
                        handleCopy(
                          report?.data?.sequential_id ? report?.data?.sequential_id : report.id
                        )
                      }
                    >
                      <Icon 
                        color={'primary'}
                        size={'sm'}
                        icon={'it-copy'}
                        aria-label={t('copy')}
                        focusable="false"
                        role="presentation"
                      />
                    </Button>
                  </div>
                )}

                <UncontrolledTooltip placement="top" target={idRef}>
                  {report?.data?.sequential_id ? report?.data?.sequential_id : report.id}
                </UncontrolledTooltip>
                <UncontrolledTooltip placement="top" target={buttonCopyRef}>
                  {copyState ? t('copied') : t('copy')}
                </UncontrolledTooltip>
                <p className={'mt-3 mb-0'}>
                  {t('published_at') + ' ' + dayjs(report?.created_at).locale(getI18n().language).format('L LT')}
                </p>
              </Col>
              <Col className="col-12 p-2 d-flex" lg={2}>
                <Dropdown className='d-flex align-items-center' isOpen={open} toggle={() => toggle(!open)}>
                  <DropdownToggle caret size={'sm'} tag={'button'} className='text-primary'>
                    <Icon
                      color='primary'
                      icon='it-more-items'
                      aria-hidden
                      focusable="false"
                      role="presentation" 
                    /> 
                    <span>{t('show_actions')}</span>
                  </DropdownToggle>
                  <DropdownMenu >
                    <Button
                      size={'xs'}
                      role="menuitem"
                      onClick={() => window.print()} disabled={!window.document.readyState === 'complete'}>
                      <span>{t('print_pdf')}</span>
                    </Button>
                  </DropdownMenu>
                </Dropdown>
              </Col>
              <Col xs={'12'}>
                <hr className="d-none d-lg-block mt-30 mb-2" />
              </Col>
            </Row>
          </Container>
          <Container tag="section" className='d-print-none'>
            <Row>
              <Col xs={12} lg={3}>
                <div className="cmp-navscroll sticky-md-top">
                  <nav
                    className="navbar it-navscroll-wrapper navbar-expand-lg"
                    aria-label={t('info')}
                  >
                    <div className="navbar-custom" id="navbarNavProgress">
                      <div className="menu-wrapper">
                        <div className="link-list-wrapper">
                          <h2 className='visually-hidden' id='page-index'>{t('index_page')}</h2>
                          <Accordion>
                            <AccordionItem>
                              <AccordionHeader
                                id={'accordion-title-one'}
                                className="pb-10 px-3"
                                active={collapseElementOpen === '1'}
                                onToggle={() =>
                                  setCollapseElement(collapseElementOpen !== '1' ? '1' : '')
                                }
                              >
                                {' '}
                                {t('index_page')}{' '}
                                <Icon
                                  icon={'it-expand'}
                                  size={'xs'}
                                  className={'right'}
                                  aria-hidden
                                  focusable="false"
                                  role="presentation"
                                ></Icon>
                              </AccordionHeader>{' '}
                              <div className="progress progress-2">
                                <div
                                  className="progress-bar it-navscroll-progressbar"
                                  role="progressbar"
                                  aria-valuenow={percentage.vertical}
                                  aria-valuemin="0"
                                  aria-valuemax="100"
                                  aria-labelledby='page-index'
                                  style={{
                                    width: percentage.vertical + '%'
                                  }}
                                ></div>
                              </div>
                              <AccordionBody active={collapseElementOpen === '1'}>
                                <LinkList data-element="page-index">
                                  {report.data?.type && report.data.type.label ? (
                                    <NavItem>
                                      <HashLink
                                        smooth
                                        className={getActiveClass('type_report')}
                                        to={'#type_report'}
                                      >
                                        <span className="title-medium">{t('type_report')}</span>
                                      </HashLink>
                                    </NavItem>
                                  ) : null}
                                  {report.data?.details ? (
                                    <NavItem>
                                      <HashLink
                                        smooth
                                        className={getActiveClass('description')}
                                        to={'#description'}
                                      >
                                        <span className="title-medium">{t('description')}</span>
                                      </HashLink>
                                    </NavItem>
                                  ) : null}
                                  {report.data?.address ? (
                                    <NavItem>
                                      <HashLink
                                        smooth
                                        className={getActiveClass('address')}
                                        to="#address"
                                      >
                                        <span className="title-medium">{t('address')}</span>
                                      </HashLink>
                                    </NavItem>
                                  ) : null}
                                  {report.data?.images?.length ? (
                                    <NavItem>
                                      <HashLink
                                        smooth
                                        className={getActiveClass('images')}
                                        to="#images"
                                      >
                                        <span className="title-medium">{t('images')}</span>
                                      </HashLink>
                                    </NavItem>
                                  ) : null}
                                  {report?.data?.docs?.length ? (
                                    <li className="nav-item">
                                      <HashLink
                                        smooth
                                        className={getActiveClass('attachments')}
                                        to="#attachments"
                                        onClick={() => isActive('attachments')}
                                      >
                                        <span className="title-medium">{t('attachments')}</span>
                                      </HashLink>
                                    </li>
                                  ) : null}
                                  {messages ? (
                                    <li className="nav-item">
                                      <HashLink
                                        smooth
                                        className={getActiveClass('comments')}
                                        to={'#comments'}
                                        onClick={() => isActive('comments')}
                                      >
                                        <span className="title-medium">{t('comments')}</span>
                                      </HashLink>
                                    </li>
                                  ) : null}
                                </LinkList>
                              </AccordionBody>
                            </AccordionItem>
                          </Accordion>
                        </div>
                      </div>
                    </div>
                    <div className="menu-wrapper">
                      <div className="link-list-wrapper">
                        <Accordion>
                          <AccordionItem>
                            <AccordionHeader
                              id={'accordion-title-two'}
                              className="pb-10 px-3"
                              active={collapseElementIterOpen === '1'}
                              onToggle={() =>
                                setCollapseElementIter(collapseElementIterOpen !== '1' ? '1' : '')
                              }
                            >
                              {t('index_report')}
                              <Icon
                                icon={'it-expand'}
                                size={'xs'}
                                className={'right'}
                                aria-hidden
                                focusable="false"
                                role="presentation"
                              ></Icon>
                            </AccordionHeader>{' '}
                            <div className="progress progress-2">
                              <div
                                className="progress-bar it-navscroll-progressbar"
                                role="progressbar"
                                aria-valuenow={100}
                                aria-valuemin="100"
                                aria-valuemax="100"
                                aria-hidden
                                style={{
                                  width: '100%'
                                }}
                              ></div>
                            </div>
                            <AccordionBody active={collapseElementIterOpen === '1'}>
                              <div className='link-list'>
                                {report?.id && history?.length ? (
                                  <div className="point-list-wrapper my-4">
                                    {history.map((element, idx) => (
                                      <HistoryPoint key={'point-list' + idx} point={element} />
                                    ))}
                                  </div>
                                ) : null}
                              </div>
                            </AccordionBody>
                          </AccordionItem>
                        </Accordion>
                      </div>
                    </div>
                  </nav>
                </div>
              </Col>
              <Col xs={12} lg={8} className={'offset-lg-1'} ref={containerRef}>
                {report.data?.type && report.data.type.label ? (
                  <div className={'mb-30'}>
                    <h2 className="title-xxlarge mb-3" {...register('type_report')}>
                      {t('type_report')}
                    </h2>
                    <p className={'mt-3  richtext-wrapper '}>{report?.data?.type.label}</p>
                  </div>
                ) : null}

                {report.data?.details ? (
                  <div className={'mb-30'}>
                    <h2 className="title-xxlarge mb-3" {...register('description')}>
                      {t('description')}
                    </h2>
                    <p className={'mt-3  richtext-wrapper  pre'}> {report.data?.details ? report.data.details : '--'}</p>
                  </div>
                ) : null}

                {report.data?.address ? (
                  <div className={'mb-30'}>
                    <h2 className="title-xxlarge mb-3" {...register('address')}>
                      {t('address')}
                    </h2>
                    <p className={''}>{report?.data?.address?.display_name}</p>
                    {report.data?.address?.lat && report?.data?.address?.lon ? (
                      <MapContainer
                        center={[report.data.address.lat, report.data.address.lon]}
                        zoom={20}
                        scrollWheelZoom={false}
                        style={{ height: '350px' }}
                      >
                        <MapCustom position={[report.data.address.lat, report.data.address.lon]} />
                      </MapContainer>
                    ) : null}
                  </div>
                ) : null}
                {report?.data?.images?.length ? (
                  <div className={'mb-30'}>
                    <div className="pagebreak"> </div>
                    <h2 className="title-xxlarge mb-3" {...register('images')}>
                      {t('report_photo')}
                    </h2>
                    <Col className="text-center">
                      <Splide
                        aria-label={t('carousel_title')}
                        
                        options={{
                          destroy: report?.data?.images.length <= 1,
                          rewind: true,
                          gap: '1rem',
                          i18n: {
                            prev: t('carousel_prev'),
                            next: t('carousel_next'),
                            slideLabel: t('carousel_of')
                          }
                        }}
                      >
                        {report?.data?.images.map((img, idx) => (
                          <ImageSlide
                            props={img}
                            key={`image-${idx.toString()}`}></ImageSlide>
                        ))}
                      </Splide>
                    </Col>
                  </div>
                ) : null}
                {report?.data?.docs?.length ? (
                  <div className={'mb-30'}>
                    {report?.data?.docs && (
                      <h2 className="title-xxlarge mb-3" {...register('attachments')}>
                        {t('attachments')}
                      </h2>
                    )}
                    <Col xs={'12'}>
                      {report?.data?.docs.map((att, idx) => (
                        <div
                          className="cmp-icon-link"
                          key={`download_attachments_${idx.toString()}`}
                        >
                          <NavLink
                            className="list-item icon-left d-inline-block p-0"
                            onClick={() => downloadFile(att.url, att.name)}
                            aria-label={t('download_attachments')}
                            title={t('download_attachments')}
                            role="button"
                          >
                            <span className="list-item-title-icon-wrapper">
                              <Icon
                                color={'primary'}
                                icon="it-clip"
                                size={'sm'}
                                aria-hidden
                                focusable="false"
                                role="presentation"
                              ></Icon>
                              <span className="list-item">{att.originalName || att.name}</span>
                            </span>
                          </NavLink>
                        </div>
                      ))}
                    </Col>
                  </div>
                ) : null}
                <div className="pagebreak"> </div>
                <div className="it-page-section mb-50 mb-lg-90">
                  <div className="cmp-card">
                    <div className="card">
                      <div className="card-header border-0 p-0 mb-lg-30 m-0">
                        <div className="d-flex">
                          {messages.length > 0 && (
                            <h2 className="title-xxlarge mb-3" {...register('comments')}>
                              {t('comments')}
                            </h2>
                          )}
                        </div>
                      </div>
                      {messages ? (
                        <div className="card-body p-0">
                          {messages.length > 0 &&
                            messages.map((comment, idx) => (
                              <Comment
                                handleNotify={handleNotify}
                                comment={comment}
                                key={comment.id + idx.toString()}
                              />
                            ))}
                        </div>
                      ) : (
                        <div className="card-body p-0">
                          <p className="text-paragraph m-0 mb-1">{t('no_comments')}</p>
                        </div>
                      )}
                      {currentUser?.nome !== 'Anonymous' && report.status >= 1900 ? (
                        <div className={'w-100 mt-3 mb-0'}>
                          {currentUser?.nome !== 'Anonymous' &&
                            report.status === '7000' &&
                            calculateExpiredMessages(report.latest_status_change_at) ? (
                            ''
                          ) : (
                            <div>
                              <Label htmlFor={'message-text'}>{t('message')}</Label>
                              <TextArea
                                name="text"
                                id="message-text"
                                rows={3}
                                onChange={(e) => setMessage(e.target.value)}
                                value={message}
                                innerRef={refMessage}
                                autoComplete="off"
                              />
                              <small
                                id="message-textDescription"
                                className='visually-hidden'>{t('required_field')}. {t('enter_max_char_info')}</small>
                              <div className="d-grid gap-2 d-md-block">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={() => saveComment()}
                                  icon={true}
                                  data-focus-mouse="false"
                                  disabled={message === ''}
                                  className={'mt-3 me-0 me-sm-3'}
                                >
                                  <span>{t('add_comment')}</span>
                                  <Icon
                                    color="light"
                                    icon="it-pencil"
                                    size={'sm'}
                                    aria-hidden
                                    focusable="false"
                                    role="presentation"
                                  />
                                </Button>
                                <Label
                                  htmlFor={'imageFile'}
                                  className='visually-hidden'
                                >{t('image')}
                                </Label>
                                <input
                                  id={'imageFile'}
                                  ref={inputFileRef}
                                  type="file"
                                  onChange={handleFileChange}
                                  accept="image/png, image/jpeg, image/jpeg"
                                  className="visually-hidden upload"
                                  multiple="multiple"
                                  tabIndex='-1'
                                />
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={() => handleClickButtonFile(inputFileRef)}
                                  icon={true}
                                  className={'mt-3 me-0 me-sm-3'}
                                  disabled={countFiles.images >= 3}
                                >
                                  <span>{t('add_image')}</span>
                                  <Icon
                                    color="light"
                                    icon="it-clip"
                                    size={'sm'} 
                                    aria-hidden
                                    focusable="false"
                                    role="presentation"
                                  />
                                </Button>
                                <Label
                                  htmlFor={'docsFile'}
                                  className='visually-hidden'>{t('file')}
                                </Label>
                                <input
                                  id={'docsFile'}
                                  ref={inputFileDocRef}
                                  type="file"
                                  onChange={handleFileChange}
                                  accept=".pdf"
                                  className="visually-hidden upload"
                                  multiple="multiple"
                                  tabIndex='-1'
                                />
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={() => handleClickButtonFile(inputFileDocRef)}
                                  icon={true}
                                  data-focus-mouse="false"
                                  className={'mt-3'}
                                  disabled={countFiles.files >= 3}
                                >

                                  <span>{t('add_file')}</span>
                                  <Icon
                                    color="light"
                                    icon="it-file"
                                    size={'sm'}
                                    aria-hidden
                                    focusable="false"
                                    role="presentation"
                                  />
                                </Button>
                              </div>
                            </div>
                          )}
                        </div>
                      ) : null}
                    </div>
                  </div>
                </div>
                <NotificationManager />
              </Col>
              <div className="pagebreak"> </div>
            </Row>
          </Container>
          <div className="d-none d-print-block">
            <section>
              <h1 className="h5 mb-3" style={{letterSpacing: 0}}>Oggetto della segnalazione</h1>
              <p className={'mt-3 richtext-wrapper'}> {report?.data?.subject}</p>
            </section>
            <section>
              <h2 className={'h5 my-3'}>ID {t('reporting')}</h2>
              <p className={'mt-3 richtext-wrapper '}>{report?.data?.sequential_id ? report?.data?.sequential_id : report.id.split('-')[0]}</p>
            </section>
            <section>
              <h2 className='h5'>Stato segnalazione:</h2>
              <div className={'my-3 richtext-wrapper'}><Status status={report.status}></Status></div>
            </section>
            <section>
              <h2 className="h5 mb-1">{t('published_at')}</h2>
              <p className={'mt-3 richtext-wrapper'}>{dayjs(report?.created_at).locale(getI18n().language).format('L LT')}</p>
            </section>
            <div>
              {report.data?.type && report.data.type.label ? (
                <div>
                  <h2 className="h5 mb-1"> {t('type_report')}</h2>
                  <p className={'mt-3  richtext-wrapper '}>{report?.data?.type.label}</p>
                </div>
              ) : null}

              {report.data?.details ? (
                <div className='mb-3'>
                  <h2 className="h5 mb-1">{t('description')}</h2>
                  <p className={'mt-3  richtext-wrapper '}>{report?.data?.details}</p>
                </div>
              ) : null}

              {report.data?.address ? (
                <div>
                  <h2 className="h5 my-3 mb-1">{t('address')}</h2>
                  <p className={'mt-3 richtext-wrapper'}>{report?.data?.address?.display_name}</p>
                </div>
              ) : null}
              {report?.data?.images?.length ? (
                <div className={'mb-30'}>
                  <div className='pagebreak'></div>
                  <h2 className="h5 mb-3">{t('report_photo')}</h2>
                  <Col className="text-center">
                    {report?.data?.images.map((img, idx) => (
                      <ImageSlide
                        props={img}
                        key={`image-${idx.toString()}`}></ImageSlide>
                    ))}
                  </Col>
                </div>
              ) : null}
              {report?.data?.docs?.length ? (
                <div className={'mb-30'}>
                  {report?.data?.docs && (
                    <h2 className="h5 mb-3"> {t('attachments')}</h2>
                  )
                  }
                  <div>
                    {report?.data?.docs.map((att, idx) => (
                      <div
                        className="cmp-icon-link"
                        key={`download_attachments_${idx.toString()}`}
                      >
                        
                        <NavLink
                          className="list-item icon-left d-inline-block p-0"
                          onClick={() => downloadFile(att.url, att.name)}
                          aria-label={t('download_attachments')}
                          title={t('download_attachments')}
                          role="button"
                        >
                          <span className="list-item-title-icon-wrapper">
                            <Icon
                              color={'primary'}
                              icon="it-clip"
                              size={'sm'}
                              aria-hidden
                              focusable="false"
                              role="presentation"
                            ></Icon>
                            <span className="list-item">{att.originalName || att.name}</span>
                          </span>
                        </NavLink>
                      </div>
                    ))}
                  </div>
                </div>
              ) : null}

              <div className="">
                <div className="cmp-card">
                  <div className="card">
                    <div className="card-header border-0 p-0 mb-lg-30 m-0">
                      {messages.length > 0 && (
                        <h2 className="h5 mb-3"> {t('comments')}</h2>
                      )}
                    </div>
                    {messages ? (
                      <div className="card-body p-0">
                        {messages.length > 0 &&
                          messages.map((comment, idx) => (
                            <Comment
                              handleNotify={handleNotify}
                              comment={comment}
                              print
                              key={comment.id + idx.toString()}
                            />
                          ))}
                      </div>
                    ) : (
                      <div className="card-body p-0">
                        <p className="text-paragraph m-0 mb-1">{t('no_comments')}</p>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : (
        <Container>
          {show_login ? (
            <Login></Login>
          ) : (
            <Row className="justify-content-center text-center m-3" style={{ minHeight: '408px' }}>
              <Col className="col-12">
                <Spinner double active />
              </Col>
            </Row>
          )}
        </Container>
      )}
    </>
  );
}
