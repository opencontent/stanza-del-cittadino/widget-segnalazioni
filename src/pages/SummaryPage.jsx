import {
  Alert,
  Button,
  Callout,
  CalloutText,
  CalloutTitle,
  Col,
  Icon,
  NotificationManager,
  notify,
  Row
} from 'design-react-kit';
import { useEffect, useLayoutEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';

import { useLocation, useNavigate } from 'react-router-dom';
import styled from "styled-components";
import {
  apiActions,
  applicationTmp,
  reset,
  resetApiApplication,
  storeApplication
} from '../_store';

export { SummaryPage };

function SummaryPage({ onNextPage, activeStep }) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();
  const { pathname } = useLocation();
  const { application, service, sequential_id } = useSelector((x) => x.api);
  const { addressForm, type, subject, details, severity } = useSelector((x) => x.form);
  const { currentUser } = useSelector((x) => x.currentUser);
  // this determines whether the button is disabled or not
  const [isDisabled, setIsDisabled] = useState(false);

  const { name, surname, fiscal_code, phone_number, email_address, images, docs } = useSelector(
    (x) => x.form
  );

  useEffect(() => {
    //dispatch(apiActions.getServiceReport());
    if (window.OC_SEQUENTIAL_ID_PROVIDER || process.env.REACT_APP_OC_SEQUENTIAL_ID_PROVIDER) {
      dispatch(apiActions.getSequentialId());
    }
  }, [dispatch]);

  const onSubmit = () => {
    setIsDisabled(true);
    const getId =
      application?.id || application?.data?.length
        ? application?.id || application?.data[0]?.id
        : null;
    const dataApplication = {
      ...(getId && { id: getId }),
      service: service?.identifier || 'inefficiencies',
      data: {
        applicant: {
          data: {
            email_address: email_address,
            phone_number: phone_number,
            completename: {
              data: {
                name: name,
                surname: surname
              }
            },
            fiscal_code: {
              data: {
                fiscal_code: fiscal_code
              }
            }
          }
        },
        type: type,
        details: details,
        severity: severity,
        subject: sequential_id ? sequential_id + ' - ' + subject : subject,
        ...(addressForm && { address: addressForm }),
        images: images,
        docs: docs,
        sequential_id: sequential_id
      },
      status: '1900',
      source: 'web'
    };

    dispatch(storeApplication(dataApplication));

    if (application?.id) {
      return dispatch(
        apiActions.updateApplication({ data: dataApplication, id: dataApplication.id })
      ).then((response) => {
        if (response?.payload) {
          dispatch(applicationTmp(dataApplication));
          dispatch(reset());
          dispatch(resetApiApplication());
          navigate(`/thankyou?${application.id}`);
        }
      });
    } else {
      return dispatch(apiActions.createApplication(dataApplication)).then((response) => {
        if (response?.payload) {
          dispatch(storeApplication(response?.payload));
          dispatch(applicationTmp(response?.payload));
          dispatch(reset());
          dispatch(resetApiApplication());
          navigate(`/thankyou?${response.payload.id}`);
        }
      });
    }
  };

  const onPrev = () => {
    onNextPage(activeStep - 1);
  };

  //Save application in draft
  const saveApplication = () => {
    const getId =
      application?.id || application?.data?.length
        ? application?.id || application?.data[0]?.id
        : null;
    const dataApplication = {
      ...(getId && { id: getId }),
      service: service?.identifier || 'inefficiencies',
      data: {
        applicant: {
          data: {
            email_address: email_address,
            phone_number: phone_number,
            completename: {
              data: {
                name: name,
                surname: surname
              }
            },
            fiscal_code: {
              data: {
                fiscal_code: fiscal_code
              }
            }
          }
        },
        type: type,
        details: details,
        severity: severity,
        subject: sequential_id ? sequential_id + ' - ' + subject : subject,
        ...(addressForm && { address: addressForm }),
        images: images,
        docs: docs,
        source: 'web',
        sequential_id: sequential_id
      },
      status: '1000'
    };

    if (dataApplication?.id) {
      return dispatch(
        apiActions.updateApplication({ data: dataApplication, id: dataApplication.id })
      ).then(() => {
        dispatch(apiActions.getDraftApplication());
        notify(t('request_saved_successfully'), {
          dismissable: true,
          state: 'success',
          duration: 3000
        });
      });
    } else {
      return dispatch(apiActions.createApplication(dataApplication))
        .unwrap()
        .then(() => {
          notify(t('request_saved_successfully'), {
            dismissable: true,
            state: 'success',
            duration: 3000
          });
        })
        .catch((e) => {
          notify(t('error_generic'), e.message, {
            dismissable: true,
            state: 'error',
            duration: 3000
          });
        });
    }
  };

  // Scroll to top if path changes
  useLayoutEffect(() => {
    window.scrollTo(0, 0);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pathname]);

  return (
    <Row className="justify-content-center">
      <Col lg={8} xs={12}>
        <WrapperCallout>
        <Callout color="warning" highlight style={{ paddingLeft: '16px' }}>
          <CalloutTitle className={'mb-20 d-flex align-items-center'}>
            <Icon aria-hidden icon="it-horn" size={'sm'} />
            <span>{t('warning')}</span>
          </CalloutTitle>
          <CalloutText className="titillium text-paragraph">
            {t('alert_info_post')}
            <span className="d-lg-block"> {t('alert_info_post_verify')}</span>
          </CalloutText>
        </Callout>
        </WrapperCallout>
        <h2 className="title-xxlarge mb-4 mt-40">{t('reporting')}</h2>
        <div className="cmp-card mb-4">
          <div className="card has-bkg-grey shadow-sm mb-0">
            <div className="card-header border-0 p-0 mb-lg-30 m-0">
              <div className="d-flex">
                <h3 className="subtitle-large mb-4">{t('malfunction')}</h3>
              </div>
            </div>
            <div className="card-body p-0">
              <div className="cmp-info-summary bg-white p-3 p-lg-4">
                <div className="card">
                  <div className="card-header border-bottom border-light p-0 mb-0 pb-2 d-flex justify-content-end">
                    <span className="text-decoration-none" onClick={onPrev}>
                    <Button className="p-0 t-primary fw-bold" color='link'>
                      {t('edit')}
                    </Button>
                    </span>
                  </div>

                  <div className="card-body p-0">
                    <div className="single-line-info border-light">
                      <div className="text-paragraph-small">{t('address')}</div>
                      <div className="border-light">
                        <p className="data-text">{addressForm?.label}</p>
                      </div>
                    </div>
                    <div className="single-line-info border-light">
                      <div className="text-paragraph-small">{t('type_of_disservice')}</div>
                      <div className="border-light">
                        <p className="data-text">{type?.label ? type.label : '--'}</p>
                      </div>
                    </div>
                    <div className="single-line-info border-light">
                      <div className="text-paragraph-small">{t('title')}</div>
                      <div className="border-light">
                        <p className="data-text">{subject ? subject : '--'}</p>
                      </div>
                    </div>
                    <div className="single-line-info border-light">
                      <div className="text-paragraph-small">{t('details')}</div>
                      <div className="border-light">
                          <p className="data-text pre">{details ? details : '--'}</p>
                      </div>
                    </div>
                    {Boolean(window.OC_SHOW_SEVERITY_FIELD === true ||
                        process.env.REACT_APP_OC_SHOW_SEVERITY_FIELD === 'true') && (
                      <div className="single-line-info border-light">
                        <div className="text-paragraph-small">{t('severity_label')}</div>
                        <div className="border-light">
                            <p className="data-text pre">{severity ? severity+'/5' : '--'}</p>
                        </div>
                      </div>
                    )}
                    <div className="single-line-info border-light">
                      <div className="text-paragraph-small">{t('images')}</div>
                      <div className="border-light">
                        {images &&
                          images.map((el, index) => (
                            <p className="data-text" key={'data-text' + index}>
                              {el.originalName || el.name}
                            </p>
                          ))}
                      </div>
                    </div>
                    <div className="single-line-info border-light">
                      <div className="text-paragraph-small">{t('docs')}</div>
                      <div className="border-light">
                        {docs &&
                          docs.map((el, index) => (
                            <p className="data-text" key={'data-text' + index}>
                              {el.originalName || el.name}
                            </p>
                          ))}
                      </div>
                    </div>
                  </div>
                  <div className="card-footer p-0"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <h2 className="title-xxlarge mb-4 mt-40">{t('general_data')}</h2>
        <div className="cmp-card mb-4">
          <div className="card has-bkg-grey shadow-sm mb-0">
            <div className="card-header border-0 p-0 mb-lg-30 m-0">
              <div className="d-flex">
                <h3 className="subtitle-large mb-4">{t('report_author')}</h3>
              </div>
            </div>
            <div className="card-body p-0">
              <div className="cmp-info-summary bg-white mb-4 mb-lg-30 p-3 p-lg-4">
                <div className="card">
                  <div className="card-header border-bottom border-light p-0 mb-0 d-flex justify-content-between d-flex justify-content-end">
                    <h3 className="title-large-semi-bold mb-3">
                      {name} {surname}
                    </h3>
                  </div>

                  <div className="card-body p-0">
                    <div className="single-line-info border-light">
                      <div className="text-paragraph-small">{t('fiscal_code')}</div>
                      <div className="border-light">
                        <p className="data-text">{fiscal_code}</p>
                      </div>
                    </div>
                  </div>
                  <div className="card-footer p-0 d-none"></div>
                </div>
              </div>
              <div className="cmp-info-summary bg-white p-3 p-lg-4">
                <div className="card">
                  <div className="card-header border-bottom border-light p-0 mb-0 d-flex justify-content-between d-flex justify-content-end">
                    <h3 className="title-large-semi-bold mb-3">{t('contacts')}</h3>
                  </div>

                  <div className="card-body p-0">
                    <div className="single-line-info border-light">
                      <div className="text-paragraph-small">{t('phone')}</div>
                      <div className="border-light">
                        <p className="data-text">{phone_number}</p>
                      </div>
                    </div>
                    <div className="single-line-info border-light">
                      <div className="text-paragraph-small">Email</div>
                      <div className="border-light">
                        <p className="data-text">{email_address}</p>
                      </div>
                    </div>
                  </div>
                  <div className="card-footer p-0 d-none"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="cmp-nav-steps">
          <nav className="steppers-nav" aria-label="Step">
            <Button type="button" size={'sm'} className="steppers-btn-prev p-0" onClick={onPrev}>
              <Icon icon={'it-chevron-left'} size={'sm'} color={'primary'}></Icon>
              <span className="text-button-sm">{t('back')}</span>
            </Button>
            {currentUser?.nome !== 'Anonymous' && (
              <Button
                type="button"
                outline
                color="primary"
                size={'sm'}
                className="bg-white steppers-btn-save saveBtn"
                data-focus-mouse="false"
                onClick={() => saveApplication()}
              >
                <span className="text-button-sm t-primary">{t('save_request')}</span>
              </Button>
            )}
            <Button
              color="primary"
              type="button"
              size={'sm'}
              className="steppers-btn-confirm"
              onClick={onSubmit}
              disabled={isDisabled}
            >
              <span className="text-button-sm">{t('send')}</span>
              <Icon
                icon={'it-chevron-right'}
                size={'sm'}
                color={'primary'}
                className={'icon-white'}
              ></Icon>
            </Button>
          </nav>
          {application?.error ? (
            <Alert color={'danger'} className="cmp-disclaimer rounded">
              <span className="d-inline-block text-uppercase cmp-disclaimer__message">
                {t('error_generic')}
              </span>
            </Alert>
          ) : (
            ''
          )}
        </div>
      </Col>
      <NotificationManager></NotificationManager>
    </Row>
  );
}

export const WrapperCallout = styled.div`
.alert-warning, .message-warning, .warning {
  background-image: none;
}
  `;
