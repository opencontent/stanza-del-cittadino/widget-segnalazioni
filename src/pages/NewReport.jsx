import {
  Col,
  Container,
  Row,
  StepperContainer,
  StepperHeader,
  StepperHeaderElement
} from 'design-react-kit';

import React, { useEffect, useState } from "react";
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { useLocation, useNavigate } from "react-router-dom";
import styled from "styled-components";
import Breadcrumbs from '../_components/Breadcrumbs/Breadcrumbs';

import { PrivacyPage } from './PrivacyPage';
import { ReportPage } from './ReportPage';
import { SummaryPage } from './SummaryPage';
import { getLoginProviders } from '_helpers/utilities';

export { NewReport };

function NewReport() {
  const [activeStep, toggleStep] = useState(1);
  const { t } = useTranslation();
  const { currentUser } = useSelector((x) => x.currentUser);
  const urlParams = new URL(decodeURIComponent(window.location.href));
  const params = urlParams.toString().split('return-url=');
  const returnUrl = params[1];

  const location = useLocation()
  const navigate = useNavigate()
  const { navigateStep } = location.state || {};
  const callbackOnNextPage = (step) => {
    if (!navigateStep) {
      navigate(
        { ...location },
        { state: { ...location.state, navigateStep: activeStep }, replace: true }
      );
    }
    navigate({ ...location }, { state: { ...location.state, navigateStep: step } });
    toggleStep(step);
  };

  useEffect(()=> {
    if(navigateStep && navigateStep < activeStep)
    toggleStep(navigateStep)
  }, [navigateStep, activeStep])

  const handleLoginClick = () => {
    window.location.href = `${window.OC_AUTH_URL}?return-url=${returnUrl || window.location.href}#/`;
    return null;
  };

  const { i18n } = useTranslation();
  const language = i18n.language;
  const loginProvider = getLoginProviders(language);

  return (
    <div>
      <Container>
      <Row className="justify-content-center">
        <Col className="col-12" lg={10}>
          <div className="cmp-heading pb-3 pb-lg-4">
            {window.OC_RENDER_BREADCRUMB === true ||
            process.env.REACT_APP_OC_RENDER_BREADCRUMB === 'true' ? (
              <Breadcrumbs></Breadcrumbs>
            ) : null}
            <h1 className="title-xxxlarge">{t('malfunction_reporting')}</h1>
            <p>
              {t('hero_text_required')}
              {(Object.keys(currentUser).length === 0 || currentUser?.nome === 'Anonymous') && activeStep === 1  ? (
                <>
                  <br />
                  <br />
                  {t('hero_text_login',{loginProviders:loginProvider})}{' '}
                  <button
                    role={'link'}
                    className="title-small-semi-bold t-primary text-decoration-none"
                    onClick={handleLoginClick}
                  >
                    {t('login')}
                  </button>
                </>
              ) : null}
            </p>
          </div>
        </Col>
        <Col className="col-12 cmp-info-progress">
          <WrapperStepsContainer>
          <StepperContainer>
            <StepperHeader>
              <StepperHeaderElement
                variant={activeStep === 1 ? 'active' : 'confirmed'}
                appendIcon={activeStep === 2 ? 'it-check' : ''}
              >
                {t('auth_conditions')}
              </StepperHeaderElement>
              <StepperHeaderElement
                variant={activeStep === 2 ? 'active' : 'confirmed'}
                appendIcon={activeStep === 3 ? 'it-check' : ''}
              >
                {t('data_request')}
              </StepperHeaderElement>
              <StepperHeaderElement variant={activeStep === 3 ? 'active' : 'confirmed'}>
                {t('summary')}
              </StepperHeaderElement>
              <StepperHeaderElement tag="span" aria-hidden='true' variant="mobile">{activeStep}/3</StepperHeaderElement>
            </StepperHeader>
          </StepperContainer>
          </WrapperStepsContainer>
        </Col>
      </Row>
      {activeStep === 1 ? (
        <PrivacyPage onNextPage={callbackOnNextPage} activeStep={activeStep}></PrivacyPage>
      ) : null}
      {activeStep === 2 ? (
        <ReportPage onNextPage={callbackOnNextPage} activeStep={activeStep}></ReportPage>
      ) : null}
      {activeStep === 3 ? (
        <SummaryPage onNextPage={callbackOnNextPage} activeStep={activeStep}></SummaryPage>
      ) : null}
    </Container>
    </div>)
}


export const WrapperStepsContainer = styled.div`
      @media (min-width: 992px) {
          .steppers-header ul {
              padding: 0;
              box-shadow: none;
              height: auto;
              display: flex;
              justify-content: space-between;
              background: rgba(0, 0, 0, 0);
              width: 100%;
          }

          .steppers .steppers-index {
              display: none;
          }
      }
    
    @media (max-width: 992px) {
        .steppers-header ul{
            display: contents;
        }
    }
    .steppers .steppers-index {
        margin-left: auto;
        font-size: 0.875rem;
        font-weight: 600;
        flex-shrink: 0;
    }
`
