import {
  Accordion,
  AccordionHeader,
  AccordionItem,
  AccordionBody,
  Card,
  CardHeader,
  CardBody,
  Button,
  NavItem,
  Icon,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  notify,
  Row,
  TextArea,
  LinkList,
  useNavScroll,
  NavLink,
  FormGroup,
  Col,
  NotificationManager,
  Progress,
  Spinner,
  Label
} from 'design-react-kit';
import {
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState
} from "react";
import { Controller, useForm } from 'react-hook-form';
import { MapContainer, TileLayer } from 'react-leaflet';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { HashLink } from 'react-router-hash-link';
import Select from 'react-select';
import AsyncSelect from 'react-select/async';
import { components } from 'react-select';
import debounce from 'lodash/debounce';
import uniqWith from 'lodash/uniqWith';

import styled from 'styled-components';
import { MapLayers } from '../_components/Map/MapLayers';
import { ModalMessages } from '../_components/Modal/ModalMessages';
import { ImageList } from '../_components/Upload/ImageList';
import useScrollPercentage from 'react-scroll-percentage-hook';
import { locationNameFormatter, resizeImage } from '../_helpers/utilities';
import CodiceFiscale from 'codice-fiscale-js';

import {
  apiActions,
  currentUserActions,
  reportsActions,
  storeAddress,
  storeDetails,
  storeDocs,
  storeEmail,
  storeFiscalCode,
  storeImages,
  storeLoadDocs,
  storeLoadImages,
  storeName,
  storePhoneNumber,
  storePrivacy,
  storeSeverity,
  storeSubject,
  storeSurname,
  storeType,
  uploadsActions
} from '../_store';
import ExternalRef from '_components/ExternalRef/ExternalRef';

export { ReportPage };

function ReportPage({ onNextPage, activeStep }) {
  const [collapseElementOpen, setCollapseElement] = useState('1');
  const [collapseElementOpenCard, setCollapseElementCard] = useState('1');
  const { percentage } = useScrollPercentage({ windowScroll: true });
  const { user_data } = useSelector((x) => x.auth);
  const { currentUser } = useSelector((x) => x.currentUser);
  const { categories } = useSelector((x) => x.reports);
  const { privacyChecked } = useSelector((x) => x.api);
  const { type: category } = useSelector((x) => x.form);
  const { t, i18n } = useTranslation();
  const [isFileLoading, setIsFileLoading] = useState(false);
  const [isImageLoading, setIsImageLoading] = useState(false);
  const [progressBar, setProgressBar] = useState(0);
  const [charCount, setCharCount] = useState(800);
  const [btsCssVersion, setBtsCssVersion] = useState(null);


  // Modal
  const [isOpenMap, toggleModalMap] = useState(false);
  const [isOpenModalMessages, setModalMessages] = useState(false);
  const [message, setMessage] = useState('');

  // MAP
  const [showUsePosition, setShowUsePosition] = useState(false)
  const [position, setPosition] = useState([]);
  const { address, application, service, boundingBox } = useSelector((x) => x.api);
  const { images, docs, addressForm, type, subject, details, severity, privacy } = useSelector((x) => x.form);
  const { name, surname, fiscal_code, phone_number, email_address } = useSelector((x) => x.form);

  const inputRef = useRef(null);
  const inputRefDoc = useRef(null);

  const dispatch = useDispatch();

  /**NavScroll**/
  const containerRef = useRef(null);
  const { register: registerScroll, isActive } = useNavScroll({
    root: containerRef.current || undefined
  });
  const getActiveClass = (id) => (isActive(id) ? 'active' : undefined);
  /** **/

  const required_fields = window.OC_REQUIRED_FIELDS || []

  const {
    handleSubmit,
    formState: { errors, isDirty, isValid },
    control,
    setValue,
    getValues
  } = useForm({
    mode: 'onChange',
    defaultValues: {
      subject: '',
      address: '',
      type: '',
      details: '',
      images: [],
      docs: [],
      email_address: '',
      name: '',
      surname: '',
      fiscal_code: '',
      phone_number: ''
    }
  });

  const countCharDetail = (value) => {
    let count = 800 - value.length;
    if (count < 0) {
      count = 0;
    }
    setCharCount(count);
}

  useEffect(() => {
    dispatch(apiActions.setApiLanguage(i18n.resolvedLanguage));
    dispatch(reportsActions.getCategories());
    if (user_data?.id && user_data?.name !== 'Anonymous') {
      dispatch(currentUserActions.getUserById({ id: user_data?.id }));
      dispatch(apiActions.getDraftApplication());
    }
  }, [dispatch, user_data,i18n.resolvedLanguage]);

  useEffect(() => {
    if (currentUser.nome === 'Anonymous') {
      if (name && surname) {
        setValue('name', name, {
          shouldValidate: true,
          shouldDirty: true
        });
        setValue('surname', surname, {
          shouldValidate: true,
          shouldDirty: true
        });
        setValue('email_address', email_address, {
          shouldValidate: true,
          shouldDirty: true
        });
        setValue('phone_number', phone_number, {
          shouldValidate: true,
          shouldDirty: true
        });
        setValue('fiscal_code', fiscal_code, {
          shouldValidate: true,
          shouldDirty: true
        });
      }

      if (addressForm) {
        setValue('address', addressForm, {
          shouldValidate: true,
          shouldDirty: true
        });
      }

      if (type) {
        setValue('type', type, {
          shouldValidate: true,
          shouldDirty: true
        });
      }

      if (subject) {
        setValue('subject', subject, {
          shouldValidate: true,
          shouldDirty: true
        });
      }

      if (details) {
        setValue('details', details, {
          shouldValidate: true,
          shouldDirty: true
        });
        countCharDetail(details);
      }
      if (severity) {
        setValue('severity', severity, {
          shouldValidate: true,
          shouldDirty: true
        })
      }
    } else if (currentUser.nome !== 'Anonymous') {
      setValue('name', currentUser.nome, {
        shouldValidate: true,
        shouldDirty: true
      });
      setValue('surname', currentUser.cognome, {
        shouldValidate: true,
        shouldDirty: true
      });
      setValue('email_address', currentUser.email, {
        shouldValidate: true,
        shouldDirty: true
      });
      setValue('fiscal_code', currentUser.codice_fiscale, {
        shouldValidate: true,
        shouldDirty: true
      });
      setValue('phone_number', currentUser?.cellulare , {
        shouldValidate: true,
        shouldDirty: true
      });

      setValue('details', details || application?.data?.details, {
        shouldValidate: false,
        shouldDirty: true
      });

      setValue('severity', severity || application?.data?.severity, {
        shouldValidate: false,
        shouldDirty: true
      });

      setValue('subject', subject || application?.data?.subject, {
        shouldValidate: false,
        shouldDirty: true
      });

      setValue('address', addressForm || application.data?.address, {
        shouldValidate: true,
        shouldDirty: true
      });

      setValue('type', type || application?.data?.type, {
        shouldValidate: true,
        shouldDirty: true
      });

      if (application?.data?.images?.length) {
        dispatch(storeLoadImages(application.data.images));
      }
      if (application?.data?.docs?.length) {
        dispatch(storeLoadDocs(application.data.docs));
      }
      if (application?.data?.privacy) {
        dispatch(storePrivacy(application.data.privacy || true));
      }
      if(application?.data?.severity){
        dispatch(storeSeverity(application.data.severity));
      }
    }
  },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [
    currentUser,
    application
  ]);

  useEffect(() => {
    if (!getValues().address && !(position && position.length > 0) && boundingBox && boundingBox.length > 0) {
      const bbArray = boundingBox.split(',');
      const lat = (Number(bbArray[1])+Number(bbArray[3]))/2
      const lon = (Number(bbArray[0])+Number(bbArray[2]))/2
      setPosition([lat, lon]);
    }
  }, [boundingBox, position, getValues])

  useEffect(() => {
    if (/Mobi|Android/i.test(navigator.userAgent)) {
      setShowUsePosition(true)
    }
  }, []);

  const handleClick = () => {
    inputRef.current.click();
    inputRef.current.value = null;
  };

  const handleClickDoc = () => {
    inputRefDoc.current.click();
    inputRefDoc.current.value = null;
  };

  const handleFileChange = async (event) => {
    setIsFileLoading(true);

    const countNumberFiles = Array.from(event.target.files).length + docs.length
    if (countNumberFiles > 3) {
      notify(t('file_limit_reached'), t('file_limit_reached_error'), {
        dismissable: true,
        state: 'warning',
        closeOnClick: true,
        duration:10000
      });
      setIsFileLoading(false);
      return;
    }
    for await (const fileObj of Array.from(event.target.files)) {
      if (!fileObj) {
        setIsFileLoading(false);
        return;
      }

      if (fileObj && fileObj.size > 6000000) {
        notify(t('file_limit_size'), {
          dismissable: true,
          state: 'error',
          closeOnClick: true,
          duration:10000
        });
        setIsFileLoading(false);
        return;
      }

      const extension = fileObj.name.match(/\.[0-9a-z]+$/i);
      const isValidFile = fileObj.type.includes('/pdf');

      if (!isValidFile) {
        notify(t('invalid_file_type'), {
          dismissable: true,
          state: 'error',
          closeOnClick: true,
          duration:10000
        });
        setIsFileLoading(false);
        return;
      }

      if (docs && docs.length >= 3) {
        notify(t('file_limit_reached'), t('file_limit_reached_error'), {
          dismissable: true,
          state: 'warning',
          closeOnClick: true,
          duration:10000
        });
        setIsFileLoading(false);
        return;
      }

      if (docs && docs.length > 0) {
        const sameNameFile = docs.filter((loadedImg) => loadedImg.originalName === fileObj.name);
        if (sameNameFile.length) {
          notify(t('file_limit_upload_error'), t('file_same_upload_error'), {
            dismissable: true,
            state: 'warning',
            closeOnClick: true,
            duration:10000
          });
          setIsFileLoading(false);
          return;
        }
      }

      let reader = new FileReader();
      reader.readAsDataURL(fileObj);
      reader.onload = function () {
        let storeData = {
          file: null,
          preview: null,
          filename: 'doc-' + Date.now() + extension[0]
        };
        const dataUpload = {
          name: storeData.filename,
          original_filename: fileObj.name,
          size: fileObj.size,
          protocol_required: false,
          mime_type: fileObj.type
        };
        setProgressBar(10);
        dispatch(uploadsActions.uploadS3(dataUpload))
          .then((response) => {
            const idUpload = response.payload.id;
            const uri = response.payload.uri;
            setProgressBar(20);
            const calcSecondsUpload = fileObj.size / 10 / 50; // (bytes / 10) / upload speed
            setTimeout(() => {
              setProgressBar(80);
            }, calcSecondsUpload);
            dispatch(
              uploadsActions.uploadS3PUT({
                uri: response.payload.uri,
                file: fileObj
              })
            )
              .then((res) => {
                setProgressBar(90);
                const dataUpload = {
                  file_hash: res.payload.etag.etag,
                  check_signature: false
                };
                dispatch(
                  uploadsActions.uploadS3PUTETAG({
                    data: dataUpload,
                    id: idUpload
                  })
                )
                  .then((res) => {
                    setProgressBar(100);
                    const completeStoredImage = {
                      name: storeData.filename,
                      url: window.OC_BASE_URL
                        ? `${window.OC_BASE_URL}/allegati/${idUpload}`
                        : `${process.env.REACT_APP_API_URL}/allegati/${idUpload}`,
                      size: fileObj.size,
                      mime_type: fileObj.type,
                      data: {
                        id: idUpload,
                        uri: uri
                      },
                      originalName: fileObj.name,
                      hash: dataUpload.file_hash,
                      preview: storeData.preview,
                      storage: "url"
                    };
                    setIsFileLoading(false);
                    dispatch(storeDocs(completeStoredImage));
                  })
                  .catch(() => {
                    notify(t('warning'), t('error_generic_files'), {
                      dismissable: true,
                      state: 'error',
                      closeOnClick: true,
                      duration:10000
                    });
                    setIsFileLoading(false);
                  });
              })
              .catch(() => {
                notify(t('warning'), t('error_generic_files'), {
                  dismissable: true,
                  state: 'error',
                  closeOnClick: true,
                  duration:10000
                });
                setIsFileLoading(false);
              });
          })
          .catch(() => {
            notify(t('warning'), t('error_generic_files'), {
              dismissable: true,
              state: 'error',
              closeOnClick: true,
              duration:10000
            });
            setIsFileLoading(false);
          });
      };
      reader.onerror = function (error) {
        setIsFileLoading(false);
        console.log('Error: ', error);
      };
    }
  };

  const handleImageChange = async (event) => {
    setIsImageLoading(true);

    const countNumberImage = Array.from(event.target.files).length + images.length
    if (countNumberImage > 3) {
      notify(t('file_limit_reached'), t('file_limit_reached_error'), {
        dismissable: true,
        state: 'warning',
        closeOnClick: true,
        duration:10000
      });
      setIsImageLoading(false);
      return;
    }

    for await (const fileObj of Array.from(event.target.files)) {
      if (!fileObj) {
        setIsImageLoading(false);
        return;
      }

      if (fileObj && fileObj.size > 6000000) {
        notify(t('file_limit_size'), {
          dismissable: true,
          state: 'error',
          closeOnClick: true,
          duration:10000
        });
        setIsImageLoading(false);
        return;
      }

      const extension = fileObj.name.match(/\.[0-9a-z]+$/i);
      const isValid = fileObj.type.startsWith('image/');

      if (!isValid) {
        notify(t('invalid_file_type'), {
          dismissable: true,
          state: 'error',
          closeOnClick: true,
          duration:10000
        });
        setIsImageLoading(false);
        return;
      }

      if (images && images.length >= 3) {
        notify(t('file_limit_reached'), t('file_limit_reached_error'), {
          dismissable: true,
          state: 'warning',
          closeOnClick: true,
          duration:10000
        });
        setIsImageLoading(false);
        return;
      }

      if (images && images.length > 0) {
        const sameNameFile = images.filter((loadedImg) => loadedImg.originalName === fileObj.name);
        if (sameNameFile.length) {
          notify(t('file_limit_upload_error'), t('file_same_upload_error'), {
            dismissable: true,
            state: 'warning',
            closeOnClick: true,
            duration:10000
          });
          setIsImageLoading(false);
          return;
        }
      }

      let reader = new FileReader();
      reader.readAsDataURL(fileObj);
      reader.onload = function () {
        let storeData = {
          file: null,
          preview: null,
          filename: 'img-' + Date.now() + extension[0]
        };
        resizeImage(reader.result, 50, 50).then((result) => {
          storeData = {
            file: reader.result.replace(/data:.+?,/, ''),
            preview: result,
            filename: 'img-' + Date.now() + extension[0]
          };
        });
        const dataUpload = {
          name: storeData.filename,
          original_filename: fileObj.name,
          size: fileObj.size,
          protocol_required: false,
          mime_type: fileObj.type
        };

        setProgressBar(10);

        dispatch(uploadsActions.uploadS3(dataUpload))
          .then((response) => {
            const idUpload = response.payload.id;
            const uri = response.payload.uri;
            setProgressBar(20);

            const calcSecondsUpload = fileObj.size / 10 / 50; // (bytes / 10) / upload speed
            setTimeout(() => {
              setProgressBar(80);
            }, calcSecondsUpload);
            dispatch(
              uploadsActions.uploadS3PUT({
                uri: response.payload.uri,
                file: fileObj
              })
            )
              .then((res) => {
                setProgressBar(90);
                const dataUpload = {
                  file_hash: res.payload.etag.etag,
                  check_signature: false
                };
                dispatch(
                  uploadsActions.uploadS3PUTETAG({
                    data: dataUpload,
                    id: idUpload
                  })
                )
                  .then((res) => {
                    setProgressBar(100);
                    const completeStoredImage = {
                      name: storeData.filename,
                      url: window.OC_BASE_URL
                        ? `${window.OC_BASE_URL}/allegati/${idUpload}`
                        : `${process.env.REACT_APP_API_URL}/allegati/${idUpload}`,
                      size: fileObj.size,
                      mime_type: fileObj.type,
                      data: {
                        id: idUpload,
                        uri: uri
                      },
                      originalName: fileObj.name,
                      hash: dataUpload.file_hash,
                      preview: storeData.preview,
                      storage: "url"
                    };
                    setIsImageLoading(false);
                    dispatch(storeImages(completeStoredImage));
                  })
                  .catch(() => {
                    notify(t('warning'), t('error_generic_files'), {
                      dismissable: true,
                      state: 'error',
                      closeOnClick: true,
                      duration:10000
                    });
                    setIsImageLoading(false);
                  });
              })
              .catch(() => {
                notify(t('warning'), t('error_generic_files'), {
                  dismissable: true,
                  state: 'error',
                  closeOnClick: true,
                  duration:10000
                });
                setIsImageLoading(false);
              });
          })
          .catch(() => {
            notify(t('warning'), t('error_generic_files'), {
              dismissable: true,
              state: 'error',
              closeOnClick: true,
              duration:10000
            });
            setIsImageLoading(false);
          });
      };
      reader.onerror = function (error) {
        setIsImageLoading(false);
        console.log('Error: ', error);
      };
    }
  };

  function getLocation() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        let lat = position.coords.latitude;
        let lon = position.coords.longitude;
        const geographic_areas_ids = service?.geographic_areas_id || [];

        if (geographic_areas_ids.length > 0) {
          dispatch(apiActions.getGeographicAreasPoints({ lat, lon, geographic_areas_ids }))
            .unwrap()
            .then((response) => {
              if (!response.result) {
                setModalMessages(!isOpenModalMessages);
                setMessage(t('error_message_position'));
              } else {
                setPosition([lat, lon]);
                setValue('address', { ...address, lat: lat, lon: lon });
                dispatch(storeAddress(address));
                toggleModalMap(!isOpenMap);
              }
            });
        } else {
          setPosition([lat, lon]);
          setValue('address', { ...address, lat: lat, lon: lon });
          dispatch(storeAddress(address));
          toggleModalMap(!isOpenMap);
        }
      },
      (errors) => {
        if (errors?.code === 1) {
          setModalMessages(!isOpenModalMessages);
          setMessage(t('error_message_code_1'));
        } else if (errors?.code === 2) {
          setModalMessages(!isOpenModalMessages);
          setMessage(t('error_message_code_2'));
        } else if (errors?.code === 3) {
          setModalMessages(!isOpenModalMessages);
          setMessage(t('error_message_code_3'));
        } else {
          setModalMessages(!isOpenModalMessages);
          setMessage(t('error_message_generic'));
        }
      }
    );
  }

  function onchangeAddress(data) {
    let lat = data?.lat || null;
    let lon = data?.lon || null;
    if (lat && lon) {
      setPosition([lat, lon]);
    } else {
      setPosition([]);
    }

    const geographic_areas_ids = service?.geographic_areas_id || [];
    if (geographic_areas_ids.length > 0 && lat && lon) {
      dispatch(apiActions.getGeographicAreasPoints({ lat, lon, geographic_areas_ids }))
        .unwrap()
        .then((response) => {
          if (!response.result) {
            setModalMessages(!isOpenModalMessages);
            setMessage(t('error_message_position'));
            setValue('address', '');
            dispatch(storeAddress(''));
            setPosition([]);
          } else {
            setValue('address', data);
            dispatch(storeAddress(data));
          }
        });
    } else {
      setValue('address', data);
      dispatch(storeAddress(data));
    }
  }

  function onChangeService(e) {
    setValue('type', e);
    dispatch(storeType(e));
  }

  const onSubmit = (data) => {
    dispatch(storeName(data.name));
    dispatch(storeSurname(data.surname));
    dispatch(storeEmail(data.email_address));
    dispatch(storePhoneNumber(data.phone_number));
    dispatch(storeFiscalCode(data.fiscal_code));
    dispatch(storeType(data.type));
    dispatch(storeDetails(data.details));
    dispatch(storeSeverity(data.severity));
    dispatch(storeSubject(data.subject));
    dispatch(storeAddress(data.address));
    dispatch(storePrivacy(privacy));
    onNextPage(activeStep + 1);
  };

  const onPrev = () => {
    onNextPage(activeStep - 1);
  };

  function saveAddress() {
    let lat = address?.lat || null;
    let lon = address?.lon || null;
    setPosition([lat, lon]);
    const geographic_areas_ids = service?.geographic_areas_id || [];
    if (geographic_areas_ids.length > 0 && lat && lon) {
      dispatch(apiActions.getGeographicAreasPoints({ lat, lon, geographic_areas_ids }))
        .unwrap()
        .then((response) => {
          if (!response.result) {
            setModalMessages(!isOpenModalMessages);
            setMessage(t('error_message_position'));
            setValue('address', '');
            dispatch(storeAddress(''));
          } else {
            toggleModalMap(!isOpenMap);
            setValue('address', address);
            dispatch(storeAddress(address));
          }
        });
    } else {
      toggleModalMap(!isOpenMap);
      dispatch(apiActions.getAddress({ ...address }));
      setValue('address', address);
      dispatch(storeAddress(address));
    }
  }

  function closeModalAddress() {
    toggleModalMap(!isOpenMap);
  }

  //Save application in draft
  const saveApplication = () => {
    const getId =
      application?.id || application?.data?.length
        ? application?.id || application?.data[0]?.id
        : null;
    const dataApplication = {
      ...(getId && { id: getId }),
      service: service?.identifier || 'inefficiencies',
      data: {
        applicant: {
          data: {
            email_address: getValues().email_address,
            phone_number: getValues().phone_number,
            completename: {
              data: {
                name: getValues().name,
                surname: getValues().surname
              }
            },
            fiscal_code: {
              data: {
                fiscal_code: getValues().fiscal_code
              }
            }
          }
        },
        type: getValues().type,
        details: getValues().details,
        subject: getValues().subject,
        ...(getValues().address && { address: getValues().address }),
        images: images,
        docs: docs,
        privacy: privacy,
        severity:getValues().severity
      },
      status: '1000'
    };

    if (dataApplication?.id) {
      return dispatch(
        apiActions.updateApplication({
          data: dataApplication,
          id: dataApplication.id
        })
      ).then(() => {
        notify(t('request_saved_successfully'), {
          dismissable: true,
          state: 'success',
          duration: 6000
        });
        dispatch(apiActions.getDraftApplication());
      });
    } else {
      return dispatch(apiActions.createApplication(dataApplication))
        .unwrap()
        .then(() => {
          notify(t('request_saved_successfully'), {
            dismissable: true,
            state: 'success',
            duration: 6000
          });
        })
        .catch((e) => {
          notify(t('error_generic'), e.message, {
            dismissable: true,
            state: 'error',
            duration: 6000
          });
        });
    }
  };

  function onChangeSubject(e) {
    dispatch(storeSubject(e.target.value));
  }

  function onChangeDetails(e) {
    dispatch(storeDetails(e.target.value));
  }

  function onChangeSeverity(e) {
    dispatch(storeSeverity(e.target.value));
  }

  // memorize the component
  const AutocompleteDropdownIndicator = useCallback(
    (props) => {
      return (
        <div>
          <components.DropdownIndicator {...props}>
            <span style={{ padding: '0px 5px' }} aria-hidden="true">
              {!props.hasValue ? (
                <Icon title={t('placeholder_address_report')} icon="it-search" aria-hidden size="sm" />
              ) : null}
            </span>
          </components.DropdownIndicator>
        </div>
      );
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  // Get by api address leaflet
  const getData = async (inputValue) => {
    let res = await dispatch(apiActions.searchAddress({ params: inputValue, boundingBox: boundingBox, language : i18n.resolvedLanguage }));
    return res.payload;
  };

  const isInAreas = async (lat, lon, geographic_areas_ids) => {
    return await dispatch(apiActions.getGeographicAreasPoints({ lat, lon, geographic_areas_ids }))
      .unwrap()
      .then((response) => {
        return response.result
      })
  }

  const dataOption = async (inputValue) =>
    getData(inputValue).then(async (res) => {
      let data = await Promise.all(
        res.map(async (r) => {
          const geographic_areas_ids = service?.geographic_areas_id || [];
          const lat = r.lat;
          const lon = r.lon;
          if (
            geographic_areas_ids.length > 0 &&
            lat &&
            lon &&
            boundingBox &&
            boundingBox.length > 0
          ) {
            return {
              ...r,
              inAreas: await isInAreas(lat, lon, geographic_areas_ids)
            };
          } else {
            return {
              ...r,
              inAreas: true
            };
          }
        })
      );
      data = data
        .filter((f) => f.inAreas === true)
        .map((t) => ({
          value: t.place_id,
          label: locationNameFormatter(t),
          ...t
        }));
      if (data) {
        data = uniqWith(data, (a, c) => a.label === c.label);
      }
      return data;
    });


  const loadOptions = async (inputValue, callback) => {
    if (inputValue && inputValue.length > 2) {
      const data = await dataOption(inputValue, callback);
      callback(data);
    }
  };

  function checkCharCount(e) {
    countCharDetail(e.target.value)
  }

  const isExternalRefCategory = () => (
    category?.external_ref
  )

  useLayoutEffect(() =>{
    if(document.readyState === 'complete'){
      let version = parseFloat(getComputedStyle(document.documentElement).getPropertyValue('--bootstrap-italia-version').replace('"',''))
      setBtsCssVersion(version)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[document.readyState])


  const checkIsValidCF = (val) => {
    if (currentUser?.nome !== 'Anonymous' || required_fields.includes("fiscal_code")) {
      if (CodiceFiscale.check(val)) {
        return true
      } else {
        return t('fiscal_code_validator');
      }
    } else {
      if(val){
        if (CodiceFiscale.check(val)) {
          return true
        } else {
          return t('fiscal_code_validator');
        }
      }
    }
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Row>
        <Col lg={'3'} className="d-lg-block mb-4 d-none">
          <div className="cmp-navscroll sticky-top" aria-labelledby="accordion-title-one">
            <nav className="navbar it-navscroll-wrapper navbar-expand-lg" aria-label={t('info')}>
              <div className="navbar-custom" id="navbarNavProgress">
                <div className="menu-wrapper">
                  <div className="link-list-wrapper">
                    <Accordion>
                      <AccordionItem>
                        <AccordionHeader
                          id={'accordion-title-one'}
                          className="pb-10 px-3"
                          active={collapseElementOpen === '1'}
                          onToggle={() =>
                            setCollapseElement(collapseElementOpen !== '1' ? '1' : '')
                          }
                        >
                          {' '}
                          {t('info')}{' '}
                          {btsCssVersion < 2.7 ? (<Icon icon={'it-expand'} size={'xs'} className={'right'}></Icon>) : null}
                        </AccordionHeader>{' '}
                        <div className="progress">
                          <div
                            className="progress-bar it-navscroll-progressbar"
                            role="progressbar"
                            aria-valuenow={percentage.vertical}
                            aria-valuemin="0"
                            aria-valuemax="100"
                            style={{
                              width: percentage.vertical + '%'
                            }}
                          ></div>
                        </div>
                        <AccordionBody active={collapseElementOpen === '1'}>
                          <div
                            id="collapse-one"
                            className="accordion-collapse collapse show"
                            role="region"
                            aria-labelledby="accordion-title-one"
                          >
                            <div className="accordion-body">
                              <LinkList data-element="page-index">
                                <NavItem>
                                  <HashLink
                                    smooth
                                    className={getActiveClass('report-place')}
                                    to="#report-place"
                                  >
                                    <span className="title-medium">{t('place')}</span>
                                  </HashLink>
                                </NavItem>
                                <NavItem>
                                  <HashLink
                                    smooth
                                    className={getActiveClass('malfunction')}
                                    to="#malfunction"
                                  >
                                    <span className="title-medium">{t('malfunction')}</span>
                                  </HashLink>
                                </NavItem>
                                <NavItem>
                                  <HashLink
                                    smooth
                                    className={getActiveClass('applicant')}
                                    to="#applicant"
                                  >
                                    <span className="title-medium">{t('report_author')}</span>
                                  </HashLink>
                                </NavItem>
                              </LinkList>
                            </div>
                          </div>
                        </AccordionBody>
                      </AccordionItem>
                    </Accordion>
                  </div>
                </div>
              </div>
            </nav>
          </div>
        </Col>
        <Col lg={'8'} className="offset-lg-1">
          <div className="steppers-content" aria-live="polite">
            <div className="it-page-sections-container">
              <section className="it-page-section" id="report-place" {...registerScroll('report-place')}>
                <div className="cmp-card mb-40">
                  <div className="card has-bkg-grey shadow-sm p-big p-lg-4">
                    <div className="card-header border-0 p-0 mb-lg-30 m-0">
                      <div className="d-flex">
                        <h2 className="title-xxlarge mb-1">{t('place')}</h2>
                      </div>
                      <p className="subtitle-small mb-0">{t('place_malfunction')}</p>
                    </div>
                    <div className="card-body p-0">
                      <div className="cmp-input-autocomplete">
                        <div className="form-group bg-white p-3 mb-0 mt-3">
                          <WrapperClassSelect>
                            <Controller
                              name="address"
                              control={control}
                              rules={{ required: required_fields.includes("address") }}
                              render={({ field }) => (
                                <FormGroup>
                                  <AsyncSelect
                                    ref={field.ref}
                                    value={field.value}
                                    className="react-select-container"
                                    classNamePrefix="react-autocomplete react-select"
                                    placeholder={`${t('placeholder_address_report')}${required_fields.includes("address") ? "*" : ""}`}
                                    noOptionsMessage={(el) => {
                                      const input = el?.inputValue;
                                      if (input && input.length > 0) {
                                        if (input.split(' ').length > 1) {
                                          return (
                                            <div>
                                              <div>{t('no_result')} </div>
                                              {Boolean(position && position.length > 1) && (
                                                <NavLink
                                                  className="list-item active icon-left p-0 cursor-pointer"
                                                  onClick={() => toggleModalMap(!isOpenMap)}
                                                >
                                                  <span className="list-item-title-icon-wrapper">
                                                    <Icon
                                                      icon={'it-map-marker-circle'}
                                                      size={'sm'}
                                                      color={'primary'}
                                                      className={'mb-1'}
                                                      title={t('free_search_on_the_map')}
                                                    ></Icon>
                                                    <span className="list-item-title t-primary">
                                                      {t('free_search_on_the_map')}
                                                    </span>
                                                  </span>
                                                </NavLink>
                                              )}
                                            </div>
                                          );
                                        } else {
                                          return (
                                            <div>
                                              <div>{t('no_result_try_more_specific_address')} </div>
                                              {Boolean(position && position.length > 1) && (
                                                <NavLink
                                                  className="list-item active icon-left p-0 cursor-pointer"
                                                  onClick={() => toggleModalMap(!isOpenMap)}
                                                >
                                                  <span className="list-item-title-icon-wrapper">
                                                    <Icon
                                                      icon={'it-map-marker-circle'}
                                                      size={'sm'}
                                                      color={'primary'}
                                                      className={'mb-1'}
                                                      title={t('free_search_on_the_map')}
                                                    ></Icon>
                                                    <span className="list-item-title t-primary">
                                                      {t('free_search_on_the_map')}
                                                    </span>
                                                  </span>
                                                </NavLink>
                                              )}
                                            </div>
                                          );
                                        }
                                      } else {
                                        return t('no_items');
                                      }
                                    }}
                                    aria-label={t('placeholder_address_report')}
                                    components={{
                                      DropdownIndicator: AutocompleteDropdownIndicator,
                                      IndicatorSeparator: null
                                    }}
                                    loadOptions={debounce(loadOptions,1000)}
                                    isClearable={true}
                                    onInputChange={debounce((e) => {
                                     // onchangeAddress(e);
                                      if(e && e.length){
                                        field.onChange(e);
                                      }
                                    },1000)
                                    }
                                      onChange={(e) => {
                                        if (e) {
                                          e.display_name = e.label;
                                        }
                                        field.onChange(e);
                                        onchangeAddress(e);
                                    }
                                  }
                                  />
                                  <label htmlFor="autocomplete-address" className="visually-hidden">
                                    {t('placeholder_address_report')}
                                  </label>
                                </FormGroup>
                              )}
                            />
                            {Boolean(getValues().address && position && position.length > 1) ? (
                              <div className="link-wrapper mt-3">
                                <NavLink
                                  className="list-item active icon-left p-0 cursor-pointer"
                                  onClick={() => toggleModalMap(!isOpenMap)}
                                >
                                  <span className="list-item-title-icon-wrapper">
                                    <Icon
                                      icon={'it-map-marker-circle'}
                                      size={'sm'}
                                      color={'primary'}
                                      className={'mb-1'}
                                      title={t('locate_on_map')}
                                    ></Icon>
                                    <span className="list-item-title t-primary">
                                      {t('locate_on_map')}
                                    </span>
                                  </span>
                                </NavLink>
                              </div>
                            ) : (
                              <>
                                {Boolean(showUsePosition) &&
                                  <div className="link-wrapper mt-3">
                                    <NavLink
                                      className="list-item active icon-left p-0 cursor-pointer"
                                      onClick={() => getLocation()}
                                    >
                                      <span className="list-item-title-icon-wrapper">
                                        <Icon
                                          icon={'it-map-marker'}
                                          size={'sm'}
                                          color={'primary'}
                                          className={'mb-1'}
                                          title={t('use_position')}
                                        ></Icon>
                                        <span className="list-item-title t-primary">
                                          {t('use_position')}
                                        </span>
                                      </span>
                                    </NavLink>
                                  </div>
                                }
                                {Boolean(position && position.length > 1) && (
                                  <div className="link-wrapper mt-3">
                                    <NavLink
                                      className="list-item active icon-left p-0 cursor-pointer"
                                      onClick={() => toggleModalMap(!isOpenMap)}
                                    >
                                      <span className="list-item-title-icon-wrapper">
                                        <Icon
                                          icon={'it-map-marker-circle'}
                                          size={'sm'}
                                          color={'primary'}
                                          className={'mb-1'}
                                          title={t('free_search_on_the_map')}
                                        ></Icon>
                                        <span className="list-item-title t-primary">
                                          {t('free_search_on_the_map')}
                                        </span>
                                      </span>
                                    </NavLink>
                                  </div>
                                )}
                              </>
                            )}
                          </WrapperClassSelect>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section className="it-page-section" id="report-info" {...registerScroll('malfunction')}>
                <div className="cmp-card mb-40">
                  <div className="card has-bkg-grey shadow-sm p-big">
                    <div className="card-header border-0 p-0 mb-lg-30 m-0">
                      <div className="d-flex">
                        <h2 className="title-xxlarge mb-3 icon-required">{t('malfunction')}</h2>
                      </div>
                    </div>
                    <div className="card-body p-0">
                      <WrapperClassSelect className="select-wrapper p-md-3 p-lg-4 pb-lg-0 select-partials">
                        <Controller
                          name="type"
                          control={control}
                          rules={{ required: required_fields.includes("type") }}
                          render={({ field }) => (
                            <Select
                              id="selectType"
                              className="react-select-container"
                              classNamePrefix="react-select"
                              options={categories[i18n.resolvedLanguage] || []}
                              placeholder={`${t('type_report')}${required_fields.includes('type') ? "*":""}`}
                              aria-label={t('placeholder_type_report')}
                              invalid={errors.type?.type === 'required' ? true : false}
                              infoText={
                                errors.type?.type === 'required' ? t('required_field') : false
                              }
                              innerRef={field.ref}
                              value={field.value}
                              isClearable={true}
                              isSearchable={true}
                              getOptionLabel={(option) => option.label}
                              getOptionValue={(option) => option.value}
                              onChange={(e) => {
                                field.onChange(e);
                                onChangeService(e);
                              }}
                              noOptionsMessage={() => t('no_items')}
                            />
                          )}
                        />
                        <ExternalRef data={category?.external_ref} />
                      </WrapperClassSelect>
                      {Boolean(!isExternalRefCategory()) && (<>
                      <div className="text-area-wrapper p-3 px-lg-4 pt-lg-5 pb-lg-1 bg-white">
                        <Controller
                          name="subject"
                          control={control}
                          rules={{
                            required: {
                              value: true,
                              message: t('required_field')
                            },
                            maxLength: {
                              value: 200,
                              message: t('enter_max_char_subject')
                            }
                          }}
                          render={({ field }) => (
                            <Input
                              id={'subject'}
                              type="text"
                              className={'cmp-input mb-0'}
                              label={t('title') + '*'}
                              placeholder={''}
                              invalid={
                                errors.subject?.message ? true : false
                              }
                              validationText={
                                errors.subject?.message
                              }
                              innerRef={field.ref}
                              value={field.value}
                              onChange={(e) => {
                                field.onChange(e);
                                onChangeSubject(e);
                              }}
                            />
                          )}
                        />
                      </div>
                      <div className="cmp-text-area m-0 p-3 px-lg-4 pt-lg-5 pb-lg-4 bg-white">
                        <Controller
                          name="details"
                          control={control}
                          rules={{
                            required: {
                              value: true,
                              message: t('required_field')
                            },
                            maxLength: {
                              value: 800,
                              message: t('enter_max_char_info')
                            }
                          }}
                          render={({ field }) => (
                            <TextArea
                              id={'details'}
                              className={'text-area'}
                              label={t('description') + '*'}
                              rows={5}
                              placeholder={''}
                              maxLength={800}
                              onLoad={(e) => checkCharCount(e)}
                              invalid={
                                errors.details?.message ? true : false
                              }
                              validationText={
                                errors.details?.message
                              }
                              innerRef={field.ref}
                              value={field.value}
                              onChange={(e) => {
                                field.onChange(e);
                                onChangeDetails(e);
                                checkCharCount(e);
                              }}
                            />
                          )}
                        />
                        <div className="title-xsmall u-grey-dark mb-0 text-end pt-1 count-message"  aria-hidden={charCount > 10}>
                          <p>{charCount} {t('remaining_characters')}</p>
                        </div>
                      </div>
                      {Boolean(window.OC_SHOW_SEVERITY_FIELD === true ||
                        process.env.REACT_APP_OC_SHOW_SEVERITY_FIELD === 'true') && (
                        <div className="px-3 pt-2 pb-3 px-lg-4 pb-lg-4 pt-lg-0 bg-white">
                        <Controller
                            name="severity"
                            control={control}
                            rules={{ required: required_fields.includes("severity") }}
                            render={({ field }) => (
                              <fieldset>
                                <legend className="title-xsmall-bold u-grey-dark" aria-labelledby="severityDescription">
                                  {t('severity_label')}{required_fields.includes("severity")?"*":""}
                                </legend>
                                <FormGroup check inline className='ps-1'>
                                  <Input
                                    name='gruppo1'
                                    type='radio'
                                    value='1'
                                    id='radio1'
                                    checked={severity === '1' }
                                    onChange={(e) => {
                                      field.onChange(e);
                                      onChangeSeverity(e);
                                    }}
                                    infoText={'1/5'}
                                    />
                                  <Label check id='radio1Description' htmlFor='radio1'>
                                    1
                                  </Label>
                                  <p id='radio1Description' className='visually-hidden'>
                                    1/5
                                  </p>
                                </FormGroup>
                                <FormGroup check inline>
                                  <Input
                                    name='gruppo1'
                                    type='radio'
                                    value='2'
                                    id='radio2'
                                    checked={severity === '2' }
                                    onChange={(e) => {
                                      field.onChange(e);
                                      onChangeSeverity(e);
                                    }}
                                    />
                                  <Label check htmlFor='radio2'>
                                    2
                                  </Label>
                                  <p id='radio2Description' className='visually-hidden'>
                                    2/5
                                  </p>
                                </FormGroup>
                                <FormGroup check inline>
                                  <Input
                                    name='gruppo1'
                                    type='radio'
                                    value='3'
                                    id='radio3'
                                    checked={severity === '3' }
                                    onChange={(e) => {
                                      field.onChange(e);
                                      onChangeSeverity(e);
                                    }}
                                    />
                                  <Label check htmlFor='radio3'>
                                    3
                                  </Label>
                                  <p id='radio3Description' className='visually-hidden'>
                                    3/5
                                  </p>
                                </FormGroup>
                                <FormGroup check inline>
                                  <Input
                                    name='gruppo1'
                                    type='radio'
                                    value='4'
                                    id='radio4'
                                    checked={severity === '4' }
                                    onChange={(e) => {
                                      field.onChange(e);
                                      onChangeSeverity(e);
                                    }}
                                    />
                                  <Label check htmlFor='radio4'>
                                    4
                                  </Label>
                                  <p id='radio4Description' className='visually-hidden'>
                                    4/5
                                  </p>
                                </FormGroup>
                                <FormGroup check inline>
                                  <Input
                                    name='gruppo1'
                                    type='radio'
                                    value='5'
                                    id='radio5'
                                    checked={severity === '5' }
                                    onChange={(e) => {
                                      field.onChange(e);
                                      onChangeSeverity(e);
                                    }}
                                    />
                                  <Label check htmlFor='radio5'>
                                    5
                                  </Label>
                                  <p id='radio5Description' className='visually-hidden'>
                                    5/5
                                  </p>
                                </FormGroup>
                                <p id='severityDescription' className="title-xsmall u-grey-dark px-2 mb-0">
                              {t('severity_description')}
                            </p>
                              </fieldset>
                            )}
                          />
                        </div>
                      )}
                      <div className="btn-wrapper px-3 pt-2 pb-3 px-lg-4 pb-lg-4 pt-lg-0 bg-white">
                        <label
                          className="title-xsmall-bold u-grey-dark pb-2 ms-2"
                          htmlFor={'imageFile'}
                        >
                          {t('images')}
                        </label>
                        {images &&
                          images.map((img, index) => (
                            <ImageList file={img} key={index + 'img'}></ImageList>
                          ))}
                        <div className={images && images.length === 3 ? 'd-none' : ''}>
                          <button
                            type="button"
                            aria-label={t('load_images')}
                            className={`${
                              isImageLoading ? 'btn-progress disabled' : ''
                            } btn btn-primary w-100 fw-bold `}
                            onClick={() => handleClick()}
                            disabled={isFileLoading}
                          >
                            <input
                              id={'imageFile'}
                              ref={inputRef}
                              type="file"
                              onChange={handleImageChange}
                              accept=".png,.jpg,.jpeg"
                              className="d-none upload"
                              multiple="multiple"
                              disabled={
                                isImageLoading || (images && images.length === 3) || !images
                              }
                            />
                            <span className="rounded-icon">
                              {isImageLoading ? (
                                <span className={'d-flex justify-content-center'}>
                                  <Spinner small active /> <span>{t('loading')}...</span>{' '}
                                </span>
                              ) : (
                                <span>
                                  <Icon
                                    icon={'it-upload'}
                                    size={'sm'}
                                    color={'primary'}
                                    className={'icon-white'}
                                  ></Icon>
                                  <span>{t('load_file')}</span>
                                </span>
                              )}
                            </span>

                            {isImageLoading ? (
                              <span>
                                <Progress value={progressBar} color={'primary'} />
                              </span>
                            ) : (
                              ''
                            )}
                          </button>
                          <p className="title-xsmall u-grey-dark pt-10 mb-0">
                            {t('select_max_images')}
                          </p>
                        </div>
                      </div>

                      <div className="btn-wrapper px-3 pt-2 pb-3 px-lg-4 pb-lg-4 pt-lg-0 bg-white">
                        <label
                          className="title-xsmall-bold u-grey-dark pb-2 ms-2"
                          htmlFor={'fileDocument'}
                        >
                          {t('document')}
                        </label>
                        {docs &&
                          docs.map((doc, index) => (
                            <ImageList file={doc} key={index + 'doc'}></ImageList>
                          ))}
                        <div className={docs && docs.length === 3 ? 'd-none' : ''}>
                          <button
                            type="button"
                            aria-label={t('load_docs')}
                            className={`${
                              isFileLoading ? 'btn-progress disabled' : ''
                            } btn btn-primary w-100 fw-bold `}
                            onClick={() => handleClickDoc()}
                            disabled={isFileLoading}
                          >
                            <input
                              id={'fileDocument'}
                              ref={inputRefDoc}
                              type="file"
                              onChange={handleFileChange}
                              className="d-none upload"
                              multiple="multiple"
                              accept=".pdf"
                              disabled={isFileLoading || (docs && docs.length === 3) || !docs}
                            />
                            <span className="rounded-icon">
                              {isFileLoading ? (
                                <span className={'d-flex justify-content-center'}>
                                  <Spinner small active /> <span>{t('loading')}...</span>{' '}
                                </span>
                              ) : (
                                <span>
                                  <Icon
                                    icon={'it-upload'}
                                    size={'sm'}
                                    color={'primary'}
                                    className={'icon-white'}
                                  ></Icon>
                                  <span>{t('load_file')}</span>
                                </span>
                              )}
                            </span>
                            {isFileLoading ? (
                              <span>
                                <Progress value={progressBar} color={'primary'} />
                              </span>
                            ) : (
                              ''
                            )}
                          </button>
                          <p className="title-xsmall u-grey-dark pt-10 mb-0">
                            {t('select_max_doc')}
                          </p>
                        </div>
                      </div>
                      </>)}
                    </div>
                  </div>
                </div>
              </section>
              {Boolean(!isExternalRefCategory()) && (<>
              <div className="it-page-sections-container">
                {currentUser?.nome !== 'Anonymous' ? (
                  <section className="it-page-section" id="applicant" {...registerScroll('applicant')}>
                    <div className="cmp-card navbar-custom">
                      <div className="card has-bkg-grey shadow-sm">
                        <div className="card-header border-0 p-0 mb-lg-30 m-0">
                          <div className="d-flex">
                            <h2 className="title-xxlarge mb-1">{t('report_author')}</h2>
                          </div>
                          <p className="subtitle-small mb-0">{t('my_info')}</p>
                        </div>
                        <div className="card-body p-0">
                          <div className="cmp-info-button-card mt-3">
                            <div className="card p-3 p-lg-4">
                              <div className="card-body p-0">
                                <h3 className="big-title mb-0">
                                  {currentUser?.nome ? currentUser?.nome : '--'}{' '}
                                  {currentUser?.cognome}
                                </h3>

                                <p className="card-info">
                                  {t('fiscal_code')} <br />
                                  <span>
                                    {currentUser?.codice_fiscale
                                      ? currentUser?.codice_fiscale
                                      : '--'}
                                  </span>
                                </p>
                                <Accordion>
                                  <AccordionItem>
                                    <AccordionHeader
                                      active={collapseElementOpenCard === '1'}
                                      onToggle={() =>
                                        setCollapseElementCard(
                                          collapseElementOpenCard !== '1' ? '1' : ''
                                        )
                                      }
                                    >
                                      {collapseElementOpenCard !== '1'
                                        ? t('show_more')
                                        : t('show_less')}
                                      <Icon icon={'it-expand'} size={'sm'} color={'primary'}></Icon>
                                    </AccordionHeader>
                                    <AccordionBody
                                      className="accordion-collapse"
                                      active={collapseElementOpenCard === '1'}
                                      listClassName={'p-0'}
                                    >
                                      <div className="cmp-info-summary bg-white has-border">
                                        <Card>
                                          <CardHeader className="border-bottom border-light p-0 mb-0 d-flex justify-content-between d-flex justify-content-end">
                                            <h4 className="title-large-semi-bold mb-3">
                                              {t('contacts')}
                                            </h4>
                                            <span className="d-none text-decoration-none">
                                              <span className="text-button-sm-semi t-primary">
                                                {t('edit')}
                                              </span>
                                            </span>
                                          </CardHeader>
                                          <CardBody className="p-0">
                                            <Controller
                                              name="email_address"
                                              control={control}
                                              rules={{
                                                required: {
                                                  value: true,
                                                  message: t('required_field')
                                                },
                                                pattern: {
                                                  value:
                                                    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                                  message: t('email_validator')
                                                }
                                              }}
                                              render={({ field }) => (
                                                <>
                                                  <Input
                                                    id={'email_address'}
                                                    type="email"
                                                    className={'cmp-input mb-0'}
                                                    label={t('email') + '*'}
                                                    placeholder={''}
                                                    invalid={errors.email_address?.message ? true : false}
                                                    validationText={
                                                      errors.email_address?.message || t('email_description')
                                                    }
                                                    innerRef={field.ref}
                                                    value={field.value}
                                                    onChange={field.onChange}
                                                  />
                                                </>
                                              )}
                                            />
                                            <Controller
                                              name="phone_number"
                                              control={control}
                                              rules={{
                                                required: {
                                                  value: true,
                                                  message: t('required_field')
                                                },
                                                pattern: {
                                                  value: /\+?\d{8,}/,
                                                  message: t('phone_number_description')
                                                }
                                              }}
                                              render={({ field }) => (
                                                <>
                                                  <Input
                                                    id={'phone_number'}
                                                    type="tel"
                                                    className={'cmp-input mb-0'}
                                                    label={t('phone_number') + '*'}
                                                    placeholder={''}
                                                    invalid={errors.phone_number?.message ? true : false}
                                                    validationText={
                                                      errors.phone_number?.message || t('phone_number_description')
                                                    }
                                                    innerRef={field.ref}
                                                    value={field.value}
                                                    onChange={field.onChange}
                                                  />
                                                </>
                                              )}
                                            />
                                          </CardBody>
                                        </Card>
                                      </div>
                                    </AccordionBody>
                                  </AccordionItem>
                                </Accordion>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                ) : (
                  <section className="it-page-section" id="applicant" {...registerScroll('applicant')}>
                    <div className="cmp-card">
                      <div className="card has-bkg-grey shadow-sm p-big">
                        <div className="card-header border-0 p-0 mb-lg-30 m-0">
                          <div className="d-flex">
                            <h2 className="title-xxlarge mb-1">{t('report_author')}</h2>
                          </div>
                          <p className={'subtitle-small mb-0'}>{t('my_info')}</p>
                        </div>
                        <div className="card-body p-0">
                          <div className={'cmp-info-button-card mt-3'}>
                            <div className="form-wrapper bg-white p-4">
                              <Controller
                                name="name"
                                control={control}
                                rules={{ required: {
                                  value: true,
                                  message: t('required_field')
                                } }}
                                render={({ field }) => (
                                  <>
                                    <Input
                                      id={'name'}
                                      type="text"
                                      className={'cmp-input mb-0'}
                                      label={t('name') + '*'}
                                      invalid={errors.name?.type === 'required' ? true : false}
                                      validationText={errors.name?.message}
                                      infoText={errors.name?.message ? '' : t('name_description')}
                                      innerRef={field.ref}
                                      value={field.value}
                                      onChange={field.onChange}
                                    />
                                  </>
                                )}
                              />
                              <Controller
                                name="surname"
                                control={control}
                                rules={{ required: {
                                  value: true,
                                  message: t('required_field')
                                } }}
                                render={({ field }) => (
                                  <>
                                    <Input
                                      id={'surname'}
                                      type="text"
                                      className={'cmp-input mb-0'}
                                      label={t('surname') + '*'}
                                      invalid={errors.surname?.type === 'required' ? true : false}
                                      validationText={errors.surname?.message}
                                      infoText={errors.surname?.message ? '' : t('surname_description')}
                                      innerRef={field.ref}
                                      value={field.value}
                                      onChange={field.onChange}
                                    />
                                  </>
                                )}
                              />
                              <Controller
                                name="fiscal_code"
                                control={control}
                                rules={{
                                  required: {
                                    value: currentUser?.nome !== 'Anonymous' ? true : required_fields.includes("fiscal_code"),
                                    message: t('required_field')
                                  },
                                  validate: checkIsValidCF, // Usa la funzione custom
                                }}
                                render={({ field }) => (
                                  <>
                                    <Input
                                      id={'fiscal_code'}
                                      type="text"
                                      className={'cmp-input mb-0'}
                                      label={
                                        currentUser?.nome !== 'Anonymous'  ||
                                        required_fields.includes('fiscal_code')
                                          ? t('fiscal_code') + '*'
                                          : t('fiscal_code')
                                      }
                                      placeholder={''}
                                      invalid={
                                        errors.fiscal_code?.type === 'required' ||
                                        errors.fiscal_code?.type === 'validate'
                                          ? true
                                          : false
                                      }
                                      validationText={errors.fiscal_code?.message}
                                      infoText={errors.fiscal_code?.message ? '' : ''}
                                      innerRef={field.ref}
                                      value={field.value}
                                      onChange={(event) => {
                                        setValue('fiscal_code', event.target.value.toUpperCase());
                                        field.onChange(event);
                                      }}
                                    />
                                  </>
                                )}
                              />
                              <Controller
                                name="email_address"
                                control={control}
                                rules={{
                                  required: {
                                    value: true,
                                    message: t('required_field')
                                  },
                                  pattern: {
                                    value:
                                      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                    message: t('email_validator')
                                  }
                                }}
                                render={({ field }) => (
                                  <>
                                    <Input
                                      id={'email_address'}
                                      type="email"
                                      className={'cmp-input mb-0'}
                                      label={t('email') + '*'}
                                      placeholder={''}
                                      invalid={errors.email_address?.message ? true : false}
                                      validationText={errors.email_address?.message}
                                      infoText={errors.email_address?.message ? '' : t('email_description')}
                                      innerRef={field.ref}
                                      value={field.value}
                                      onChange={field.onChange}
                                    />
                                  </>
                                )}
                              />
                              <Controller
                                name="phone_number"
                                control={control}
                                rules={{
                                  required: {
                                    value: true,
                                    message: t('required_field')
                                  },
                                  pattern: {
                                    value: /^(?:(\+|00)(?:\d{1,3}\s?)?(?:\d{1,3}\s?))?((?:[1-9](?:[0-9]{1,4})(?:\s?)(?:\d{3,5})(?:\s?)(?:\d{3,5}))|(?:0(?:[0-9]{1,4})(?:\s?)(?:\d{4,13})))$/,
                                    message: t('phone_number_validator')
                                  }
                                }}
                                render={({ field }) => (
                                  <>
                                    <Input
                                      id={'phone_number'}
                                      type="tel"
                                      className={'cmp-input mb-0'}
                                      label={t('phone_number') + '*'}
                                      placeholder={''}
                                      invalid={errors.phone_number?.message ? true : false}
                                      validationText={errors.phone_number?.message}
                                      infoText={errors.phone_number?.message ? '' : t('phone_number_description')}
                                      innerRef={field.ref}
                                      value={field.value}
                                      onChange={field.onChange}
                                    />
                                  </>
                                )}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                )}
              </div>
              </>)}
            </div>
          </div>

          <div className="cmp-nav-steps">
            <nav className="steppers-nav" aria-label="Step">
              <Button
                type="button"
                size={'sm'}
                className="steppers-btn-prev p-0"
                onClick={onPrev}
                disabled={privacyChecked}
              >
                <Icon icon={'it-chevron-left'} size={'sm'} color={'primary'}></Icon>
                <span className="text-button-sm">{t('back')}</span>
              </Button>
              {currentUser?.nome !== 'Anonymous' && (
                <Button
                  type="button"
                  outline
                  color="primary"
                  size={'sm'}
                  className="bg-white steppers-btn-save saveBtn"
                  data-focus-mouse="false"
                  onClick={() => saveApplication()}
                >
                  <span className="text-button-sm t-primary">{t('save_request')}</span>
                </Button>
              )}
              <Button
                color="primary"
                type="submit"
                size={'sm'}
                disabled={!isValid || !isDirty || isExternalRefCategory()}
                className="steppers-btn-confirm"
              >
                <span className="text-button-sm">{t('next')}</span>
                <Icon
                  icon={'it-chevron-right'}
                  size={'sm'}
                  color={'primary'}
                  className={'icon-white'}
                ></Icon>
              </Button>
            </nav>
          </div>
        </Col>
        <div>
          <Modal
            keyboard={false}
            backdrop={'static'}
            isOpen={isOpenMap}
            toggle={() => toggleModalMap(!isOpenMap)}
            centered
            labelledBy="modalMap"
            className='mx-0 mx-sm-auto'
            size={'lg'}
          >
            <ModalHeader toggle={() => toggleModalMap(!isOpenMap)} id="modalMap">
              {t('select_map_point')}
            </ModalHeader>
            <ModalBody>
              <div className='title-xsmall u-grey-dark mb-1'>{t('desc_map_point')}</div>
              <MapContainer
                center={position}
                zoom={20}
                scrollWheelZoom={true}
                style={{ height: 'calc(100vh - 300px)', minHeight: '350px', maxHeight: '500px' }}
              >
                <MapLayers
                  position={position}
                  draggable={true}
                  layers={
                    service?.geographic_areas_id?.length > 0 ? service?.geographic_areas_id : false
                  }
                />
                <TileLayer
                  attribution='&copy; <a target="_blank" href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                  url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
              </MapContainer>
            </ModalBody>
            <ModalFooter>
              <Button color="secondary" onClick={() => closeModalAddress()}>
                {t('close')}
              </Button>
              <Button color="primary" onClick={() => saveAddress()}>
                {t('save_address')}
              </Button>
            </ModalFooter>
          </Modal>
          <ModalMessages
            isOpen={isOpenModalMessages}
            toggleModal={setModalMessages}
            message={message}
          ></ModalMessages>
        </div>
      </Row>
      <NotificationManager></NotificationManager>
    </form>
  );
}

export const WrapperClassSelect = styled.div`
  .react-select__value-container {
    border: none;
    border-radius: 0;
    outline: 0;
    width: 100%;
    box-shadow: none;
    font-weight: 700;
    color: #17324d;
    background-color: #fff;

    &:focus:not(.focus--mouse) {
      border-color: #000 !important;
      box-shadow: 0 0 0 3px #000 !important;
      outline: 3px solid #fff !important;
      outline-offset: 3px;
    }
  }

  .react-select__control {
    border: none;
    border-bottom: 1px solid #5b6f82;
    box-shadow: none;
    border-radius: 0;

    &:hover {
      border-color: #5b6f82;
    }

    &.react-select__control--is-focused {
      border-color: #000 !important;
      box-shadow: 0 0 0 3px #000 !important;
      outline: 3px solid #fff !important;
      outline-offset: 3px;
    }

    &.react-select__control--is-disabled {
      .react-select__value-container {
        background-color: hsl(0, 0%, 95%);
      }
    }
  }

  .react-select__input {
    height: 2rem !important;

    :focus:not(.focus--mouse) {
      box-shadow: none !important;
      outline: none !important;
      outline-offset: 3px;
    }
  }

  .react-select__indicator-separator {
    display: none;
  }

  .react-select__menu {
    z-index: 2;
  }

  .react-select__single-value {
    margin-left: 4px;
  }

  .react-select__placeholder {
    color: #596d88;
  }

  input:focus {
    border: none !important;
    outline: none !important;
    box-shadow: none !important;
  }
`;
