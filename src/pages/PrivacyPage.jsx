import { Button, Col, Input, Label, Row, Spinner } from 'design-react-kit';
import React, { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';

import { Trans, useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { Link } from 'react-router-dom';
import { apiActions, authActions, storePrivacy } from '../_store';

export { PrivacyPage };

function PrivacyPage({ onNextPage, activeStep }) {
  const { t } = useTranslation();
  const [privacyState, setPrivacy] = useState(false);
  const [loader, setLoader] = useState(true);
  const {application } = useSelector((x) => x.api);
  const { token } = useSelector((x) => x.auth);
  const { privacy } = useSelector((x) => x.form);
  const dispatch = useDispatch();

  const {
    handleSubmit,
    formState: { isDirty, isValid },
    control,
    setValue
  } = useForm({
    mode: 'onChange',
    defaultValues: {
      privacy: false
    }
  });

  const onSubmit = (data) => {
    dispatch(storePrivacy(data.privacy));
    onNextPage(activeStep + 1);
  };

  const isTrue = (value) => value || t('required_field');

  // get draft application
  useEffect(() => {
    if (application?.data?.hasOwnProperty('privacy')) {
      setPrivacy(true);
      setValue('privacy', application?.data?.privacy, {
        shouldValidate: true,
        shouldDirty: true
      });
    } else {
      setPrivacy(privacy);
      setValue('privacy', privacy, {
        shouldValidate: true,
        shouldDirty: true
      });
    }
  }, [setValue, application.data, privacy]);

  const handleChange = (event) => {
    dispatch(storePrivacy(event.target.checked));
    setPrivacy(event.target.checked);
  };

  // recovery token otherwise I create it
  useEffect(() => {
    dispatch(apiActions.getServiceReport());
    dispatch(apiActions.tenantInfo())
      .unwrap()
      .then((res) => {
        dispatch(authActions.getSessionToken()).then((el) => {
          if (el.meta.requestStatus === 'rejected' && !token) {
            dispatch(authActions.createSessionToken())
              .unwrap()
              .then(() => {
                setLoader(false);
              });
          } else {
            dispatch(apiActions.getPrivacyCheckApplication())
              .unwrap()
              .then((response) => {
                if (response.data.length) {
                  dispatch(storePrivacy(true));
                  onNextPage(activeStep + 1);
                  setLoader(false);
                } else {
                  dispatch(apiActions.getDraftApplication());
                  setLoader(false);
                }
              });
          }
        });
        if (!window.OC_BB) {
          const cadastral_code = res.cadastral_code ? res.cadastral_code : res.administrative_code;
          dispatch(apiActions.cityInfo({ cadastral_code: cadastral_code }))
            .unwrap()
            .then((cityRes) => {
              if (cityRes.length === 1 && cityRes[0].comune && cityRes[0].cap) {
                const data = cityRes[0];
                const cap = Array.isArray(data.cap) ? data.cap[0] : data.cap;
                dispatch(apiActions.searchBoundingbox({ city: data.comune, postalcode: cap }));
              }
            });
        }
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token]);

  return (
    <div style={{ minHeight: '408px' }}>
      {!loader ? (
        <form onSubmit={handleSubmit(onSubmit)}>
          <Row className="justify-content-center">
            <div className="col-12 col-lg-8 pb-40 pb-lg-80">
              <p className="text-paragraph mt-4 mb-0 mt-md-40 mb-lg-40">
                <Trans
                  t={t}
                  values={{
                    url: window.OC_PRIVACY_URL
                  }}
                  i18nKey={"privacy_description"}
                  components={{ Link: <Link /> }}
                ></Trans>
              </p>

              <div className="form-check mt-4 mb-3 mt-md-40 mb-lg-40">
                <div className="checkbox-body d-flex align-items-center">
                  <Controller
                    name="privacy"
                    control={control}
                    rules={{ required: false, validate: isTrue }}
                    render={({ field }) => (
                      <>
                        <Input
                          id="privacy"
                          invalid={!isValid ? true : false}
                          innerRef={field.ref}
                          value={field.value}
                          onChange={(event) => {
                            field.onChange(event);
                            handleChange(event);
                          }}
                          type="checkbox"
                          checked={privacyState}
                          aria-label={t('privacy_label')}
                        />
                        <Label
                          className="title-small-semi-bold pt-1 active"
                          htmlFor="privacy"
                          id={'privacyDescription'}
                        >
                          {t('privacy_label')}
                        </Label>
                      </>
                    )}
                  />
                </div>
              </div>
              <Button
                color="primary"
                className="mobile-full"
                type="submit"
                disabled={!isValid || !isDirty}
              >
                <span className=""> {t('next')}</span>
              </Button>
            </div>
          </Row>
        </form>
      ) : (
        <Row>
          <Col
            lg={12}
            className="d-flex justify-content-center align-items-center"
            style={{ minHeight: '408px' }}
          >
            <Spinner active></Spinner>
          </Col>
        </Row>
      )}
    </div>
  );
}
