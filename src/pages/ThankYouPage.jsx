import { Col, Container, Icon, Row } from 'design-react-kit';
import { t } from 'i18next';
import { useEffect } from "react";
import { Trans } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
export { ThankYouPage };

function ThankYouPage() {
  // const { email_address, application, sequential_id } = useSelector((x) => x.form);
  const { application_tmp } = useSelector((x) => x.reports);
  const { currentUser } = useSelector((x) => x.currentUser);

  // SE clicco il pulsante back del browser ricarico la pagina
  useEffect(() => {
    window.addEventListener("popstate", () => {
      window.location.reload()
    });
  }, []);

  return (
    <Container>
      <Row className="justify-content-center mb-50">
        <Col className="col-12 pb-2" lg={10}>
          <div className="cmp-heading p-0">
            <div className="categoryicon-top d-flex">
              <Icon color="success" icon="it-check-circle" className={' mr-10 icon-sm mb-1'} />
              <h1 className="title-xxxlarge">{t('report_sent')}</h1>
            </div>
            <p className="subtitle-small">
              {application_tmp?.data?.sequential_id || application_tmp?.id ? (
                <Trans
                  t={t}
                  values={{ id: application_tmp?.data?.sequential_id || application_tmp?.id }}
                  i18nKey={'thanks_message_report_sent'}
                  components={{ bold: <strong /> }}
                ></Trans>
              ) : null}
            </p>
            {/* <p className="subtitle-small">
              <Trans
                t={t}
                i18nKey={'thanks_message_report_sent_description'}
                components={{ Link: <Link /> }}
              ></Trans>
            </p> */}
            <p className="subtitle-small pt-3 pt-lg-4">
              <Trans t={t} i18nKey={'thanks_message_report_sent_email'}></Trans>
              <br />
              <strong>{application_tmp?.data['applicant.data.email_address']}</strong>
            </p>
            {/*        <Button outline color={'primary'} className="bg-white fw-bold">
              <Link to={`/`} download target={'_blank'}>
                <span className="rounded-icon">
                  <Icon icon={'it-download'} size={'sm'} color={'primary'}></Icon>
                </span>
                <span>
                  {' '}
                  <Trans t={t} i18nKey={'thanks_message_report_sent_download'}></Trans>
                </span>
              </Link>
            </Button> */}
          </div>
          {currentUser?.nome !== 'Anonymous' && (
            <p className="mt-3">
              <Trans
                t={t}
                values={{
                  url: window.OC_BASE_URL && window.OC_SHOW_DETAIL_ON_WEBSITE !== true ? `${window.OC_BASE_URL}/pratiche/${application_tmp.id}/detail` : `/segnalazioni/${application_tmp.id}`
                }}
                i18nKey={'thanks_message_report_sent_read_post'}
                components={{ Link: <Link /> }}
              ></Trans>
              <span className="text-paragraph">
                <Trans t={t} i18nKey={'thanks_message_report_sent_read_post_description'}></Trans>
              </span>
            </p>
          )}
        </Col>
      </Row>
    </Container>
  );
}
