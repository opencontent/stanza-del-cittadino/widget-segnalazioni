import {
  Button,
  Col,
  Container,
  Nav,
  NavItem,
  NavLink,
  Row,
  Spinner,
  TabContent,
  TabPane
} from 'design-react-kit';

import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { MapContainer } from 'react-leaflet';

import { useSelector, useDispatch } from 'react-redux';

import { useNavigate } from 'react-router-dom';
import Breadcrumbs from '../_components/Breadcrumbs/Breadcrumbs';
import { CardPost } from '../_components/Card/CardPost';
import { CategoryItem } from '../_components/Card/CategoryItem';
import { MapCustom } from '../_components/Map/MapCustom';
import { detectBoundingBox } from '../_helpers/utilities';
import { apiActions, reportsActions } from '../_store';
import { Login } from '../login/Login';

export { ListReports };

function ListReports() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const ref = useRef(null);
  const [enableMap, setEnableMap] = useState(false);
  const { currentUser } = useSelector((x) => x.currentUser);
  const { token, loading } = useSelector((x) => x.auth);
  const { t, i18n } = useTranslation();

  const { listReports, newItems, categories, filters, geoJson } = useSelector((x) => x.reports);
  const { service } = useSelector((x) => x.api);

  const [activeTab, toggleTab] = useState('2');
  let [reset, setReset] = useState(false);

  const defaultBoundingBox = detectBoundingBox();

  useEffect(() => {
    dispatch(reportsActions.getCategories());
    dispatch(apiActions.getServiceReport());
    if (token) {
      // dispatch(currentUserActions.getCurrentUser());
      if (filters?.categories?.length && currentUser?.nome !== 'Anonymous') {
        const query = filters.categories
          .map((el) => `field[type.value]=${el.value}&`)
          .join('')
          .trim();
        dispatch(reportsActions.filterPosts(query));
        dispatch(reportsActions.getReportsGeoJson(query));
      } else {
        dispatch(reportsActions.getAllReports());
        dispatch(reportsActions.getReportsGeoJson());
      }
    }
  }, [dispatch, filters, currentUser?.nome, token]);

  const onClickNextPost = (next) => {
    if (next) {
      dispatch(reportsActions.getOthersReports({ next }), listReports).unwrap();
    }
  };

  const loadMap = () => {
    setEnableMap(true);
  };

  const handleRemoveAllFilters = (event) => {
    //dispatch(postsActions.filterMyPost(false));
    dispatch(reportsActions.removeAllFiltersCategory());
    setChildCallables();
  };

  // const handleMyPost = (event) => {
  // Toggle filter for my posts
  // dispatch(postsActions.filterMyPost(event.target.checked));
  // dispatch(postsActions.myPosts());
  //};

  const setChildCallables = () => {
    setReset(true);
  };

  const pull_data = () => {
    if (reset) {
      setReset(false);
    }
  };

  return (
    <Container tag="section" style={{minHeight:'100vh'}}>
      {!loading && currentUser?.nome !== 'Anonymous' && token ? (
        <>
          <Row className="justify-content-center">
            <Col
              tag="header"
              className="col-12"
              lg={10}
            >
              {window.OC_RENDER_BREADCRUMB === true ||
              process.env.REACT_APP_OC_RENDER_BREADCRUMB === 'true' ? (
                <Breadcrumbs></Breadcrumbs>
              ) : null}
              <h1>{t('report_list')}</h1>
              <p className="subtitle-small">{t('list_reports_description')}</p>
            </Col>
          </Row>
          <Row className="mb-md-40 mb-lg-80">
            <Col>
              <hr className="d-none d-lg-block mt-30 mb-2" />
            </Col>
          </Row>
          <Row className={'mb-md-40 mb-lg-80'}>
            <Col
              tag="aside"
              lg={3}
            >
              <form aria-labelledby='filters-name'>
                <fieldset>
                  <div className="pb-4">
                    <div className="w-100 d-flex justify-content-between border-bottom border-light pb-3 mt-5">
                      <legend className="h6 text-uppercase category-list__title" id="filters-name">
                        {t('categories')}
                      </legend>
                      {filters?.categories?.length > 0 ? (
                        <span>
                          <Button
                            type="button"
                            onClick={(e) => handleRemoveAllFilters(e)}
                            className="p-0 pe-2 d-none d-lg-block"
                          >
                            <span className="title-xsmall-semi-bold ms-1">
                              {t('remove_all_filters')}
                            </span>
                          </Button>
                          <Button
                            type="button"
                            className="p-0 pe-2 d-lg-none"
                            onClick={(e) => handleRemoveAllFilters(e)}
                          >
                            <span className="rounded-icon"></span>
                            <span className="t-primary title-xsmall-semi-bold ms-1">
                              {t('remove_all_filters')}
                            </span>
                          </Button>
                        </span>
                      ) : null}
                    </div>
                    {categories[i18n.resolvedLanguage]?.length ? (
                      <ul>
                        {categories[i18n.resolvedLanguage].map((cat, index) => (
                          <CategoryItem
                            category={cat}
                            key={cat.value + index.toString()}
                            setCallables={pull_data}
                            reset={reset}
                          ></CategoryItem>
                        ))}
                      </ul>
                    ) : (
                      <Col className="text-center p-5">
                        <Spinner double active />
                      </Col>
                    )}
                  </div>
                </fieldset>
              </form>
            </Col>
            <Col
              tag="section"
              lg={8}
              className={'offset-lg-1'}
            >
              <Row>
                <div className="w-100 d-flex justify-content-between border-bottom border-light pb-3 mt-5">
                  <p role="status" className="search-results">
                    {listReports.meta?.count ? `${t('results')} ${listReports.meta?.count}` : ''}
                  </p>
                  {/*        {currentUser && currentUser.last_access_at !== null ? (
                <FormGroup check className={'mt-0'}>
                  <Input
                    id="checkboxMyPosts"
                    type="checkbox"
                    {...(filters.auth && { checked: true })}
                    onChange={handleMyPost}
                    checked={true}
                    readOnly={true}
                  />
                  <Label for="checkboxMyPosts" className={'mb-0 lh-auto'}>
                    {t('my_reports')}
                  </Label>
                </FormGroup>
              ) : (
                ''
              )} */}
                </div>

                <Nav
                  tabs
                  className="w-100 flex-nowrap border-bottom border-light mb-40 shadow-none"
                >
                  <NavItem className="w-100">
                    <NavLink
                      tag="button"
                      active={activeTab === '2'}
                      className="title-medium-semi-bold"
                      onClick={(e) => {
                        e.preventDefault();
                        if (activeTab !== '2') {
                          toggleTab('2');
                        }
                      }}
                    >
                      {t('list')}
                    </NavLink>
                  </NavItem>
                  <NavItem className="w-100">
                    <NavLink
                      tag="button"
                      active={activeTab === '1'}
                      className="title-medium-semi-bold"
                      onClick={(e) => {
                        e.preventDefault();
                        if (activeTab !== '1') {
                          toggleTab('1');
                        }
                        loadMap();
                      }}
                    >
                      {t('map')}
                    </NavLink>
                  </NavItem>
                </Nav>
                <TabContent activeTab={activeTab} className={'w-100 p-0'}>
                  <TabPane tabId="1" className="p-0 p-md-3" style={{ height: '600px' }}>
                    {geoJson && geoJson.features && enableMap ? (
                      <MapContainer
                        //center={[44.7510665, 11.7575136]}
                        bounds={defaultBoundingBox || []}
                        zoom={6}
                        scrollWheelZoom={false}
                        style={{ height: '600px' }}
                      >
                        <MapCustom
                          draggable={false}
                          geoJson={geoJson}
                          layers={
                            service?.geographic_areas_id.length > 0
                              ? service?.geographic_areas_id
                              : false
                          }
                        ></MapCustom>
                      </MapContainer>
                    ) : (
                      <Col lg={12} className="text-center p-5">
                        <Spinner double active />
                      </Col>
                    )}
                  </TabPane>
                  <TabPane tabId="2" className="p-0 p-md-3">
                    {listReports.meta?.count > 0 ? (
                      <div>
                        {listReports.data.map((r, index) => (
                          <CardPost report={r} key={r.id + index.toString()}></CardPost>
                        ))}
                        <div ref={ref}></div>
                      </div>
                    ) : null}
                    {listReports.meta?.count === 0 ? (
                      <div>
                        <p role="status" className="h3">{t('no_reports')}</p>
                      </div>
                    ) : null}

                    {listReports?.loading || newItems?.loading ? (
                      <Col lg={12} className="text-center">
                        <Spinner double active />
                      </Col>
                    ) : null}
                    {listReports?.links?.next && !newItems?.loading && (
                      <Col lg={12} className="text-center">
                        <Button
                          outline
                          color="primary"
                          type="button"
                          onClick={() => onClickNextPost(listReports.links.next)}
                          className="mobile-full py-3 mt-10 mx-auto"
                        >
                          {t('load_more_reports')}
                        </Button>
                      </Col>
                    )}
                    <div ref={ref}></div>
                  </TabPane>
                </TabContent>
              </Row>
              {currentUser?.nome !== 'Anonymous' && currentUser.last_access_at !== null && window.OC_READ_ONLY_MODE !== true ? (
                <Col
                  tag="section"
                  lg={6}
                  className="mt-50 mb-4 mb-lg-0"
                >
                  <div className="cmp-text-button mt-0" onClick={() => navigate('/')}>
                    <h2 className="title-xxlarge mb-0">{t('make_a_report')}</h2>
                    <div className="text-wrapper">
                      <p className="subtitle-small mb-3 mt-3">{t('make_a_report_description')}</p>
                    </div>
                    <div className="button-wrapper">
                      <Button
                        type="button"
                        color={'primary'}
                        onClick={() => navigate('/')}
                        className="mobile-full py-3 mt-2 mb-4 mb-lg-0"
                        data-focus-mouse="false"
                      >
                        <span className="">{t('report_outage')}</span>
                      </Button>
                    </div>
                  </div>
                </Col>
              ) : null}
            </Col>
          </Row>
        </>
      ) : null}

      {!loading && !token ? <Login></Login> : null}
      {loading ? (
        <Row  style={{ minHeight: '408px' }}>
          <Col
            lg={12}
            className="d-flex justify-content-center align-items-center"
          >
            <Spinner active></Spinner>
          </Col>
        </Row>
      ) : null}
    </Container>
  );
}
