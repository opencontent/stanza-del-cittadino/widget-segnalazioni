import { useEffect, useLayoutEffect } from 'react';
import { Routes, Route, useLocation } from 'react-router-dom';
import 'react-medium-image-zoom/dist/styles.css';

//Set globally BTS-ITALIA Version
import BOOTSTRAP_ITALIA_VERSION from 'bootstrap-italia/dist/version';
import { loadFonts } from 'bootstrap-italia';
import { routes } from './routes';
import AuthProvider from './context/AuthProvider';
import { ErrorBoundary } from 'react-error-boundary';
import { Errors } from './pages/Errors';
import { useSelector } from 'react-redux';
const { version } = require('../package.json');

window.BOOTSTRAP_ITALIA_VERSION = BOOTSTRAP_ITALIA_VERSION;
window.OC_INEFFICIENCIES_VERSION = version;

export function App() {
  // init custom history object to allow navigation from
  // anywhere in the react app (inside or outside components
  const { pathname } = useLocation();
  const { error } = useSelector((x) => x.auth);

  useEffect(() => {
    // Importing files depending on env
    if (process.env.REACT_APP_STYLE === 'true') {
      import(`./assets/stylesheets/core.scss`);
      import('./index.scss');
      loadFonts('https://static.opencityitalia.it/fonts');
    } else {
      import('./index.scss');
    }
  }, []);

  // Scroll to top if path changes
  useLayoutEffect(() => {
    window.scrollTo(0, 0);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pathname]);

  return (
    <div>
      {error ? (
        <ErrorBoundary
          FallbackComponent={Errors}
          fallback={<Errors error={{ name: 'ErrorBoundary' }}></Errors>}>
          <Errors error={{ name: 'ErrorBoundary' }}></Errors>
        </ErrorBoundary>
      ) : (
        <AuthProvider>
          <Routes>
            {routes.map((route) => (
              <Route key={route.path} path={route.path} element={route.Component} />
            ))}
          </Routes>
        </AuthProvider>
      )}
    </div>
  );
}
