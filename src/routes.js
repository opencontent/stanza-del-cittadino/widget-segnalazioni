import { Navigate } from 'react-router-dom';
import { PrivateRoute } from './_components/PrivateRoute';
import { Login } from './login/Login';
import { DetailsReportPage } from './pages/DetailsReportPage';
import { ListReports } from './pages/ListReports';
import { NewReport } from './pages/NewReport';
import { ThankYouPage } from './pages/ThankYouPage';
import {ReturnLoginAuthRoute} from "./_components/ReturnLoginAuthRoute";
import {NoAuthRoute2} from "./_components/NoAuthRoute2";

/*
 path: route path
 name: i18n name
 Component: Element React to render
 */
export const routes = [
  {
    path: '/',
    name: 'report_outage',
    Component: <Navigate to={`${window.OC_READ_ONLY_MODE === true  || /segnalazioni\/|inefficiencies-list/.test(window.location.href) ? "/segnalazioni" : "/segnala_disservizio"}`} />
  },
  {
    path: '/segnalazioni',
    name: 'reports',
    Component: (
      <PrivateRoute>
        <ListReports />
      </PrivateRoute>
    )
  },
  {
    path: '/segnalazioni/:id',
    name: 'detail',
    Component: (
      <PrivateRoute>
        <DetailsReportPage />
      </PrivateRoute>
    )
  },
  {
    path: '/segnala_disservizio',
    name: 'report_outage',
    Component: <NoAuthRoute2><NewReport></NewReport></NoAuthRoute2>
  },
  {
    path: '/segnala_disservizio/thankyou',
    name: 'malfunction_reporting',
    Component: <ThankYouPage></ThankYouPage>
  },
  { path: '/login', name: 'Login', Component: <ReturnLoginAuthRoute><Login></Login></ReturnLoginAuthRoute>},
  { path: '/thankyou', name: 'malfunction_reporting', Component: <ThankYouPage></ThankYouPage> }
];
