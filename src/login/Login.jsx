import { Button, Container, Row } from "design-react-kit";
import { t } from 'i18next';
import React from 'react';
import { useNavigate} from 'react-router-dom';
import Breadcrumbs from '../_components/Breadcrumbs/Breadcrumbs';
import { useTranslation } from "react-i18next";
import { getLoginProviders } from "_helpers/utilities";
import {useAuthContext} from "../context/AuthProvider";

export { Login };

function Login({anonymAllowed,setterWithoutLogin}) {
  const navigate = useNavigate();
  const readOnlyMode = window.OC_READ_ONLY_MODE === true  || /segnalazioni\/|inefficiencies-list/.test(window.location.href);
  const { setSharedState,sharedState } = useAuthContext();
  const handleClick = () => {
    const urlParams = new URL(decodeURIComponent(window.location.href));
    const params = urlParams.toString().split('return-url=');
    const returnUrl = params[1];
    const defaultRedirectUrl = readOnlyMode ? 'segnalazioni' : 'segnala_disservizio'
    window.location.href = `${window.OC_AUTH_URL}?return-url=${returnUrl || window.location.href.replace(/login/,defaultRedirectUrl)}`;
    return null;
  };

  const handleClickAnonym = () => {
    setSharedState({anonym:true})
   return  navigate('/segnala_disservizio',{state:{anonym:true}});
  };



  const { i18n } = useTranslation();
  const language = i18n.language;
  const loginProvider = getLoginProviders(language);

  return (
      <Container style={{ minHeight: '408px' }}>
        <Row>
          <div className="col-12 col-lg-10 offset-lg-1">
            <div className="cmp-heading pb-3 pb-lg-4">
              {window.OC_RENDER_BREADCRUMB === true ||
              process.env.REACT_APP_OC_RENDER_BREADCRUMB === 'true' ? (
                  <Breadcrumbs></Breadcrumbs>
              ) : null}
              <h1 className="title-xxxlarge">{t('malfunction_reporting')}</h1>
              <p className="subtitle-small">{t('login_description',{loginProviders:loginProvider})}</p>
            </div>
          </div>
        </Row>
        <hr className="d-none d-lg-block mt-0 mb-4" />
        <Row>
          <div className="cmp-text-button col-lg-6">
            <h2 className="title-xxlarge mb-0">{t('id_login_auth',{loginProviders:loginProvider})}</h2>
            <div className="text-wrapper">
              <p className="subtitle-small mb-3">{t('id_description',{loginProviders:loginProvider})}</p>
            </div>
            <div className="button-wrapper mb-2">
              <Button type="button" color={'primary'} className="btn-icon" onClick={handleClick}>
                <span className="">{t('login')}</span>
              </Button>
            </div>
          </div>
          {!readOnlyMode && (
              <div className="cmp-text-button col-lg-6">
                <h2 className="title-xxlarge mb-0">{t('access_anonymous')}</h2>
                <div className="text-wrapper">
                  <p className="subtitle-small mb-3">{t('access_anonymous_description')}</p>
                </div>
                <div className="button-wrapper mb-2">
                  <Button
                      type="button"
                      color={'primary'}
                      className="btn-icon"
                      onClick={handleClickAnonym}
                  >
                    <span>{t('access_anonymous')}</span>
                  </Button>
                </div>
              </div>
          )}
        </Row>
      </Container>
  );
}
