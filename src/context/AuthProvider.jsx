import { useContext, createContext, useState, useEffect } from "react";
import { authActions, currentUserActions } from "../_store";
import {useDispatch, useSelector} from "react-redux";
import { Spinner } from "design-react-kit";
import {isEmpty} from "lodash-es";
import {jwtDecode} from "jwt-decode";


const AuthContext = createContext();

const AuthProvider = ({ children }) => {
  const [isAnonymUser, setIsAnonymUser] = useState(null);
  const [errorAuth, setErrorAuth] = useState(null);
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();
  const { token } = useSelector((x) => x.auth);
  const [sharedState, setSharedState] = useState(null);


  const setterWithoutLogin = (data) => {
    setIsAnonymUser(data);
  };

  const checkStateService = (idUser) => {
    dispatch(currentUserActions.getUserById({id: idUser}))
      .unwrap()
      .then(() => {
        setLoading(false);
        setIsAnonymUser(false)
        setErrorAuth(false)
        setSharedState({anonym: false})
      });
  };

  const decodeUserToken = (token) => {
    return jwtDecode(token)
  }


//Fake auth for developer mode use REACT_APP_DEV_TOKEN
  useEffect(() => {
    if (process.env.NODE_ENV === "development" ){
    if(`${process.env.REACT_APP_DEV_TOKEN}`) {
        const user = decodeUserToken(`${process.env.REACT_APP_DEV_TOKEN}`)
    //  dispatch(authActions.storeUser(user))
        checkStateService(user.id);
      }else{
        setLoading(false);
        setErrorAuth(true)
      }
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])

  useEffect(() => {
    if (process.env.NODE_ENV === "production") {
      dispatch(authActions.getSessionToken()).then((el) => {
        if (el.meta.requestStatus !== "rejected") {
          if( el.payload.token){
            const user = decodeUserToken(el.payload.token)
            if(!isEmpty(user)){
              checkStateService(user.id);
            }
          }
        } else {
          dispatch(authActions.createSessionToken())
              .unwrap()
              .then((item) => {
                if(item.token){
                  const user = decodeUserToken(item.token)
                  if(!isEmpty(user)){
                    checkStateService(user.id);
                  }
                }
              });
            setLoading(false);
            setErrorAuth(true)
        }
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className={"ocl"}>
      {loading ? (
        <div className="d-flex align-items-center justify-content-center vh-100">
          <Spinner active></Spinner>
        </div>
      ) :
          <AuthContext.Provider value={{ sharedState, setSharedState }}>{children}</AuthContext.Provider>
    }
    </div>
  );
};

export default AuthProvider;

export const useAuthContext = () => {
  return useContext(AuthContext);
};
